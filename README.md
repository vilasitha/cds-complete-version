# General Setup for Angular 4 or 5 app release branch
    Make sure you have node version above 6.9 and npm above 3
    Check the version by using node --version and npm --version
    Install Node.js and npm: https://nodejs.org/en/download/
    Install angular cli using npm install -g @angular/cli
    -g installs the angular globally on your system and you just have to run this command only once
    Create the angular project using ‘ng new angularprojectname’
    Go to that project folder and check the angular version by ‘ng -v’
   
# Repo Specific Setup
    Clone the repo. Open git-bash and navigate to that directory (should be C:\DEV\git\cds-frontend) Install angular-cli globally: npm install -g @angular/cli Run npm install, Run npm start

# Local Development
    Angular is single page application and locally it runs on command line interface.
    Before running the local server for the first time, open git bash and Run npm install.
    Run this command to get the local server
    `npm start`
    Then visit `http://localhost:4200/`

# CdsFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.3. 

## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` or `npm run-script build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
