import { Component, OnInit, AfterViewInit, ViewChild, Output, EventEmitter } from '@angular/core';

import { DevdataComponent } from '../devdata/devdata.component';
import { LoginUser } from '../../modules/cdscheck/home/shared/models/login-user';
import { Observable } from 'rxjs/Observable';
import { DevdataService } from '../devdata/devdata.service';
import { SharedService } from '../../modules/cdscheck/home/shared/services/shared.service';
import { HomeService } from '../../modules/cdscheck/home/services/home.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  loginUserNameVar: string;
  headerTitle = 'Check Service Center';
  constructor(
    private homeService: HomeService) { }
  ngOnInit() {
    this.getUser();
  }

  /**below method is to get displayName from users api */
  getUser() {
    this.homeService.currentUser.subscribe((resp) => this.loginUserNameVar = resp);
  }
  logout() {
    window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
  }

}
