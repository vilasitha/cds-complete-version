import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common/src/pipes';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  currentYear = new Date();
  constructor() { }

  ngOnInit() {
  }

}
