import { Component, OnInit, Input } from '@angular/core';
import { LoginUser } from '../../modules/cdscheck/home/shared/models/login-user';
import { DevdataService } from '../devdata/devdata.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Params } from '@angular/router/src/shared';
import { TransactionsResponse } from '../../modules/cdscheck/home/your-transactions/models/all-trasactions';
import { HomeService } from '../../modules/cdscheck/home/services/home.service';
import { SharedService } from '../../modules/cdscheck/home/shared/services/shared.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {

  requestVar: boolean;
  replaceVar: boolean;
  voidVar: boolean;
  clientId: string;
  maintTab: boolean;
  yourTransTab: boolean;
  sourcesTab :boolean;

  constructor(
    private devdataService: DevdataService,
    private activatedroute: ActivatedRoute,
    private homeService: HomeService,
    private router: Router,
    private sharedService: SharedService) {

  }
  ngOnInit() {
    this.homeService.requestVarMsg.subscribe((result) => this.requestVar = result);
    this.homeService.voidVarMsg.subscribe((result) => this.voidVar = result);
    this.homeService.replaceVarMsg.subscribe((result) => this.replaceVar = result);
    this.homeService.maintenanceVarMsg.subscribe((result) => { this.maintTab = result; });
    this.homeService.yourTransVarMsg.subscribe((result) => { this.yourTransTab = result; });
    this.sharedService.clientIdFromAPI.subscribe((result) => { this.clientId = result; });
    this.homeService.sourcesVarMsg.subscribe((result) =>  {this.sourcesTab = result;});
  }

}
