import { Routes } from '@angular/router';
import { HomeComponent } from './modules/cdscheck/home/home.component';
import { PageNotFoundComponent } from './modules/cdscheck/home/page-not-found/page-not-found/page-not-found.component';

export const appRoutes: Routes = [

  { path: '', redirectTo: 'home/yourTransactions', pathMatch: 'full' },
  {
    path: 'home', component: HomeComponent, loadChildren: 'app/modules/cdscheck/home/home.module#HomeModule', data: { preload: true }
  },

  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: 'not-found' }

];