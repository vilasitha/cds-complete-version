import { Routes, Router } from '@angular/router';

import { HomeComponent } from './home.component';
import { ReplaceOrVoidChecksComponent } from '../home/checktransactions/replace-or-void-checks/replace-or-void-checks.component';
import { VerifyCheckInfoComponent } from '../home/checktransactions/verify-check-info/verify-check-info.component';
import { ConfirmchecksComponent } from '../home/checktransactions/confirmchecks/confirmchecks.component';
import { ViewReplaceOrVoidChecksComponent } from '../home/checktransactions/view-replace-or-void-checks/view-replace-or-void-checks.component';
import { DisbursementComponent } from '../home/checktransactions/disbursement/disbursement.component';
import { YourTransactionsComponent } from './your-transactions/your-transactions.component';
import { PayeeInfoComponent } from '../home/checktransactions/disbursement/payee-info/payee-info.component';
import { AccountingDetailsComponent } from '../home/checktransactions/disbursement/payee-info/accounting-details/accounting-details.component';
import { ViewDisbursementComponent } from './checktransactions/view-disbursement/view-disbursement.component';
import { VerifyVoidCheckComponent } from './checktransactions/verify-void-check/verify-void-check.component';
import { SearchPayeeComponent } from './maintenance/payee-table/search-payee/search-payee.component';
import { AddPayeeComponent } from './maintenance/payee-table/add-payee/add-payee.component';
import { ViewPayeeComponent } from './maintenance/payee-table/view-payee/view-payee.component';
import { SearchSourcecodeComponent } from './maintenance/source-code/search-sourcecode/search-sourcecode.component';
import { ViewMaintSourcesComponent } from './maintenance/source-code/view-maint-sources/view-maint-sources.component';
import { SuccessMessageComponent } from './maintenance/success-message/success-message.component';

export const homeRoutes: Routes = [

    {
        path: '',
        children: [
            { path: 'yourTransactions', component: YourTransactionsComponent },
            { path: 'voidChecks/:clientId', component: ReplaceOrVoidChecksComponent },
            { path: 'replaceChecks/:clientId', component: ReplaceOrVoidChecksComponent },
            { path: 'requestDisbursement/:path', component: DisbursementComponent },
            { path: 'viewChecks', component: ViewReplaceOrVoidChecksComponent },
            { path: 'verifyCheckInfo/:check', component: VerifyCheckInfoComponent },
            { path: 'verifyVoidCheckInfo/:check', component: VerifyVoidCheckComponent },
            { path: 'confirmation', component: ConfirmchecksComponent },
            { path: 'payeeAddress/:payeeAddress', component: PayeeInfoComponent },
            { path: 'accoutingDetails/:list', component: AccountingDetailsComponent },
            { path: 'viewDisbursement', component: ViewDisbursementComponent },
            { path: 'searchPayee', component: SearchPayeeComponent },
            { path: 'addPayee/:data', component: AddPayeeComponent},
            { path: 'viewPayee',component: ViewPayeeComponent},
            { path: 'maintenance/sourceCodes', component: SearchSourcecodeComponent },
            { path: 'maintenance/viewSources', component: ViewMaintSourcesComponent },
            { path: 'maintenance/success', component: SuccessMessageComponent}
        ]
    }
];