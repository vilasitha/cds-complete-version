import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';
import { LoginUser } from '../../shared/models/login-user';
import { OnInit } from '@angular/core';
import { Constants } from '../constants/constants';

@Injectable()
export class UserRolesService implements OnInit {
  ngOnInit() {
  }
  cookieValue;

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService
  ) { }

  getReqUserTypes(): any {
    this.cookieService.set('persona', 'req');
    this.cookieValue = this.cookieService.getAll();
    return this.httpClient.get<LoginUser>(Constants.REST_API_URL + '/user')
      .catch((error: any) => Observable.throw(error.json().error ||
        'Server Error Occurred while getting --> Request <-- users'));
  }

  getMaintUserTypes(): any {
    this.cookieService.set('persona', 'maint');
    this.cookieValue = this.cookieService.getAll();
    return this.httpClient.get<LoginUser>(Constants.REST_API_URL + '/user')
      .catch((error: any) => Observable.throw(error.json().error ||
        'Server Error Occurred while getting --> Maint <--users'));
  }

  getBothUser(): any {
    this.cookieService.set('persona', 'both');
    this.cookieValue = this.cookieService.getAll();
    return this.httpClient.get<LoginUser>(Constants.REST_API_URL + '/user')
      .catch((error: any) => Observable.throw(error.json().error ||
        'Server Error Occurred while getting --> both <--users'));
  }
}


