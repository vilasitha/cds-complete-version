import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { LoginUser } from '../../shared/models/login-user';
import { OnInit } from '@angular/core';
import { Constants } from '../constants/constants';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';
import { RequestChecksModel } from '../../checktransactions/replace-or-void-checks/models/checks-request-backend';
import { DisbursementReqModel } from '../../checktransactions/disbursement/models/disbursement-backend';
import { AddPayeeInfoModel } from '../../maintenance/payee-table/add-payee/models/add-payee-info';

@Injectable()
export class SharedService {

    constructor() { }
    private checkFlagVar = new BehaviorSubject<string>('');
    public transactionsToDisplay = new BehaviorSubject<TransactionsResponse[]>([]);
    public payeeAddress = new BehaviorSubject<DisbursementReqModel[]>([]);
    public payeeInfoToDisplay = new BehaviorSubject<RequestChecksModel[]>([]);
    public paramClientId = new BehaviorSubject<string>('');
    private description = new BehaviorSubject<string>('');
    private checkType = new BehaviorSubject<string>('');
    private payeeCodeYOrN = new BehaviorSubject<string>('');
    private eftFlagVar = new BehaviorSubject<string>(null);

    currentDescCode = this.description.asObservable();
    currentPayeeAddress = this.payeeAddress.asObservable();
    currentPayeeInfo = this.payeeInfoToDisplay.asObservable();
    currentCheckFlag = this.checkFlagVar.asObservable();
    currentTransaction = this.transactionsToDisplay.asObservable();
    clientIdFromAPI = this.paramClientId.asObservable();
    currentCheckType = this.checkType.asObservable();
    currentPayeeCdFlag = this.payeeCodeYOrN.asObservable();
    eftFlagValue = this.eftFlagVar.asObservable();
    setPayeeAddForDisbursement(payeeAdd: DisbursementReqModel[]) {
        this.payeeAddress.next(payeeAdd);
    }

    setPayeeAddreesInfo(payeeAdd: RequestChecksModel[]) {
        this.payeeInfoToDisplay.next(payeeAdd);
    }

    setCheckFlag(flag: string) {
        this.checkFlagVar.next(flag);
    }

    setTransactionsToDisplay(transactions: TransactionsResponse[]) {
        this.transactionsToDisplay.next(transactions);
    }

    setClientId(id: string) {
        this.paramClientId.next(id);
    }

    setDescriptionCOde(code: string) {
        this.description.next(code);
    }

    setCheckType(type: string) {
        this.checkType.next(type);
    }

    setPayeeCodeYOrNFlag(type: string) {
        this.payeeCodeYOrN.next(type);
    }

    setEftFlag(flag: string) {
        this.eftFlagVar.next(flag);
    }

}