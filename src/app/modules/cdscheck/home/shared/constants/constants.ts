export class Constants {

    public static REST_API_URL = '/cdscheck/secure/rest';
    public static REST_REQ_API_URL = '/cdscheck/secure/rest/requests';
    public static REST_LOOKUP_URL = '/cdscheck/secure/rest/lookup';
    
    public static REST_MAINT_URL = '/cdscheck/secure/rest/maintenance';
    public static SEARCH = 'search';
    public static readonly CSCMAINT = 'CSCMAINT';
    public static METADATA_SOURCE_SYS ='/metadata/sourceSystems';
    public static SECURITY_GRP = 'securityGroups';

}