import { Component, OnInit, Input, HostListener, EventEmitter, OnDestroy } from '@angular/core';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: '[app-home]',
  templateUrl: './home.component.html',
  providers: [NGXLogger]
})
export class HomeComponent implements OnInit {

  constructor() {
  }
 
  ngOnInit() {
  }

}
