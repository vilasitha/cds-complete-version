import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import { NGXLogger } from 'ngx-logger';

import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { concat } from 'rxjs/operator/concat';
import { ReasonCodes } from '../../shared/models/reasons';
import { AltVoidAccounts } from '../../shared/models/voidAccounts';
import { States } from '../../shared/models/states';
import { MailCodes } from '../../shared/models/mailcodes';
import { SharedService } from '../../shared/services/shared.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';
import { ContainerDisbursementDataModel } from '../disbursement/models/reqDisbursement';
import { DisbursementService } from '../disbursement/disbursement.service';
import { Companies } from '../../shared/models/companies';
import { ReplaceOrVoidChecksService } from '../replace-or-void-checks/replace-or-void-checks.service';
import { ContainerReplaceOrVoidChecksModel, ReplaceOrVoidChecksModel } from '../replace-or-void-checks/models/replaceOrVoidChecksModel';
import { RequestChecksModel } from '../replace-or-void-checks/models/checks-request-backend';
import { CommonRestService } from '../services/common-rest-service.service';
import { ConfirmationService } from '../confirmchecks/confirmation.service';

@Component({
  selector: 'app-verify-check-info',
  templateUrl: './verify-check-info.component.html',
  styleUrls: ['./verify-check-info.component.css'],
  providers: [NGXLogger]
})
export class VerifyCheckInfoComponent implements OnInit {

  checkType: string;
  verifyCheckInfoForm: FormGroup;
  payeeState: States[] = [];
  reasonCodes: ReasonCodes[] = [];
  mailCodes: MailCodes[] = [];
  companyCode: Companies[];
  editPayeeInfo: RequestChecksModel[] = [];
  payeeResponse: any;
  errorMsgStatusFail = '';
  editClientId: string;
  verifyCheckPath: string;
  enabledOtherReason: boolean = false;
  enabledVoidToSection: boolean = false;
  edit_BankAccount;
  edit_BankRoutingNum;
  edit_zipCode;
  edit_zipCode5: string;
  edit_zipCode4: string;
  edit_PayeeCountry;
  edit_usOrIntRadioBtn;
  interAddFields : boolean= false;
  paymentAdminSystem;
  constructor(private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private dropdownService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService,
    private logger: NGXLogger,
    private confirmationService: ConfirmationService,
    private replaceOrVoidChecksService: ReplaceOrVoidChecksService,
    private commonRestService: CommonRestService) { }

  ngOnInit() {
    this.activeRoute.paramMap.subscribe(params => {
      this.verifyCheckPath = params.get('check');
    });
    this.activeRoute.queryParams.subscribe(params => { this.checkType = params['type']; });
    this.errorMsgStatusFail = '';
    this.activeRoute.queryParams.subscribe(params => { this.editClientId = params['clientId']; });
    this.enabledVoidToSection;
    this.displayReasonCodesDD();
    this.displayMailcodeDD();
    this.displayStatesDropdown();
    this.getPaymentAdminSystem();
    if (this.editClientId) {
      this.editVerifyCheckInfo();
    } else {
      this.setPayee();
    }
    this.buildVerifyCheckInfoForm(this.verifyCheckPath);
    this.verifyCheckInfoForm.get('paymentAdminSystem').disable();
    this.validateStateOrCountry(null);
  }

  buildVerifyCheckInfoForm(check: string): void {
    this.verifyCheckInfoForm = this.fb.group({
      payeeName: [this.payeeResponse.payeeName || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeName],
      amount: [this.payeeResponse.checkAmount || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.checkAmount],
      verifyDate: [this.payeeResponse.checkDate || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.checkDate],
      companyCode: [this.payeeResponse.companyCode || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.companyCode],
      description: [this.payeeResponse.description || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.descriptionCode],
      reasonCodes: [this.payeeResponse.reason || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason, [this.validationService.validateReasonCode]],
      mailCodes: [this.payeeResponse.mailCode || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.mailCode, [this.validationService.validateMailCodes]],
      originalCheck: [this.payeeResponse.originalCheckFlag || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckFlag, [this.validationService.validateOriginalCheck]],
      otherReason: [this.payeeResponse.reason || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason],
      payeeAddress1: [this.payeeResponse.payeeAddress1 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress1, [this.validationService.validatePayeeAddress]],
      payeeAddress2: [this.payeeResponse.payeeAddress2 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress2],
      payeeAddress3: [this.payeeResponse.payeeAddress3 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress3],      
      payeeAddress4:[this.payeeResponse.payeeAddress4 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress4],
      payeeCity: [this.payeeResponse.payeeCity || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeCity, [this.validationService.validatePayeeCity]],
      payeeState: [this.payeeResponse.payeeState || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeState, [this.validationService.validateStates]],
      payeeZip: [this.edit_zipCode5 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeZip, [this.validationService.validatePayeeZip]],
      payeeZipPlusFour: [this.edit_zipCode4 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeZipPlusFour],
      originalCheckBankAccount: [this.edit_BankAccount || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankAccount],
      originalCheckBankRouting: [this.edit_BankRoutingNum || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankRouting],
      usOrIntRadioBtn: [ this.edit_usOrIntRadioBtn || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddressIndicator ],
      country:[this.edit_PayeeCountry ||this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeCountry, [this.validationService.validateCountry]],
      paymentAdminSystem: [this.payeeResponse.paymentAdminSystem || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.paymentAdminSystem ]
    });
  }

  /** below method will set the payee address by calling company and check number service */
  setPayee() {
    this.sharedService.currentPayeeInfo.subscribe(
      (data) => {
        this.payeeResponse = data;
        const zipCode = this.payeeResponse.payeeZip;
        this.edit_PayeeCountry = this.payeeResponse.payeeCountry;
        this.validateForInternAdd(this.payeeResponse.payeeCountry);
        if(this.payeeResponse.bankAccountNumber){
          this.edit_BankAccount = this.payeeResponse.bankAccountNumber.trim();
        }
        if(zipCode){      
          this.edit_zipCode = zipCode.split('-');
          this.edit_zipCode5 = this.edit_zipCode[0].trim();
          this.edit_zipCode4 = this.edit_zipCode[1];
        } 

      }, (error: any) => {
        this.logger.error('Error Occurred while setting payee info', error);
        Observable.throw(error);
      }
    );
  }

  /** Onclick of previous button it will navigate to previous page */
  clickPrevious() {
    this.getPayeeInfoOnPrevious();
    if (this.checkType === 'Replace') {
      this.router.navigate(['home/replaceChecks', 'clientId'], { queryParams: { type: 'Replace', edit: 'rc', clientId: this.editClientId } });
    }
  }

  /** Home button will navigate to your transactions page */
  navigateToHome() {
    this.replaceOrVoidChecksService.reset();
    this.router.navigate(['home/yourTransactions']);
  }

  /**save of verify check information */
  onSubmit(form: FormGroup): void {
    this.validationService.validateUsOrIntFields(form);
    if (form.valid) {
      this.setVerifyCheckInfo();
      if (this.editClientId != null) {
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.clientId = this.editClientId;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.status = 'Complete';
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.sourceCode = this.payeeResponse.sourceCode;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankAccount = this.edit_BankAccount;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankRouting = this.edit_BankRoutingNum;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckNumber = this.payeeResponse.originalCheckNumber;
        this.setLocalStorage('localStoragePhoneNumber', this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.phoneNumber);                    
        this.replaceOrVoidChecksService.updateReplaceChecks(this.editClientId, this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel)
          .subscribe((result) => {
            this.logger.info('Update Replace checks data', result);
            this.replaceOrVoidChecksService.reset();
            this.router.navigate(['home/confirmation'], { queryParams: { type: 'Replace' } });

          }, (error: any) => {
            this.logger.error('Error Occurred while updating Replace checks', error);
            if (error.errorStat == '404') {
              this.errorMsgStatusFail = '404 not found';
            } else if (error.errorStat == '403') {
              this.errorMsgStatusFail = 'You’re not authorized to perform that action.';
            } else if (error.errorStat == '500') {
              this.errorMsgStatusFail = 'Your request cannot be completed at this time.  Please try again';
            } else if (error.errorStat == '400') {
              this.errorMsgStatusFail = error.errorText;
            }
            else if (error.errorStat == '401') {
              window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
            }
            else {
              this.errorMsgStatusFail = 'An error occurred while processing this request.';
            }
          });
      } else {
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.transactionType = 'Replacement';
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.status = 'Complete';
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankAccount = this.edit_BankAccount;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankRouting = this.edit_BankRoutingNum;
        this.setLocalStorage('localStoragePhoneNumber', this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.phoneNumber);        
        this.replaceOrVoidChecksService.saveReplaceChecks(this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel)
          .subscribe((result) => {
            this.logger.info('Save Replace checks data', result);
            this.replaceOrVoidChecksService.reset();
            this.router.navigate(['home/confirmation'], { queryParams: { type: 'Replace' } });

          }, (error: any) => {
            this.logger.error('Error Occurred while Saving Replace checks', error);
            if (error.errorStat == '404') {
              this.errorMsgStatusFail = '404 not found';
            } else if (error.errorStat == '403') {
              this.errorMsgStatusFail = 'You’re not authorized to perform that action.';
            } else if (error.errorStat == '500') {
              this.errorMsgStatusFail = 'Your request cannot be completed at this time.  Please try again';
            } else if (error.errorStat == '400') {
              this.errorMsgStatusFail = error.errorText;
            }
            else if (error.errorStat == '401') {
              window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
            }
            else {
              this.errorMsgStatusFail = 'An error occurred while processing this request.';
            }
          });
      }
    }
    else {
      this.validationService.validateAllFormFields(form);
    }
  } // end

  /** below method will call while editing existing repalce check */
  editVerifyCheckInfo() {
    this.sharedService.currentTransaction.subscribe(
      (response) => {
        this.payeeResponse = response;
        if(this.payeeResponse.originalCheckBankAccount){
          this.edit_BankAccount =  this.payeeResponse.originalCheckBankAccount.trim();
        }
        this.edit_PayeeCountry = this.payeeResponse.payeeCountry;
        this.validateForInternAdd(this.payeeResponse.payeeCountry);
        this.edit_BankRoutingNum =  this.payeeResponse.originalCheckBankRouting;
        this.edit_zipCode4 = this.payeeResponse.payeeZipPlusFour;
        this.edit_zipCode5 = this.payeeResponse.payeeZip;
        let zipCode: string
        if(this.edit_zipCode4){
          zipCode = this.payeeResponse.payeeZip + '-' + this.payeeResponse.payeeZipPlusFour.trim(); 
          this.edit_zipCode = zipCode.split('-');     
          this.edit_zipCode5 =  this.edit_zipCode[0];
          this.edit_zipCode4 = this.edit_zipCode[1];
        }else{
          zipCode = this.payeeResponse.payeeZip;
          this.edit_zipCode5 = zipCode;
        } 

      },
    (error: any) => {
      this.logger.error('Error occurred while editing verifyCheckInfo', error);
    });
  }

  /** below method will set the payee address information to model class from UI */
  setVerifyCheckInfo() {
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.paymentType = 'Check';
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeName = this.verifyCheckInfoForm.get('payeeName').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress1 = this.verifyCheckInfoForm.get('payeeAddress1').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress2 = this.verifyCheckInfoForm.get('payeeAddress2').value;
    if (this.verifyCheckInfoForm.get('payeeAddress3')) {
      this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress3 = this.verifyCheckInfoForm.get('payeeAddress3').value;
    } if (this.verifyCheckInfoForm.get('payeeAddress4')) {
      this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress4 = this.verifyCheckInfoForm.get('payeeAddress4').value;
    }
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeCity = this.verifyCheckInfoForm.get('payeeCity').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeState = this.verifyCheckInfoForm.get('payeeState').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeZip = this.verifyCheckInfoForm.get('payeeZip').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeZipPlusFour = this.verifyCheckInfoForm.get('payeeZipPlusFour').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.mailCode = this.verifyCheckInfoForm.get('mailCodes').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.checkAmount = this.verifyCheckInfoForm.get('amount').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.checkDate = this.verifyCheckInfoForm.get('verifyDate').value;
    if (this.verifyCheckInfoForm.get('companyCode').value) {
      this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.companyCode = this.verifyCheckInfoForm.get('companyCode').value;
    }
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.descriptionCode = this.verifyCheckInfoForm.get('description').value;
    if (this.verifyCheckInfoForm.get('reasonCodes').value) {
      this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason = this.verifyCheckInfoForm.get('reasonCodes').value;
    } else {
      if (this.verifyCheckInfoForm.get('otherReason').value) {
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason = this.verifyCheckInfoForm.get('otherReason').value;
      }
    }
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckFlag = this.verifyCheckInfoForm.get('originalCheck').value;
    if (this.verifyCheckInfoForm.get('originalCheck').value === 'Y') {
      this.confirmationService.setReplaceCheckMsg(true);
      this.confirmationService.setVoidChecktMsg(false);
      this.confirmationService.setDisbursementMsg(false);
      this.confirmationService.setInterfaceReport(true);
    } else if (this.verifyCheckInfoForm.get('originalCheck').value === 'N') {
      this.confirmationService.setVoidChecktMsg(false);
      this.confirmationService.setReplaceCheckMsg(true);
      this.confirmationService.setDisbursementMsg(false);
      this.confirmationService.setInterfaceReport(false);
    }
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeCountry = this.verifyCheckInfoForm.get('country').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.paymentAdminSystem = this.verifyCheckInfoForm.get('paymentAdminSystem').value;
  }

    /** render the states or country fields  */
    validateStateOrCountry(control: AbstractControl) {
      let stateOrCountry = this.verifyCheckInfoForm.get('usOrIntRadioBtn');
      if (stateOrCountry.value === 'U.S') {
        this.interAddFields = true;
        this.verifyCheckInfoForm.get('payeeAddress4').setValue(null);
        this.verifyCheckInfoForm.get('country').setValue(null);
      } else if (stateOrCountry.value === 'International') {
        this.interAddFields = false;
        this.verifyCheckInfoForm.get('payeeZipPlusFour').setValue(null);
        this.verifyCheckInfoForm.get('payeeCity').setValue(null);
        this.verifyCheckInfoForm.get('payeeZip').setValue(null);
        this.verifyCheckInfoForm.get('payeeState').setValue(null);
      }
    } // end

    getPaymentAdminSystem(){
      this.dropdownService.getSourceSystems().subscribe(
        (resp) => {
          this.paymentAdminSystem = resp;
        },
        (error: any) => {
          this.logger.error('Error Occurred while loading Payment Admin Systems dropdown', error.errorStat);
        });
    }

  /** display the values in reason to void Or replaace check dropdown **/
  displayReasonCodesDD(): any {
    this.dropdownService.getReasonsDropdown().subscribe((res) => {
      this.reasonCodes = res;
    }, (error: any) => {
      this.logger.info('Error Occurred while calling reasoncodes dropdown(VerifyCheckInfoComponent)', error);
    });
  }
  /** display the values in Mail code dropdown **/
  displayMailcodeDD(): any {
    this.dropdownService.getMailcodesDropdown().subscribe((res) => {
      this.mailCodes = res;
    }, (error: any) => {
      this.logger.info('Error Occurred while calling Mailcodes dropdown(VerifyCheckInfoComponent)', error);
    });
  }

  /** display the states dropdown **/
  displayStatesDropdown(): any {
    this.dropdownService.getStatesDropdown().subscribe((res) => {
      this.payeeState = res;
    },
      (error: any) => {
        this.logger.error('Error Occurred while calling states dropdown (VerifyCheckInfoComponent)', error);
      });
  }
  /**
   * On Change of dropdown value enable Other field.
   */
  onChangeOfReasons() {
    let control = this.verifyCheckInfoForm.get('reasonCodes');
    if (control.value === 'Other') {
      this.enabledOtherReason = true;
    } else {
      this.enabledOtherReason = false;
    }
  }

  validateForInternAdd(country){
    if(country && country.trim()){
      this.interAddFields = true;
      this.edit_usOrIntRadioBtn= 'International';
    }else{
      this.interAddFields = false;
      this.edit_usOrIntRadioBtn= 'U.S';
    }
  }

  getPayeeInfoOnPrevious(){
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason = this.verifyCheckInfoForm.get('reasonCodes').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckFlag = this.verifyCheckInfoForm.get('originalCheck').value;    
  }
  
  getChecksDataModel(): ContainerReplaceOrVoidChecksModel {
    return this.replaceOrVoidChecksService.containerChecksModel;
  }

  setChecksDataModel(containerDataModel: ContainerReplaceOrVoidChecksModel) {
    this.replaceOrVoidChecksService.containerChecksModel = containerDataModel;
  }

  setLocalStorage(key, value) {
    localStorage.setItem(key, value);
  }

}
