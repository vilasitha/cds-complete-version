import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyCheckInfoComponent } from './verify-check-info.component';

describe('VerifyCheckInfoComponent', () => {
  let component: VerifyCheckInfoComponent;
  let fixture: ComponentFixture<VerifyCheckInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyCheckInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyCheckInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
