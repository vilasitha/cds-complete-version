import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';


export class ValidationService {
  private regex: RegExp = new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g);
  static getValidationErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      'required': 'Value is required.',
      'checkNumRequired': 'Check Number is required',
      'minLength': 'Check Numbers must be 11 digits.',
      'companyNamesValidation': 'Company Name/Bank Information is required.',
      'pattern': 'Check number should be numbers only',
      'date.format.invalid': 'Date should be of format MM/DD/YYYY',
      'companyCodesValidation': 'Company name is required.',
      'sourceCodeValidation': 'Source code is required.',
      'stateCode': 'State is required.',
      'payeeName': 'Payee is required.',
      'mailCode': 'Mail Code is required.',
      'payeeAddress': 'Address is required.',
      'payeeDesc': 'Description is required.',
      'payeeCity': 'City is required.',
      'payeeZip': 'Zipcode is required.',
      'payeeZipPlusFour': 'Zipcode plus four is required.',
      'reasonCode': 'Need to select a valid reason.',
      'originalCheck': 'Please check Yes/No to having Original Check.',
      'payeeCode': 'Payee code is required.',
      'accountMandatory': ' Account Number is required',
      'accountNumeric': 'Only numbers allowed',
      'totalAmnt': 'Total Check Amount is required.',
      'accDetailsamount': 'Amount is required .',
      'zipCodeNumeric': 'Only numbers allowed for ZipCode',
      'policyNumber': 'Policy number is required.',
      'country': 'Country is required.',
      'routingNumberValidation': 'Bank Routing Number is required.',
      'accNumberValidation': 'Bank Account Number is required.',
      'accountTypeValidation': 'Account Type is required.',
      'payeeCodeYesOrNo': 'Do you have a Payee code?',
      'taxId': 'Tax ID is required and must contain 9 characters.',
      'distribCode': 'Distribution code is required.',
      'legacyCode': 'Legacy Source Code is required.',
      'routingErrorMsg': 'Invalid routing number.',
      'companyCodes': 'Company code is required.',
      'maxAmount': 'The Amount is too big, the maximum allowable amount is 99999999.99',
      'transactionCode': 'Transaction code is required.',
      'routingNumLength': 'Bank routing number must be 9 digits.',
      'invalidTaxId': 'Tax ID is invalid.',
      'taxIdLength': 'Tax ID must contain 9 characters.',
      'invalidAccountNumber': 'Company Code and Account Number combination is Invalid.',
      'errorOccuredAccountNumber': 'An error occurred while processing this request.',
      'tabCharErrorMsg': 'Invalid characters have been entered in the field, please re-type details.',
      'adminSysErrorMsg':'Admin System is required.',
      'secGrpErrorMsg':'Security Group is required.',
      'invalidSecurityGroup': 'Security Group entered is not valid',
      'paymentAdminSys': 'Payment Admin System is required.'

    };
    return config[validatorName];
  }

  public checkNumberValidation(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'checkNumRequired': true };
    } else if (control.dirty && control.value.length < 11) {
      return { 'minLength': true };
    }
  }

  /**Checking company names dropdown value with default text */
  public companyNamesValidation(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'companyNamesValidation': true };
    }
    return null;
  }

  /**Validating Company names for request disbursement */
  public validateCompanyName(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '-1' || control.value === '' || control.value == null) {
      return { 'companyCodesValidation': true };
    }
    return null;
  }

  /**Validating source code. */
  public validateSourceCode(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == null || control.value === '') {
      return { 'sourceCodeValidation': true };
    }
    return null;
  }

  /** Validating state codes. */
  public validateStates(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '-1' || control.value == null || control.value === '') {
      return { 'stateCode': true };
    }
    return null;
  }

  /** Validating state Names. */
  public validateStateNames(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '-1' || control.value === '' || control.value == null) {
      return { 'stateCode': true };
    }
    return null;
  }

  /**Validating mail codes dropdown. */
  public validateMailCodes(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == null) {
      return { 'mailCode': true };
    }
    return null;
  }
  /**disbursement payee adresss fields validation begin */
  public validatePayeeName(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == null || control.value === '') {
      return { 'payeeName': true };
    }
    if (control.value != null && /\t/.test(control.value)) {
      return { 'tabCharErrorMsg': true };
    }
    return null;
  }
  public validatePayeeAddress(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'payeeAddress': true };
    }
    if (control.value != null && /\t/.test(control.value)) {
      return { 'tabCharErrorMsg': true };
    }
    return null;
  }
  
  public validatePayeeCity(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'payeeCity': true };
    }
    return null;
  }
  public validatePayeeDescription(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'payeeDesc': true };
    }
    return null;
  }

  public validatePayeeZip(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'payeeZip': true };
    } else {
      let numbers = /^[0-9]+$/;

      if (!(/^[-+]?\d*\.?\d*$/.test(control.value))) {
        return { 'zipCodeNumeric': true };
      } else {
        return null;
      }

    }
  }

  /**End payee adresss fields validation  */

  public validateReasonCode(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null || control.value.startsWith('Please Select Reason')) {
      return { 'reasonCode': true };
    }
    return null;
  }

  public validateOriginalCheck(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'originalCheck': true };
    }
    return null;
  }
  public validatePayeeCode(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'payeeCode': true };
    }
    return null;
  }
  public validateAnyFiled(control: FormControl): { [key: string]: boolean } | null {
    if ((control.touched && control.value == null)) {
      return { 'required': true };
    }
    return null;
  }
  public validateAccountMandaotryFields(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '') {
      return { 'accountMandatory': true };
    }
    return null;
  }
  public validateTotalCheckAmount(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'totalAmnt': true };
    }
    return null;
  }
  public validateAccDetailsAmount(control: FormControl): { [key: string]: boolean } | null {
    if (control.value !== '' || control.value !== null) {
      if (control.value > '99999999.99') {
        return { 'maxAmount': true };
      }
    }
    return null;
  }
  public validatePolicyNumber(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'policyNumber': true };
    }
    return null;
  }
  public validateCountry(control: FormControl): { [key: string]: boolean } | null {
    if ((control.touched && control.dirty) && control.value === '' || control.value == null) {
      return { 'country': true };
    }
    return null;
  }
  public fieldNumericValidation(control: FormControl): { [key: string]: boolean } | null {
    const numbers = /^[0-9]+$/;
    if (!(/^[-+]?\d*\.?\d*$/.test(control.value))) {
      return { 'accountNumeric': true };
    } else {
      return null;
    }
  }

  /**Validating Bank Routing Number for request disbursement */
  public validateRoutingNumber(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == null || control.value == '') {
      return { 'routingNumberValidation': true };
    } else if (control.dirty && control.value.length < 9) {
      return { 'routingNumLength': true };
    }
    else {
      let numbers = /^[0-9]+$/;
      if (!(/^[-+]?\d*\.?\d*$/.test(control.value))) {
        return { 'accountNumeric': true };
      } else {
        return null;
      }
    }
  }

  validateAccoutDetails(formGroup: FormGroup) {
    let accControler = formGroup.get('accountNumber');
    let amountControler = formGroup.get('amount');
    let policyControler = formGroup.get('policyNumber');
    if (accControler.value) {
      if (!amountControler.value) {// no value in amount field
        amountControler.markAsTouched({ onlySelf: true });
        amountControler.setErrors({
          'accDetailsamount': true
        });
      }
      if (!policyControler.value) {
        policyControler.markAsTouched({ onlySelf: true });
        policyControler.setErrors({
          'policyNumber': true
        });
      }
    }
    else {
      if (!amountControler.value) {// no value in amount field
        amountControler.setErrors(null);
        accControler.setErrors(null);
        policyControler.setErrors(null);
      }
      else {
        let numbers = /^[0-9]+$/;
        if ((/^[-+]?\d*\.?\d*$/.test(amountControler.value))) {
          accControler.markAsTouched({ onlySelf: true });
          accControler.setErrors({
            'accountMandatory': true
          });
        }
      }
    }
  }

  /**Validating Company names for request disbursement */
  public validateAccountType(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'accountTypeValidation': true };
    }
    return null;
  }

  /**Validating Bank account number for request disbursement */
  public validateAccountNumber(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == null || control.value == '') {
      return { 'accNumberValidation': true };
    } else {
      const numbers = /^[0-9]+$/;

      if (!(/^[-+]?\d*\.?\d*$/.test(control.value))) {
        return { 'accountNumeric': true };
      } else {
        return null;
      }
    }

  }

  /**Validating do you have a payee code for request disbursement */
  public validatePayeeYesOrNo(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == null || control.value == '') {
      return { 'payeeCodeYesOrNo': true };
    }
    return null;
  }

  public validateDistribCode(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'distribCode': true };
    }
    return null;
  }
  validateUsOrIntFields(formGroup: FormGroup) {
    const usOrIntRadioBtn = formGroup.get('usOrIntRadioBtn');
    const payeeState = formGroup.get('payeeState');
    const payeeCity = formGroup.get('payeeCity');
    const payeeZip = formGroup.get('payeeZip');
    const payeeZipPlus = formGroup.get('payeeZipPlusFour');
    const country = formGroup.get('country');
    if (usOrIntRadioBtn.value == 'International') {
      payeeState.setErrors(null);
      payeeState.markAsTouched();
      payeeCity.setErrors(null);
      payeeCity.markAsTouched();
      payeeZip.setErrors(null);
      payeeZip.markAsTouched();
      payeeZipPlus.setErrors(null);
      payeeZipPlus.markAsTouched();

    } else {
      if (usOrIntRadioBtn.value == 'U.S') {
        country.setErrors(null);
        country.markAsTouched();
      }
    }
  }

  validateDisbursementAllFormFields(formGroup: FormGroup) {
    const checkOrEftRadioFlag = formGroup.get('checkOrEftRadioFlag');
    const payeeCodeFlag = formGroup.get('payeeCodeFlag');
    const routingNumber = formGroup.get('routingNumber');
    const accountNumber = formGroup.get('accountNumber');
    const accountType = formGroup.get('accountType');
    const payeeCode = formGroup.get('payeeCode');
    if (checkOrEftRadioFlag.value == 'EFT') {
      if (payeeCodeFlag.value == 'Y') {
        if (payeeCode.value == '' || payeeCode.value == null) {
          payeeCode.setErrors({
            'payeeCode': true
          });
        }
        routingNumber.setErrors(null);
        routingNumber.markAsTouched();
        accountNumber.setErrors(null);
        accountNumber.markAsTouched();
        accountType.setErrors(null);
        accountType.markAsTouched();
      } else {
        if (payeeCodeFlag.value == 'N') {
          payeeCode.setErrors(null);
          payeeCode.markAsTouched();
        }
        payeeCode.setErrors(null);
        payeeCode.markAsTouched();
      }
    } else {
      payeeCodeFlag.setErrors(null);
      payeeCodeFlag.markAsTouched();
      routingNumber.setErrors(null);
      routingNumber.markAsTouched();
      accountNumber.setErrors(null);
      accountNumber.markAsTouched();
      accountType.setErrors(null);
      accountType.markAsTouched();
      payeeCode.setErrors(null);
      payeeCode.markAsTouched();
    }

  }

  validateTaxId(formGroup: FormGroup) {
    const source = formGroup.get('sourceCode');
    const taxId = formGroup.get('taxId');
    const distribCode = formGroup.get('distributionCode');

    if (source.value == 'X01' || source.value == 'X0109' || source.value == 'X01I') {
      if (taxId.value == '' || taxId.value == null) {
        taxId.setErrors({
          'taxId': true
        });
      }
      else {
        this.validateTaxIdWithSSNRules(formGroup);
      }
      if (distribCode.value == '' || distribCode.value == null) {
        distribCode.setErrors({
          'distribCode': true
        })
      } else {
        distribCode.setErrors(null);
        distribCode.markAsTouched();
      }
    } else {

      this.validateTaxIdWithSSNRules(formGroup);
      distribCode.setErrors(null);
      distribCode.markAsTouched();
    }

  }

  validRoutingNumber(formGroup: FormGroup) {
    const routingNumberVal = formGroup.get('routingNumber');
    if (routingNumberVal.value !== null) {
      var checksumTotal = (7 * (parseInt(routingNumberVal.value.charAt(0), 10) + parseInt(routingNumberVal.value.charAt(3), 10) + parseInt(routingNumberVal.value.charAt(6), 10))) +
        (3 * (parseInt(routingNumberVal.value.charAt(1), 10) + parseInt(routingNumberVal.value.charAt(4), 10) + parseInt(routingNumberVal.value.charAt(7), 10))) +
        (9 * (parseInt(routingNumberVal.value.charAt(2), 10) + parseInt(routingNumberVal.value.charAt(5), 10) + parseInt(routingNumberVal.value.charAt(8), 10)));
      var checksumMod = checksumTotal % 10;
      if (checksumMod !== 0) {
        routingNumberVal.setErrors({
          'routingErrorMsg': true
        });

      } else {
        routingNumberVal.setErrors(null);
        routingNumberVal.markAsTouched();
      }
    }
  }

  /**
   * 
   * @param formGroup To validate all the fields in form
   */
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  /**Validating legacy source code. */
  public validatelegacyCode(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == null || control.value === '') {
      return { 'legacyCode': true };
    }
    return null;
  }

  public companyCodeValidation(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'companyCodes': true };
    }
    return null;
  }

  public validateTransactionCode(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'transactionCode': true };
    }
    return null;
  }

  validatePayeeAddressforEFT(formGroup: FormGroup) {
    const payeeAddress1 = formGroup.get('payeeAddress1');
    const payeeCity = formGroup.get('payeeCity');
    const payeeState = formGroup.get('payeeState');
    const payeeZip = formGroup.get('payeeZip');
    const description1 = formGroup.get('description1');

    payeeAddress1.markAsTouched();
    payeeAddress1.setErrors(null);
    payeeCity.markAsTouched();
    payeeCity.setErrors(null);
    payeeState.markAsTouched();
    payeeState.setErrors(null);
    payeeZip.markAsTouched();
    payeeZip.setErrors(null);
    description1.markAsTouched();
    description1.setErrors(null);
  }

  validateTaxIdWithSSNRules(formGroup: FormGroup) {
    const taxId = formGroup.get('taxId');
    if (taxId.value) {
      if (taxId.value && taxId.value.length < 9) {
        taxId.setErrors({ 'taxIdLength': true });
      }
      else {
        taxId.setErrors(null);
        taxId.markAsTouched();
      }
    }
    else {
      taxId.setErrors(null);
      taxId.markAsTouched();
    }
  }

  public validateAddess(control: FormControl): { [key: string]: boolean } | null {
    if (control.value != null && /\t/.test(control.value)) {
      return { 'tabCharErrorMsg': true };
    }
    return null;
  }

  public validateSecurityGrp(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'secGrpErrorMsg': true };
    }
    return null;
  }
  public validateAdminSys(control: FormControl): { [key: string]: boolean } | null {
    if ( control.value == null || control.value.startsWith('Select a Value') || control.value === '' || control.value == '-1') {
      return { 'adminSysErrorMsg': true };
    }
    return null;
  }
  public validatePaymentAdminSys(control: FormControl): { [key: string]: boolean } | null {
    if (control.value === '' || control.value == null) {
      return { 'paymentAdminSys': true };
    }
    return null;
  }
  
}