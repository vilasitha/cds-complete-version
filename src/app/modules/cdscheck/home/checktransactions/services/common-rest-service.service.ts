import { Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Constants } from '../../shared/constants/constants';

import { HttpErrorHandler, HandleError } from '../error-messages/services/http-error-handler.service';
import { Observable } from 'rxjs/Rx';
import { RequestChecksModel } from '../replace-or-void-checks/models/checks-request-backend';
import { DisbursementReqModel } from '../disbursement/models/disbursement-backend';
import { Response } from '@angular/http/src/static_response';
import { ResponseOptions } from '@angular/http';
import { HttpEvent } from '@angular/common/http/src/response';

@Injectable()
export class CommonRestService {

  urlCompanies = 'companies';
  urlChecks = 'checks'
  urlDescriptions = 'descriptions';
  urlPayees = 'payees';
  urlSources = 'sources';
  urlAccounts = 'accounts';


  constructor(private httpClient: HttpClient,
  ) { }

  getPayeeInfoForDisbursement(companyCode: string, sourceCode: string, payeeCode: string): Observable<any> {
    const url = `${Constants.REST_LOOKUP_URL}/${this.urlCompanies}/${companyCode}/${this.urlSources}/${sourceCode}/${this.urlPayees}/${payeeCode}`;
    return this.httpClient.get<DisbursementReqModel>(url)
      .catch(this.handleError)
  }

  validateDescriptionCode(descriptioncode: string): any {
    const url = `${Constants.REST_LOOKUP_URL}/${this.urlDescriptions}/${descriptioncode}`;
    return this.httpClient.get(url)
      .map((response: Response) => {
        return response;
      })._catch(this.handleError);
  }
  validatePayeeCode(payeecode: string): any {
    const url = `${Constants.REST_LOOKUP_URL}/${this.urlPayees}/${payeecode}`;
    return this.httpClient.get(url)
      .map((response: Response) => {
      })._catch(this.handleError);
  }

  validateSecurityGrp(securityGrp: string): any {
    const url = `${Constants.REST_LOOKUP_URL}/${Constants.SECURITY_GRP}/${securityGrp}`;
    return this.httpClient.get(url)
      .map((response: Response) => {})._catch(this.handleError);
  }
  validateSouceCode(sourcecode: string): any {
    const url = `${Constants.REST_LOOKUP_URL}/${this.urlSources}/${sourcecode}`;
    return this.httpClient.get(url)
      .map((response: Response) => {
      })
      ._catch(this.handleError);
  }
  
  validateCompanyAndSouceCode(companyCode:string, sourcecode: string): any {
    const url = `${Constants.REST_LOOKUP_URL}/${this.urlCompanies}/${companyCode}/${this.urlSources}/${sourcecode}`;
    return this.httpClient.get(url)
      .map((response: Response) => {
      })
      ._catch(this.handleError);
  }
  companyAndAccountValidation(companyCode: string, accountNum: string): any {
    const url = `${Constants.REST_LOOKUP_URL}/${this.urlCompanies}/${companyCode}/${this.urlAccounts}/${accountNum}`;
    return this.httpClient.get(url)
    .map((response: Response) => {
    })
    ._catch(this.handleError);
  }

  getPayeeInfo(company: string, checkNum: string): Observable<any> {
    const url = `${Constants.REST_LOOKUP_URL}/${this.urlCompanies}/${company}/${this.urlChecks}/${checkNum}`;
    return this.httpClient.get<RequestChecksModel>(url)._catch(this.handleErrorTest);
  }

  validateFields(urls: Array<any>): Observable<any> {
    const baseUrl = `${Constants.REST_LOOKUP_URL}`;
    let resultantAray: Array<any> = new Array();
    let httpCalls: Array<any> = new Array();
    urls.forEach(element => {
      httpCalls.push(this.httpClient.get(baseUrl + '/' + element.urlPath + '/' + element.value)
        .catch(err => {
          return Observable.of(err.error);
        }))
    })
    return Observable.forkJoin(httpCalls)
      .map((data: any[]) => {
        for (let i = 0; i < data.length; i++) {
          urls[i].result = data[i];
        }
        return urls;
      }).catch(this.handleError);;

  }
  private handleErrorTest(error: any) {
    let errorObj: any = {
      errorMessage: error.message,
      errorStat: error.status,
      errorText: error.error ? error.error[0].code : error.statusText
      
    }
    return Observable.throw(errorObj);
  }

  private handleError(error: any) {
    let errorObj: any = {
        errorMessage: error.message,
        errorStat: error.status,
        errorText: error.statusText
    };
    return Observable.throw(errorObj);
}
}
