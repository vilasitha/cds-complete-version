import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Constants } from '../../shared/constants/constants';
import { OnInit } from '@angular/core';
import { Companies } from '../../shared/models/companies';
import { States } from '../../shared/models/states';
import { MailCodes } from '../../shared/models/mailcodes';
import { ReasonCodes } from '../../shared/models/reasons';
import { AltVoidAccounts } from '../../shared/models/voidAccounts';
import { BankNames } from '../../shared/models/bank-names';
import { DistributionCodes } from '../../shared/models/distributionCodes';
import { SourceSytems } from '../../maintenance/source-code/models/sourceSystems';

@Injectable()
export class CommondropdownService implements OnInit {
  private serviceUrl = '/metadata/companies';
  private statesServiceUrl = '/metadata/states';
  private mailcodesServiceUrl = '/metadata/mailCodes';
  private reasonServiceUrl = '/metadata/reasons';
  private voidAccountsServiceUrl = '/metadata/alternateVoidAccounts';
  private bankNamesUrl = '/metadata/banks';
  private distributionCodesUrl = '/metadata/distributionCodes';
  private accountTypesServiceUrl = '/assets/data/accountTypes.json';

  constructor(
    private httpClient: HttpClient) { }

  ngOnInit() { }

  /**
   * Below method willget the company/bank names.
   */
  getCompanyNames(): any {
      return this.httpClient.get<Companies>(Constants.REST_API_URL + this.serviceUrl)._catch(this.handleError);
  }
  /**
   * Below method willget the bank names.
   */
  getBanksNames(): any {
      return this.httpClient.get<BankNames>(Constants.REST_API_URL + this.bankNamesUrl)._catch(this.handleError);
  }

  /**
* Below method is to get state codes dropdown.
*/
  public getStatesDropdown(): any {
    return this.httpClient.get<States>(Constants.REST_API_URL + this.statesServiceUrl)._catch(this.handleError);
  }

  /**
   * Below method is to get mail codes dropdown.
   */
  public getMailcodesDropdown(): any {
    return this.httpClient.get<MailCodes>(Constants.REST_API_URL + this.mailcodesServiceUrl)
      .catch(this.handleError);
  }

  /**Below method is to get reason for void or replace code dropdown. */
  public getReasonsDropdown(): any {   
      return this.httpClient.get<ReasonCodes>(Constants.REST_API_URL + this.reasonServiceUrl)
        .catch(this.handleError);  
  }

  /**get Void To Acoount number dropdown */
  public getVoidToDropdown(): any { 
      return this.httpClient.get<AltVoidAccounts>(Constants.REST_API_URL + this.voidAccountsServiceUrl)
        .catch(this.handleError);
  }

  /**get distributionCodes dropdown */
  public getDistributionCodes(): any {
    return this.httpClient.get<DistributionCodes>(Constants.REST_API_URL + this.distributionCodesUrl)
      ._catch(this.handleError);

  }
  
  getSourceSystems(): any{
    return this.httpClient.get<SourceSytems>(Constants.REST_API_URL + Constants.METADATA_SOURCE_SYS).catch(this.handleError);
  }
  private handleError(error: any) {
    let errorObj: any = {
      errorMessage: error.message,
      errorStat: error.status,
      errorText: error.statusText
    };
    return Observable.throw(errorObj);
  }

}
