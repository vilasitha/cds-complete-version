import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ElementRef } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { SharedService } from '../../shared/services/shared.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';
import { RequestDisbursementModel } from '../disbursement/models/reqDisbursement';
import { DisbursementService } from '../disbursement/disbursement.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DialogueService } from '../../services/dialogue.service';
import { ConfirmDialogueComponent } from '../../confirm-dialogue/confirm-dialogue.component';
import { States } from '../../shared/models/states';
import { CommondropdownService } from '../services/commondropdown.service';
import { MailCodes } from '../../shared/models/mailcodes';

@Component({
  selector: 'app-view-disbursement',
  templateUrl: './view-disbursement.component.html',
  styleUrls: ['./view-disbursement.component.css']
})
export class ViewDisbursementComponent implements OnInit {
  urlClientId: string;
  viewDisbursementForm: FormGroup;
  states: States[] = [];
  mailCodes: MailCodes[]
  transactions: any;
  errorMsg = '';
  totalAmount: any;
  data_debitCreditFlag;
  data_accounNum;
  data_accountAmount: any;
  data_policy;
  data_ssnOrSuspence;
  data_alpha;
  data_accDetailsList: any;
  data_phoneNumber;
  enableAccountDtl: boolean = false;
  enableDeleteFlag: boolean = false;
  enableAddressFields: boolean = false;
  enableIntAdd: boolean= false;

  constructor(private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private logger: NGXLogger,
    private sharedService: SharedService,
    private disbursementService: DisbursementService,
    private dialogueService: DialogueService,
    private commonDropdownService: CommondropdownService) {

  }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => { this.urlClientId = params['clientId']; });
    this.errorMsg = '';
    this.enableDeleteFlag;
    this.enableAddressFields;
    this.enableIntAdd;
    this.getStates();
    this.displayMailCodesDropdown();
    this.getDisbursementTransInfo();
    this.viewBuildForm();
    this.viewDisbursementForm.get('viewStateCode').disable();
    this.viewDisbursementForm.get('viewMailCode').disable();
  }

  viewBuildForm(): void {
    this.viewDisbursementForm = this.fb.group({
      viewCompanyCode: this.transactions.companyCode || '--NA--',
      viewSourceCode: this.transactions.sourceCode || '--NA--',
      viewPhoneNumber: this.data_phoneNumber || '--NA--',
      viewPayeeCode: this.transactions.payeeCode || '--NA--',
      viewPayeeName: this.transactions.payeeName || '--NA--',
      viewPayeeAddress: this.transactions.payeeAddress1 || '--NA--',
      viewPayeeAddress2: this.transactions.payeeAddress2 || '--NA--',
      viewPayeeAddress3: this.transactions.payeeAddress3 || '--NA--',
      viewPayeeAddress4: this.transactions.payeeAddress4 || '--NA--',
      viewPayeeCity: this.transactions.payeeCity || '--NA--',
      viewPayeeState: this.transactions.payeeState || '--NA--',
      viewPayeeZip: this.transactions.payeeZip || '--NA--',
      viewAmount: this.totalAmount || '--NA--',
      viewCheckType: this.transactions.paymentType || '--NA--',
      viewDescription: this.transactions.description1 || '--NA--',
      viewDescription2: this.transactions.description2 || '--NA--',
      viewStateCode: this.transactions.stateCode || '--NA--',
      viewMailCode: this.transactions.mailCode || '--NA--',
      viewTaxId: this.transactions.taxId || '--NA--',
      viewAccountingArray: this.fb.array([
        this.getAccountDetails(),
      ])
    });
  }

  getAccountDetails() {
    return this.fb.group({
      accountNumber: this.data_accounNum || '--NA--',
      accountAmount: this.data_accountAmount || '--NA--',
      policyNumber: this.data_policy || '--NA--',
      suspenceNumber: this.data_ssnOrSuspence || '--NA--',
      alpha: this.data_alpha || '--NA--',
      debitCreditFlag: this.data_debitCreditFlag || '--NA--'
    });
  }

  /** Onclick of print button below method will call */
  onPrint() {
    window.print();
  }

  /** Onclick of home button will navigate to Home */
  onClickHome() {
    this.router.navigate(['home/yourTransactions']);
  }

  /** By calling below method record will be deleted */
  delete(urlClientId) {
    const navigationExtras: NavigationExtras = { queryParams: { 'clientId': this.urlClientId } };
    const componentRef = this.dialogueService.openNewDialog(ConfirmDialogueComponent);
    if (componentRef && componentRef.instance) {
      componentRef.instance.setDetails({
        title: 'Confirm',
        message: 'Are you sure you want to delete this record (OK=yes Cancel=no)'
      });
      componentRef.instance.onClose.subscribe(
        isConfirmed => {
          this.dialogueService.closeComponent(componentRef);
          if (isConfirmed) {
            this.disbursementService.deleteDisbursement(this.urlClientId).subscribe(
              data => {
                this.logger.info('deleted data: ==>>', data);
                this.router.navigate(['home/yourTransactions']);
              },
              (error: any) => {
                this.logger.error('Error Occurred while deleting checks', error.errorStat);
                if (error.errorStat == '404') {
                  this.errorMsg = '404 not found';
                } else if (error.errorStat == '403') {
                  this.errorMsg = 'You’re not authorized to perform that action.';
                } else if (error.errorStat == '500') {
                  this.errorMsg = 'Your request cannot be completed at this time.  Please try again';
                } else if (error.errorStat == '401') {
                  window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
                }
                else {
                  this.errorMsg = 'An error occurred while processing this request.';
                }
              }
            );
          } else {
            this.router.navigate(['home/viewDisbursement'], navigationExtras);
          }
        },
        (error: any) => {
          this.logger.error('Error Occurred while deleting disbursement', error);
        }
      );
    }
  }

  /** below method will display the transaction */
  getDisbursementTransInfo() {
    this.sharedService.currentTransaction.subscribe(
      (response) => {
        this.transactions = response;
        this.showAddForInternational(this.transactions.payeeCountry);
        this.showAddFieldsForCheck(this.transactions.paymentType);
        this.data_accDetailsList = this.transactions.accountingDetails;
        this.data_phoneNumber= this.transactions.phoneNumber;
        this.hideDeleteBtn(this.transactions.status);
        if (this.data_accDetailsList != null && this.data_accDetailsList.length > 0) {
          this.enableAccountDtl = true;
          let creditSum: number = 0;
          let debitSum : number = 0;
          for (const i of this.data_accDetailsList) {
            this.data_accounNum = i.accountNumber;
            this.data_accountAmount = i.amount;
            this.data_alpha = i.alpha;
            this.data_policy = i.policyNumber;
            this.data_ssnOrSuspence = i.suspenseNumber;
            this.data_debitCreditFlag = i.debitCreditFlag;    
            if(this.data_debitCreditFlag == 'D'){
             debitSum = this.transactions.accountingDetails.filter((item) => item.debitCreditFlag == 'D')
              .map((item) => +item.amount)
              .reduce((sum, current) => sum + current);
            }
              if(this.data_debitCreditFlag == 'C'){
                creditSum = this.transactions.accountingDetails.filter((item) => item.debitCreditFlag == 'C')
                .map((item) => +item.amount)
                .reduce((sum, current) => sum + current);
              }      
            // const total = this.transactions.accountingDetails.reduce((sum, item) => sum + item.amount, 0);       
          }
          const total = (debitSum - creditSum).toFixed(2);
          this.totalAmount = parseFloat(total).toFixed(2);
        } else {
          this.enableAccountDtl = false;
        }
      }, (error: any) => {
        this.logger.error('Error Occurred while getting transaction in getDisbursementTransInfo', error);
      }
    );
  }

  hideDeleteBtn(status){
    if(status == 'Processed'){
      this.enableDeleteFlag = false;
    }else{
      this.enableDeleteFlag = true;
    }        
  }

  /** get the values in states dropdown **/
  getStates() {
    this.commonDropdownService.getStatesDropdown().subscribe((response) => {
      this.states = response;
    },
      (error: any) => {
        this.logger.error('Error in loading states dropdown', error);
      });
  }

  /** display the values in Mail code dropdown **/
  displayMailCodesDropdown() {
    this.commonDropdownService.getMailcodesDropdown().subscribe((response) => {
      this.mailCodes = response;
    },
      (error: any) => {
        this.logger.error('Error in loading MailCodes dropdown', error);
      });
  }

  // editmethod to navigate to edit request disbursement method
  onSubmit(i) {
    const navigationExtras: NavigationExtras = { queryParams: { 'clientId': this.urlClientId } };
    this.router.navigate(['home/requestDisbursement', 'payee'], navigationExtras);
  }

  showAddForInternational(payeeCountry){
    if(payeeCountry !=null ){
      this.enableIntAdd = true;
    }else{
      this.enableIntAdd = false;
    }
  }

  showAddFieldsForCheck(checkType){
    if(checkType == 'Check'){
      this.enableAddressFields=true;
    }else{
      this.enableAddressFields = false;
    }
  }


}
