import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDisbursementComponent } from './view-disbursement.component';

describe('ViewDisbursementComponent', () => {
  let component: ViewDisbursementComponent;
  let fixture: ComponentFixture<ViewDisbursementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDisbursementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDisbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
