import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params, NavigationExtras } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';
import { SharedService } from '../../shared/services/shared.service';
import { NGXLogger } from 'ngx-logger';
import { HomeService } from '../../services/home.service';
import { ReplaceOrVoidChecksService } from '../replace-or-void-checks/replace-or-void-checks.service';
import { DialogueService } from '../../services/dialogue.service';
import { ConfirmDialogueComponent } from '../../confirm-dialogue/confirm-dialogue.component';
import { BankNames } from '../../shared/models/bank-names';

@Component({
  selector: 'app-view-replace-or-void-checks',
  templateUrl: './view-replace-or-void-checks.component.html',
  styleUrls: ['./view-replace-or-void-checks.component.css'],
  providers: [NGXLogger]
})
export class ViewReplaceOrVoidChecksComponent implements OnInit {
  private static readonly TRANS_TYPE_R = 'Replacement';
  private static readonly TRANS_TYPE_V = 'Void';
  bankNames: BankNames[] = [];
  enableReasonLabel: boolean = false;
  enableReasonVLabel: boolean = false;
  enableMailCode: boolean = false;
  interAddress4: boolean = false;
  enableAccountDtl: boolean = false;
  urlParam: string;
  errorMsg = '';
  data_transactionType;
  data_companyName;
  data_bankName;
  data_viewCheckNumber;
  data_viewDateOfTrans;
  data_viewPayee;
  data_viewAdd1;
  data_viewAdd2;
  data_viewAdd3;
  data_viewAdd4;
  data_viewCity;
  data_ViewState;
  data_viewZipCode;
  data_viewSourceCode;
  data_mailCode;
  data_viewAmount = '';
  data_viewDesc;
  data_viewRFV;
  data_viewOriginalCheck;
  data_payeeCountry;
  data_debitCreditFlag;
  data_accounNum;
  data_accountAmount: any;
  data_policy;
  data_ssnOrSuspence;
  data_alpha;
  data_accDetailsList;
  viewServiceReqForm: FormGroup;
  transactions: any;
  constructor(
    private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private logger: NGXLogger,
    private homeService: HomeService,
    private companyNamesService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService,
    private replaceOrVoidChecksService: ReplaceOrVoidChecksService,
    private dialogueService: DialogueService
  ) { }

  ngOnInit() {
    this.interAddress4 = false;
    this.enableAccountDtl = false;
    this.displayBankNames();
    this.getTransactionsInfo();
    this.errorMsg = '';
    this.buildForm();
    this.viewServiceReqForm.disable();
  }

  /**
   * Building a form with params
   */
  buildForm(): void {
    this.viewServiceReqForm = this.fb.group({
      transactionType: this.data_transactionType || '-- NA --',
      companyName: this.data_companyName || '-- NA --',
      bankName: this.data_bankName || '-- NA --',
      viewCheckNumber: this.data_viewCheckNumber || '-- NA --',
      viewDateOfTrans: this.data_viewDateOfTrans || '-- NA --',
      viewPayee: this.data_viewPayee || '-- NA --',
      viewAdd1: this.data_viewAdd1 || '-- NA --',
      viewAdd2: this.data_viewAdd2 || '-- NA --',
      viewAdd3: this.data_viewAdd3 || '-- NA --',
      viewAdd4: this.data_viewAdd4 || '-- NA --',
      viewCity: this.data_viewCity || '-- NA --',
      ViewState: this.data_ViewState || '-- NA --',
      viewZipCode: this.data_viewZipCode || '-- NA --',
      viewSourceCode: this.data_viewSourceCode || '-- NA --',
      mailCode: this.data_mailCode || '-- NA --',
      viewAmount: this.data_viewAmount || '-- NA --',
      viewDesc: this.data_viewDesc || '-- NA --',
      viewRFV: this.data_viewRFV || '-- NA --',
      viewOriginalCheck: this.data_viewOriginalCheck || '-- NA --',
      viewCountry: this.data_payeeCountry || '-- NA --',
      viewAccountingArray: this.fb.array([
        this.getAccountDetails(),
      ])
    });
  }

  getAccountDetails() {
    return this.fb.group({
      accountNumber: this.data_accounNum || '--NA--',
      accountAmount: this.data_accountAmount || '--NA--',
      policyNumber: this.data_policy || '--NA--',
      suspenceNumber: this.data_ssnOrSuspence || '--NA--',
      alpha: this.data_alpha || '--NA--',
      debitCreditFlag: this.data_debitCreditFlag || '--NA--'
    });
  }

  /** below method will get the transaction by client Id */
  getTransactionsInfo() {
    this.activeRoute.queryParams.subscribe(params => { this.urlParam = params['clientId']; });
    this.sharedService.currentTransaction.subscribe(
      (response) => {
        this.transactions = response;       
        this.data_transactionType = this.transactions.transactionType;
        this.data_companyName = this.transactions.companyCode;
        this.data_bankName = this.data_companyName +',' + this.transactions.originalCheckBankAccount;
        this.data_viewCheckNumber = this.transactions.originalCheckNumber;
        this.data_viewDateOfTrans = this.transactions.checkDate;
        this.data_viewPayee = this.transactions.payeeName;
        this.data_viewAdd1 = this.transactions.payeeAddress1;
        this.data_viewAdd2 = this.transactions.payeeAddress2;
        this.data_viewAdd3 = this.transactions.payeeAddress3;
        this.data_viewAdd4 = this.transactions.payeeAddress4;
        this.data_viewDateOfTrans = this.transactions.checkDate;
        this.data_viewCity = this.transactions.payeeCity;
        this.data_ViewState = this.transactions.payeeState;
        if(this.transactions.payeeZipPlusFour){
          this.data_viewZipCode = this.transactions.payeeZip +'-'+this.transactions.payeeZipPlusFour;  
        }else{
          this.data_viewZipCode = this.transactions.payeeZip ;
        }       
        this.data_viewSourceCode = this.transactions.sourceCode;
        this.data_mailCode = this.transactions.mailCode;
        this.data_viewAmount = this.transactions.checkAmount;
        this.data_viewDesc = this.transactions.description;
        this.data_viewRFV = this.transactions.reason;
        this.data_viewOriginalCheck = this.transactions.originalCheckFlag;
        this.data_payeeCountry = this.transactions.payeeCountry;
        this.showAdd4ForInternational(this.data_payeeCountry);
        if (this.transactions.transactionType == 'Replacement') {
          this.enableReasonLabel = true;
          this.enableReasonVLabel = false;
          this.enableMailCode = true;
        } else if (this.transactions.transactionType == 'Void') {
          this.enableReasonLabel = false;
          this.enableReasonVLabel = true;
          this.enableMailCode = false;
        }
        this.replaceCheckAccountDtls(this.transactions.accountingDetails);
      }
    );
  }

  /**Below method is to display accountdetails for Replace checks */
  replaceCheckAccountDtls(accountDetails){
    this.data_accDetailsList = accountDetails;
    if (this.data_accDetailsList != null && this.data_accDetailsList.length > 0) {
      this.enableAccountDtl = true;
      for (const i of this.data_accDetailsList) {
        this.data_accounNum = i.accountNumber;
        this.data_accountAmount = i.amount;
        this.data_alpha = i.alpha;
        this.data_policy = i.policyNumber;
        this.data_ssnOrSuspence = i.suspenseNumber;
        this.data_debitCreditFlag = i.debitCreditFlag;       
      }  
    } else {
      this.enableAccountDtl = false;
    }
  }

  //Edit method to navigate to respective pages
  onSubmit(form: FormGroup) {
    this.activeRoute.queryParams.subscribe((params: Params) => {
      const id = params['clientId'];
      let navigationExtras: NavigationExtras = { queryParams: { 'clientId': id } };
      let recordType = this.viewServiceReqForm.get('transactionType');
      if (recordType.value === 'Replacement') {
        this.logger.info('<<=ViewReplaceOrVoidChecksComponent=>>EDIT REPLACE=> ', recordType.value);
        this.sharedService.setTransactionsToDisplay(this.transactions);
        this.router.navigate(['home/verifyCheckInfo', 'check'], { queryParams: { type: 'Replace', 'clientId': id } });
      } else if (recordType.value === 'Void') {
        this.logger.info('<<== ViewReplaceOrVoidChecksComponent==>>Edit VOID=>', recordType.value);
        this.sharedService.setTransactionsToDisplay(this.transactions);
        this.router.navigate(['home/verifyVoidCheckInfo', 'check'], { queryParams: { type: 'Void', 'clientId': id } });
      } else if (recordType.value === 'Disbursement') {
        this.logger.info('<<= ViewReplaceOrVoidChecksComponent==>>EDIT Disbursement=>', recordType.value);
        this.router.navigate(['home/requestDisbursement', 'path'], navigationExtras);
      }
    });

  }

  /** below method print the data */
  onPrint() {
    window.print();
  }

  /** below method will navigate to home page */
  onClickHome() {
    this.router.navigate(['home/yourTransactions']);
  }

  /** by calling this method we can delete the existing record */
  delete() {
    let navigationExtras: NavigationExtras = { queryParams: { 'clientId': this.urlParam } };
    const componentRef = this.dialogueService.openNewDialog(ConfirmDialogueComponent);
    if (componentRef && componentRef.instance) {
      componentRef.instance.setDetails({
        title: 'Confirm',
        message: 'Are you sure you want to delete this record (OK=yes Cancel=no)'
      });
      componentRef.instance.onClose.subscribe(
        isConfirmed => {
          this.dialogueService.closeComponent(componentRef);
          if (isConfirmed) {
            this.replaceOrVoidChecksService.deleteChecks(this.urlParam).subscribe(
              data => {
                this.logger.info('Deleted data', data);
                this.router.navigate(['home/yourTransactions']);
              },
              (error: any) => {
                this.logger.error('Error Occurred while deleting checks' + error.errorStat);
                if (error.errorStat == '404') {
                  this.errorMsg = '404 not found';
                } else if (error.errorStat == '403') {
                  this.errorMsg = 'You’re not authorized to perform that action.';
                } else if (error.errorStat == '500') {
                  this.errorMsg = 'Your request cannot be completed at this time.  Please try again';
                } else if (error.errorStat == '401') {
                  window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
                }
                else {
                  this.errorMsg = 'An error occurred while processing this request.';
                }
              });
          } else {
            this.router.navigate(['home/viewChecks'], navigationExtras);
          }
        });
    }
  }

    /** display the values in company names dropdown **/
    displayBankNames() {
      this.companyNamesService.getBanksNames().subscribe((res) => {
        this.bankNames = res;
      }, (error: any) => {
        this.logger.error('Error Occurred while loading bank names table');
      });
    }

    showAdd4ForInternational(payeeCountry){
      if(payeeCountry && payeeCountry.trim()){
        this.interAddress4 = true;
      }else{
        this.interAddress4 = false;
      }
    }
  
}
