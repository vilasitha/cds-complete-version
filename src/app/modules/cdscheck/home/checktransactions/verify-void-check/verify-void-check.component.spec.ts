import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyVoidCheckComponent } from './verify-void-check.component';

describe('VerifyVoidCheckComponent', () => {
  let component: VerifyVoidCheckComponent;
  let fixture: ComponentFixture<VerifyVoidCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyVoidCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyVoidCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
