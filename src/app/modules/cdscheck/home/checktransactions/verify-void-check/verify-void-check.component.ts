import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { States } from '../../shared/models/states';
import { ReasonCodes } from '../../shared/models/reasons';
import { MailCodes } from '../../shared/models/mailcodes';
import { AltVoidAccounts } from '../../shared/models/voidAccounts';
import { Companies } from '../../shared/models/companies';
import { RequestChecksModel } from '../replace-or-void-checks/models/checks-request-backend';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { SharedService } from '../../shared/services/shared.service';
import { NGXLogger } from 'ngx-logger';
import { ReplaceOrVoidChecksService } from '../replace-or-void-checks/replace-or-void-checks.service';
import { CommonRestService } from '../services/common-rest-service.service';
import { Observable } from 'rxjs/Observable';
import { ConfirmationService } from '../confirmchecks/confirmation.service';

@Component({
  selector: 'app-verify-void-check',
  templateUrl: './verify-void-check.component.html',
  styleUrls: ['./verify-void-check.component.css']
})
export class VerifyVoidCheckComponent implements OnInit {
  verifyVoidCheckForm: FormGroup;
  payeeState: States[] = [];
  reasonCodes: ReasonCodes[] = [];
  mailCodes: MailCodes[] = [];
  voidTo: AltVoidAccounts[] = [];
  companyCode: Companies[];
  editPayeeInfo: RequestChecksModel[] = [];
  editTransactions: any;
  payeeResponse: any;
  verifyCheckPath: string;
  checkType: string;
  editClientId: string;
  enabledOtherReason: boolean = false;
  enabledVoidToSection: boolean = false;
  errorMsgStatusFail = '';
  bank_AccountNumber;
  edit_BankRoutingNum
  edit_zipCode5: string;
  edit_zipCode4: string;
  edit_zipCode;
  constructor(private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private dropdownService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService,
    private logger: NGXLogger,
    private confirmationService: ConfirmationService,
    private replaceOrVoidChecksService: ReplaceOrVoidChecksService,
    private commonRestService: CommonRestService) { }

  ngOnInit() {
    this.activeRoute.paramMap.subscribe(params => {
      this.verifyCheckPath = params.get('check');
    });
    this.activeRoute.queryParams.subscribe((params: Params) => {
      this.checkType = params['type'];
    });
    this.activeRoute.queryParams.subscribe(params => { this.editClientId = params['clientId']; });
    this.errorMsgStatusFail = '';
    this.displayReasonCodesDD();
    this.displayVoidToDD();
    this.displayStatesDropdown();
    if (this.editClientId) {
      this.editVerifyCheckInfo();
      this.buildVerifyVoidCheckInfoForm(this.verifyCheckPath);
    } else {
      this.setPayee();
      this.buildVerifyVoidCheckInfoForm(this.verifyCheckPath);
    }

  }

  buildVerifyVoidCheckInfoForm(check: string): void {
    this.verifyVoidCheckForm = this.fb.group({
      payeeName: [this.payeeResponse.payeeName || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeName],
      amount: [this.payeeResponse.checkAmount || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.checkAmount],
      verifyDate: [this.payeeResponse.checkDate || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.checkDate],
      companyCode: [this.payeeResponse.companyCode || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.companyCode],
      description: [this.payeeResponse.description || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.descriptionCode],
      reason: [this.payeeResponse.reason || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason, [this.validationService.validateReasonCode]],
      originalCheck: [this.payeeResponse.originalCheckFlag || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckFlag, [this.validationService.validateOriginalCheck]],
      otherReason: [this.payeeResponse.reason || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason],
      payeeAddress1: [this.payeeResponse.payeeAddress1 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress1],
      payeeAddress2: [this.payeeResponse.payeeAddress2 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress2],
      payeeAddress3: [this.payeeResponse.payeeAddress3 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress3],  
      payeeCity: [this.payeeResponse.payeeCity || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeCity],
      payeeState: [this.payeeResponse.payeeState || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeState],
      payeeZip: [this.edit_zipCode5 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeZip],
      payeeZipPlusFour: [this.edit_zipCode4 || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeZipPlusFour],
      voidTo: [this.payeeResponse.voidTo || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.voidTo],
      originalCheckBankAccount: [this.bank_AccountNumber || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankAccount],
      originalCheckBankRouting: [this.edit_BankRoutingNum || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankRouting]

    });
  }

  clickPrevious() {
      this.getPayeeInfoOnPrevious();
     if (this.checkType === 'Void') {
      this.router.navigate(['home/voidChecks', 'clientId'], { queryParams: { type: 'Void', edit: 'vc', clientId: this.editClientId } });
    }
  }

  // Clicking cancel button will navigate to your transactions page
  navigateToHome() {
    this.replaceOrVoidChecksService.reset();
    this.router.navigate(['home/yourTransactions']);
  }
  onSubmit(form: FormGroup): void {
    if (form.valid) {
      this.setVerifyCheckInfo();
      if (this.editClientId != null) {
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.transactionType = 'Void';
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.clientId = this.editClientId;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.status = 'Complete';
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.sourceCode = this.payeeResponse.sourceCode;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankAccount = this.bank_AccountNumber;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankRouting = this.edit_BankRoutingNum;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckNumber = this.payeeResponse.originalCheckNumber;
        this.setLocalStorage('localStoragePhoneNumber', this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.phoneNumber);
        this.replaceOrVoidChecksService.updateVoidChecks(this.editClientId, this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel).
          subscribe(
          (response) => {
            this.logger.info('Update Void checks data', response);
            this.replaceOrVoidChecksService.reset();
            this.router.navigate(['home/confirmation'], { queryParams: { type: 'Void' } });

          }, (error: any) => {
            this.logger.error('Error Occurred while updating void checks', error.errorStat,
              'Error Message', error.errorMessage, 'Error Statu stext', error.statusText);
            if (error.errorStat == '404') {
              this.errorMsgStatusFail = '404 not found';
            } else if (error.errorStat == '403') {
              this.errorMsgStatusFail = 'You’re not authorized to perform that action.';
            } else if (error.errorStat == '500') {
              this.errorMsgStatusFail = 'Your request cannot be completed at this time.  Please try again';
            } else if (error.errorStat == '400') {
              this.errorMsgStatusFail = error.errorText;
            }
            else if (error.errorStat == '401') {
              window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
            }
            else {
              this.errorMsgStatusFail = 'An error occurred while processing this request.';
            }
          }
          );
      } else {
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.transactionType = 'Void';
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.status = 'Complete';
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankAccount = this.bank_AccountNumber;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckBankRouting = this.edit_BankRoutingNum;
        this.setLocalStorage('localStoragePhoneNumber', this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.phoneNumber);
        this.replaceOrVoidChecksService.saveVoidChecks(this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel).
          subscribe(
          (response) => {
            this.logger.info('Save Void checks data', response);
            this.replaceOrVoidChecksService.reset();
            this.router.navigate(['home/confirmation'], { queryParams: { type: 'Void' } });
          }, (error: any) => {
            this.logger.error('Error Occurred while saving void checks', error.errorStat,
              'Error Message', error.errorMessage + 'Error Statu stext', error.statusText);
            if (error.errorStat == '404') {
              this.errorMsgStatusFail = '404 not found';
            } else if (error.errorStat == '403') {
              this.errorMsgStatusFail = 'You’re not authorized to perform that action.';
            } else if (error.errorStat == '500') {
              this.errorMsgStatusFail = 'Your request cannot be completed at this time.  Please try again';
            } else if (error.errorStat == '400') {
              this.errorMsgStatusFail = error.errorText;
            }
            else if (error.errorStat == '401') {
              window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
            }
            else {
              this.errorMsgStatusFail = 'An error occurred while processing this request.';
            }
          }
          );
      }

    } else {
      this.validationService.validateAllFormFields(form);
    }
  }

  /** below method will set the payee address information to model class from UI */
  setVerifyCheckInfo() {
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeName = this.verifyVoidCheckForm.get('payeeName').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress1 = this.verifyVoidCheckForm.get('payeeAddress1').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress2 = this.verifyVoidCheckForm.get('payeeAddress2').value;
    if (this.verifyVoidCheckForm.get('payeeAddress3')) {
      this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress3 = this.verifyVoidCheckForm.get('payeeAddress3').value;
    } if (this.verifyVoidCheckForm.get('payeeAddress4')) {
      this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeAddress4 = this.verifyVoidCheckForm.get('payeeAddress4').value;
    }
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeCity = this.verifyVoidCheckForm.get('payeeCity').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeState = this.verifyVoidCheckForm.get('payeeState').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeZip = this.verifyVoidCheckForm.get('payeeZip').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.payeeZipPlusFour = this.verifyVoidCheckForm.get('payeeZipPlusFour').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.checkAmount = this.verifyVoidCheckForm.get('amount').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.checkDate = this.verifyVoidCheckForm.get('verifyDate').value;
    if (this.verifyVoidCheckForm.get('companyCode').value) {
      this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.companyCode = this.verifyVoidCheckForm.get('companyCode').value;
    }
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.descriptionCode = this.verifyVoidCheckForm.get('description').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.voidTo = this.verifyVoidCheckForm.get('voidTo').value;
    if (this.verifyVoidCheckForm.get('reason').value) {
      this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason = this.verifyVoidCheckForm.get('reason').value;
    } else {
      if (this.verifyVoidCheckForm.get('otherReason').value) {
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason = this.verifyVoidCheckForm.get('otherReason').value;
      }
    }
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckFlag = this.verifyVoidCheckForm.get('originalCheck').value;
    if (this.verifyVoidCheckForm.get('originalCheck').value === 'Y') {
      this.confirmationService.setVoidChecktMsg(true);
      this.confirmationService.setReplaceCheckMsg(false);
      this.confirmationService.setDisbursementMsg(false);
      this.confirmationService.setInterfaceReport(true);
    } else if (this.verifyVoidCheckForm.get('originalCheck').value === 'N') {
      this.confirmationService.setVoidChecktMsg(true);
      this.confirmationService.setReplaceCheckMsg(false);
      this.confirmationService.setDisbursementMsg(false);
      this.confirmationService.setInterfaceReport(false);
    }
  }

  /** below method will call while editing existing void check */
  editVerifyCheckInfo() {
    this.sharedService.currentTransaction.subscribe(
      (response) => {
        this.payeeResponse = response;
        if(this.payeeResponse.originalCheckBankAccount){
          this.bank_AccountNumber = this.payeeResponse.originalCheckBankAccount.trim();
        } 
        this.edit_zipCode4 = this.payeeResponse.payeeZipPlusFour;
        this.edit_zipCode5 = this.payeeResponse.payeeZip;
        let zipCode: string
        if (this.edit_zipCode4) {
          zipCode = this.payeeResponse.payeeZip + '-' + this.payeeResponse.payeeZipPlusFour.trim();
          this.edit_zipCode = zipCode.split('-');
          this.edit_zipCode5 = this.edit_zipCode[0];
          this.edit_zipCode4 = this.edit_zipCode[1]
        } else {
          zipCode = this.payeeResponse.payeeZip;
          this.edit_zipCode5 = zipCode;
        }

      }, (error: any) => {
        this.logger.error('Error Occurred while editong verify void check info', error);
        Observable.throw(error);
      }
    );
  }

  /** below method will set the payee address by calling company and check number service */
  setPayee() {
    this.sharedService.currentPayeeInfo.subscribe(
      (data) => {
        this.payeeResponse = data;
        const zipCode = this.payeeResponse.payeeZip;
        if(this.payeeResponse.bankAccountNumber){
          this.bank_AccountNumber = this.payeeResponse.bankAccountNumber.trim();
        }     
        if (zipCode) {
          this.edit_zipCode = zipCode.split('-');
          this.edit_zipCode5 = this.edit_zipCode[0].trim();
          this.edit_zipCode4 = this.edit_zipCode[1];
        }

      }, (error: any) => {
        this.logger.error('Error Occurred while setting payee info', error);
        Observable.throw(error);
      }
    );
  }
  /**
 * On Change of dropdown value enable Other field.
 */
  onChangeOfReasons() {
    const control = this.verifyVoidCheckForm.get('reason');
    if (control.value === 'Other') {
      this.enabledOtherReason = true;
    } else {
      this.enabledOtherReason = false;
    }
  }
  /**
   * Onchange of void to dorpdown enable ssn, date, state.
   */
  onChangeVoidTo() {
    const control = this.verifyVoidCheckForm.get('voidTo');
    this.logger.info('<<=VerifyCheckInfoComponent=>> in onChangeVoidTo', control.value);
    if (control.value !== 'Original Account') {
      this.enabledVoidToSection = true;
    } else {
      this.enabledVoidToSection = false;
    }
  }

  getPayeeInfoOnPrevious(){
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.reason = this.verifyVoidCheckForm.get('reason').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckFlag = this.verifyVoidCheckForm.get('originalCheck').value;
  }
  
  /** display the values in reason to void check dropdown **/
  displayReasonCodesDD(): any {
    this.dropdownService.getReasonsDropdown().subscribe((res) => {
      this.reasonCodes = res;
    },
      (error: any) => {
        this.logger.info('Error Occurred while calling reasoncodes dropdown(VerifyVoidCheckComponent)', error);
      });
  }
  /** display the values in Void To dropdown **/
  displayVoidToDD(): any {
    this.dropdownService.getVoidToDropdown().subscribe((res) => {
      this.voidTo = res;
    },
      (error: any) => {
        this.logger.info('Error Occurred while calling VoidTo dropdown(VerifyVoidCheckComponent)', error);
      });
  }
  /** display the states dropdown **/
  displayStatesDropdown(): any {
    this.dropdownService.getStatesDropdown().subscribe((res) => {
      this.payeeState = res;
    },
      (error: any) => {
        this.logger.error('Error Occurred while calling states dropdown (VerifyvoidCheckComponent)', error);
      }
    );
  }

  setLocalStorage(key, value) {
    localStorage.setItem(key, value);
  }

}
