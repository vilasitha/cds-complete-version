import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { RequestDisbursementModel, ContainerDisbursementDataModel } from '../disbursement/models/reqDisbursement';
import { States } from '../../shared/models/states';
import { MailCodes } from '../../shared/models/mailcodes';
import { Companies } from '../../shared/models/companies';
import { SharedService } from '../../shared/services/shared.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';
import { DisbursementService } from './disbursement.service';
import { NGXLogger } from 'ngx-logger';
import { element } from 'protractor';
import { CommonRestService } from '../services/common-rest-service.service';
import { ReplaceOrVoidChecksService } from '../replace-or-void-checks/replace-or-void-checks.service';
import { DisbursementReqModel } from './models/disbursement-backend';
import { DistributionCodes } from '../../shared/models/distributionCodes';
import { PayeeTableService } from '../../maintenance/payee-table/services/payee-table.service';

@Component({
  selector: 'app-disbursement',
  templateUrl: './disbursement.component.html',
  styleUrls: ['./disbursement.component.css'],
  providers: [NGXLogger]
})
export class DisbursementComponent implements OnInit {

  disbursementForm: FormGroup;
  companies: Companies[] = [];
  mailCodes: MailCodes[] = [];
  states: States[] = [];
  distributionCodes: DistributionCodes[] = [];
  accountTypes: any;

  editCurrenTransaction: any;
  requestModel: DisbursementReqModel[] = [];
  /**Flags to render the fields based on business fucntionality */
  enablePayeeDetails: boolean = false;
  enablePayeeCdYesOrNo: boolean = false;
  enablePayeeCode: boolean = true;
  enableMailCode: boolean = true;
  requiredTaxId: boolean = false;
  enableDistribCode: boolean = false;
  routePath: string;
  routeClientId: string;
  errorMsgDescCode = '';
  errorCompanyNAcc = '';
  statusFailMsg = '';
  errorRoutingNumber = '';
  errMsgCompanySource = '';
  // payeeResponse;
  data_editCompanyName;
  data_editSourceCode;
  data_editPhoneNumber;
  data_editEFT;
  data_editDescription;
  data_editState;
  data_editMailCode;
  data_editTaxId;
  data_editPayeeCode;
  data_editPayeeCodeFlag;
  data_editRoutingNumber;
  data_editAccountNumber;
  data_editAccountType;
  data_distributionCode;
  responseStatus: any;

  constructor(
    private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private commonDropdownService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService,
    private disbursementService: DisbursementService,
    private logger: NGXLogger,
    private commonRestService: CommonRestService,
    private replaceOrVoidChecksService: ReplaceOrVoidChecksService,
    private payeeMaintService: PayeeTableService
  ) { }

  ngOnInit() {
    if (this.router.url === '/home/requestDisbursement/payee') {
      if (this.disbursementService || this.disbursementService.reqtDisbursementDataModel) {
        this.disbursementService.reqtDisbursementDataModel.reset();
      }
    }
    if (this.replaceOrVoidChecksService || this.replaceOrVoidChecksService.containerChecksModel) {
      this.replaceOrVoidChecksService.containerChecksModel.reset();
    }
    this.payeeMaintService.reset();
    this.errorMsgDescCode = '';
    this.errorCompanyNAcc = '';
    this.statusFailMsg = '';
    this.errorRoutingNumber = '';
    this.errMsgCompanySource = '';
    this.routePath = this.activeRoute.snapshot.params['path'];
    this.activeRoute.queryParams.subscribe(params => { this.routeClientId = params['clientId']; });
    this.displayCompanyNames();
    this.displayMailCodesDropdown();
    this.displayDistributionCodes();
    this.getStates();
    this.enablePayeeDetails;
    this.enablePayeeCdYesOrNo;
    this.enablePayeeCode;
    if (this.routeClientId) {
      if (this.router.url === '/home/requestDisbursement/payee?clientId=' + this.routeClientId) {
        this.editOrUpdateReqDisbursement();
      }
    }
    this.buildForm(this.routePath);
    if (this.router.url == '/home/requestDisbursement/payee' || this.router.url === '/home/requestDisbursement/payee?clientId=' + this.routeClientId) {
      this.getLocalStorage('localStoragePhoneNumber');
    }
    this.validateOnChange(null);
    this.validateOnVendorCode(null);
    this.onChangeOfSourceCode(null);
  }

  buildForm(path: string) {
    this.disbursementForm = this.fb.group({
      companyCode: [this.data_editCompanyName || this.disbursementService.reqtDisbursementDataModel.disbursementModel.companyCode, [this.validationService.validateCompanyName]],
      states: [this.data_editState || this.disbursementService.reqtDisbursementDataModel.disbursementModel.stateCode,
      [this.validationService.validateStateNames]],
      sourceCode: [this.data_editSourceCode || this.disbursementService.reqtDisbursementDataModel.disbursementModel.sourceCode,
      this.validationService.validateSourceCode],
      rbPhnNum: [this.data_editPhoneNumber || this.disbursementService.reqtDisbursementDataModel.disbursementModel.phoneNumber],
      checkOrEftRadioFlag: [this.data_editEFT || this.disbursementService.reqtDisbursementDataModel.disbursementModel.paymentType],
      payeeCodeFlag: [this.data_editPayeeCodeFlag || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCodeFlag,
      [this.validationService.validatePayeeYesOrNo]],
      payeeCode: [this.data_editPayeeCode || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCode],
      descriptionCode: [this.data_editDescription || this.disbursementService.reqtDisbursementDataModel.disbursementModel.descriptionCode],
      mailCodes: [this.data_editMailCode || this.disbursementService.reqtDisbursementDataModel.disbursementModel.mailCode, [this.validationService.validateMailCodes]],
      taxId: [this.data_editTaxId || this.disbursementService.reqtDisbursementDataModel.disbursementModel.taxId],
      routingNumber: [this.data_editRoutingNumber || this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankRouting,
      [this.validationService.validateRoutingNumber]],
      accountNumber: [this.data_editAccountNumber || this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankAccount,
      [this.validationService.validateAccountNumber]],
      accountType: [this.data_editAccountType || this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountType,
      [this.validationService.validateAccountType]],
      distributionCode: [this.data_distributionCode || this.disbursementService.reqtDisbursementDataModel.disbursementModel.distributionCode, [this.validationService.validateDistribCode]]
    });
  }

  /**Onsubmit of request disbursement */
  onSubmit(form: FormGroup): void {
    this.validationService.validateDisbursementAllFormFields(form);
    this.validationService.validateTaxId(form);
    this.validationService.validRoutingNumber(form);
    if (form.valid) {
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.transactionType = 'Disbursement';
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.companyCode = this.disbursementForm.get('companyCode').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.sourceCode = this.disbursementForm.get('sourceCode').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.phoneNumber = this.disbursementForm.get('rbPhnNum').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.mailCode = this.disbursementForm.get('mailCodes').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.stateCode = this.disbursementForm.get('states').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankAccount = this.disbursementForm.get('accountNumber').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankRouting = this.disbursementForm.get('routingNumber').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.paymentType = this.disbursementForm.get('checkOrEftRadioFlag').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.taxId = this.disbursementForm.get('taxId').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.descriptionCode = this.disbursementForm.get('descriptionCode').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCode = this.disbursementForm.get('payeeCode').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.distributionCode = this.disbursementForm.get('distributionCode').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountType = this.disbursementForm.get('accountType').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCodeFlag = this.disbursementForm.get('payeeCodeFlag').value;
      const sourceCodeVal: string = this.disbursementForm.get('sourceCode').value;
      const descCodeVal: string = this.disbursementForm.get('descriptionCode').value;
      const payeeCodeVal: string = this.disbursementForm.get('payeeCode').value;
      const companyCodeVal: string = this.disbursementForm.get('companyCode').value;
      this.sharedService.setEftFlag(this.disbursementForm.get('checkOrEftRadioFlag').value);
      this.setPayeeAddOfPayeeCode(payeeCodeVal);
      if (companyCodeVal && sourceCodeVal) {
        this.commonRestService.validateCompanyAndSouceCode(companyCodeVal, sourceCodeVal).subscribe(
          (response: any) => {
            this.sharedService.setCheckType(this.disbursementForm.get('checkOrEftRadioFlag').value);
            this.sharedService.setPayeeCodeYOrNFlag(this.disbursementForm.get('payeeCodeFlag').value);
            if (payeeCodeVal && descCodeVal) {
              this.errorMsgDescCode = '';
              this.errorCompanyNAcc = '';
              this.errMsgCompanySource = '';
              this.payeeCodeAndDesCodeValidation();
            } else {
              this.errMsgCompanySource = '';

              if (companyCodeVal && sourceCodeVal && payeeCodeVal) {
                this.errorMsgDescCode = '';
                this.getPayeeInformation();
              }
              else {
                this.errorCompanyNAcc = '';
                if (descCodeVal) {
                  this.desCodeServerValidation();
                }
                else {
                  this.router.navigate(['home/payeeAddress', 'payeeAddress'], { queryParams: { 'clientId': this.routeClientId } });
                }
              }
            }
          }, (err: any) => {
            if (err.errorStat == '404') {
              this.errMsgCompanySource = 'Company Code and Source Code combination is invalid';
            }
          }
        );

      }

      if (descCodeVal == null || descCodeVal == '') {
        this.sharedService.setDescriptionCOde(null);
      }
    }
    else {
      this.validationService.validateAllFormFields(form);
    }
  } // endmailCode

  /** below method will call onclick of cancel */
  onCancel() {
    this.disbursementService.reset();
    this.router.navigate(['home/yourTransactions']);
  }

  /** below method will call onclick of edit Or update request disbursement */
  editOrUpdateReqDisbursement() {
    this.activeRoute.queryParams.subscribe(params => { this.routeClientId = params['clientId']; });
    this.sharedService.currentTransaction.subscribe(
      (result) => {
        this.editCurrenTransaction = result;
        this.data_editCompanyName = this.editCurrenTransaction.companyCode;
        this.data_editSourceCode = this.editCurrenTransaction.sourceCode;
        this.data_editPhoneNumber = this.editCurrenTransaction.phoneNumber;
        this.data_editEFT = this.editCurrenTransaction.paymentType;
        this.data_editDescription = this.editCurrenTransaction.description;
        this.data_editPayeeCodeFlag = this.editCurrenTransaction.originalCheckFlag;
        this.data_editState = this.editCurrenTransaction.stateCode;
        this.data_editMailCode = this.editCurrenTransaction.mailCode;
        this.data_editTaxId = this.editCurrenTransaction.taxId;
        this.data_editPayeeCode = this.editCurrenTransaction.payeeCode;
        this.data_editAccountNumber = this.editCurrenTransaction.bankAccount;
        this.data_editAccountType = this.editCurrenTransaction.accountType;
        this.data_editRoutingNumber = this.editCurrenTransaction.bankRouting;
        this.data_distributionCode = this.editCurrenTransaction.distributionCode;
        if (this.data_editPayeeCode) {
          this.data_editPayeeCodeFlag = 'Y';
        } else {
          this.data_editPayeeCodeFlag = 'N';
        }

      }, (error: any) => { this.logger.info('Error Occurrred while editing disbursment', error); });
  }

  /** Description code validation method */
  desCodeServerValidation() {
    this.commonRestService.validateDescriptionCode(this.disbursementService.reqtDisbursementDataModel.disbursementModel.descriptionCode).subscribe(
      (resp) => {
        this.logger.info('Description code', resp);
        this.sharedService.setDescriptionCOde(resp.description);
        this.router.navigate(['home/payeeAddress', 'payeeAddress'], { queryParams: { 'clientId': this.routeClientId } });
      }, (err: any) => {
        if (err.errorStat == '404') {
          this.errorMsgDescCode = 'Invalid Description code';
        }
      });
  }

  /** Payee code validation method */
  getPayeeInformation() {
    if (this.disbursementService.reqtDisbursementDataModel.disbursementModel.companyCode != null
      && this.disbursementService.reqtDisbursementDataModel.disbursementModel.sourceCode != null
      && this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCode != null
    ) {
      this.commonRestService.getPayeeInfoForDisbursement(this.disbursementService.reqtDisbursementDataModel.disbursementModel.companyCode,
        this.disbursementService.reqtDisbursementDataModel.disbursementModel.sourceCode,
        this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCode).
        subscribe(
        (result) => {
          this.sharedService.setPayeeAddForDisbursement(result);
          this.router.navigate(['home/payeeAddress', 'payeeAddress'], { queryParams: { 'clientId': this.routeClientId } });
        }
        , (err: any) => {
          if (err.errorStat == '404') {
            this.errorCompanyNAcc = 'Invalid Company Code / Source Code / Payee Code combination';
          }
        });
    }
  }

  /** payee code and description code validation method */
  payeeCodeAndDesCodeValidation() {
    this.commonRestService.validateDescriptionCode(this.disbursementService.reqtDisbursementDataModel.disbursementModel.descriptionCode).subscribe(
      (resp) => {
        this.logger.info('Description code', resp);
        this.sharedService.setDescriptionCOde(resp.description);
        this.commonRestService.getPayeeInfoForDisbursement(this.disbursementService.reqtDisbursementDataModel.disbursementModel.companyCode,
          this.disbursementService.reqtDisbursementDataModel.disbursementModel.sourceCode,
          this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCode).
          subscribe(
          (result) => {
            this.sharedService.setPayeeAddForDisbursement(result);
            this.router.navigate(['home/payeeAddress', 'payeeAddress'], { queryParams: { 'clientId': this.routeClientId } });
          }, (err: any) => {
            if (err.errorStat == '404') {
              this.errorCompanyNAcc = 'Invalid Company Code / Source Code / Payee Code combination';
            }
          });
      }, (err: any) => {
        if (err.errorStat == '404') {
          this.errorMsgDescCode = 'Invalid Description code';
        }
      });
  }

  /** below method will call onclick of saveas draft */
  onSaveAsDraft(form: any) {
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.transactionType = 'Disbursement';
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.companyCode = this.disbursementForm.get('companyCode').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.sourceCode = this.disbursementForm.get('sourceCode').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.phoneNumber = this.disbursementForm.get('rbPhnNum').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.mailCode = this.disbursementForm.get('mailCodes').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.stateCode = this.disbursementForm.get('states').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankAccount = this.disbursementForm.get('accountNumber').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankRouting = this.disbursementForm.get('routingNumber').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.paymentType = this.disbursementForm.get('checkOrEftRadioFlag').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.taxId = this.disbursementForm.get('taxId').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.descriptionCode = this.disbursementForm.get('descriptionCode').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCode = this.disbursementForm.get('payeeCode').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.distributionCode = this.disbursementForm.get('distributionCode').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountType = this.disbursementForm.get('accountType').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCodeFlag = this.disbursementForm.get('payeeCodeFlag').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.status = 'Draft';
    if (this.routeClientId) {
      this.updateDraft(this.routeClientId, this.disbursementService.reqtDisbursementDataModel.disbursementModel);

    } else {
      this.onDraft(this.disbursementService.reqtDisbursementDataModel.disbursementModel);
    }
  }

  /** below method will call onclick of draft */
  onDraft(disbursementModel: RequestDisbursementModel): void {
    this.disbursementService.saveAsDraft(disbursementModel)
      .subscribe(
      (data) => {
        this.disbursementService.reset();
        this.router.navigate(['home/yourTransactions']);
      },
      (error: any) => {
        this.logger.error('Error Occurred while saving', error.errorStat);
        if (error.errorStat == '404') {
          this.statusFailMsg = '404 not found';
        } else if (error.errorStat == '403') {
          this.statusFailMsg = 'You’re not authorized to perform that action.';
        } else if (error.errorStat == '500') {
          this.statusFailMsg = 'Your request cannot be completed at this time.  Please try again';
        } else if (error.errorStat == '401') {
          window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
        } else {
          this.statusFailMsg = 'An error occurred while processing this request.';
        }
      });
  }

  /** below method will call onclick of edit Or update of existing draft */
  updateDraft(clientId: string, disbursementModel: RequestDisbursementModel): void {
    this.disbursementService.updateDisbursement(clientId, disbursementModel)
      .subscribe(
      (data) => {
        this.disbursementService.reset();
        this.router.navigate(['home/yourTransactions']);
      },
      (error: any) => {
        this.logger.error('Error Occurred while saving', error.errorStat);
        if (error.errorStat == '404') {
          this.statusFailMsg = '404 not found';
        } else if (error.errorStat == '403') {
          this.statusFailMsg = 'You’re not authorized to perform that action.';
        } else if (error.errorStat == '500') {
          this.statusFailMsg = 'Your request cannot be completed at this time.  Please try again';
        } else if (error.errorStat == '401') {
          window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
        } else {
          this.statusFailMsg = 'An error occurred while processing this request';
        }
      });
  }

  /** get the values in states dropdown **/
  getStates() {
    this.commonDropdownService.getStatesDropdown().subscribe((response) => {
      this.states = response;
    },
      (error: any) => {
        this.logger.error('Error in loading states dropdown', error);
      });
  }

  /** display the values in Mail code dropdown **/
  displayMailCodesDropdown() {
    this.commonDropdownService.getMailcodesDropdown().subscribe((response) => {
      this.mailCodes = response;
    },
      (error: any) => {
        this.logger.error('Error in loading MailCodes dropdown', error.errorStat);
      });
  }
  /** display the values in company names dropdown **/
  displayCompanyNames() {
    this.commonDropdownService.getCompanyNames().subscribe((res) => {
      this.companies = res;
    },
      (error: any) => {
        this.logger.error('Error Occurred while loading Company name dropdown', error.errorStat);
      });
  }

  displayDistributionCodes(): any {
    this.commonDropdownService.getDistributionCodes().subscribe(
      (response) => {
        this.distributionCodes = response;
      },
      (error: any) => {
        this.logger.error('Error in loading ditribution code dropdown', error.errorStat);
      });
  }

  /** render the fields based on money transfer type */
  validateOnChange(control: AbstractControl) {
    this.clearForm();
    let transferType = this.disbursementForm.get('checkOrEftRadioFlag');
    if (transferType.value === 'Check') {
      this.enablePayeeCode = true;
      this.enablePayeeCdYesOrNo = false;
      this.enablePayeeDetails = false;
      this.enableMailCode = true;
      this.disbursementForm.get('payeeCodeFlag').setValue(null);
      this.disbursementForm.get('accountNumber').setValue(null);
      this.disbursementForm.get('accountType').setValue(null);
      this.disbursementForm.get('routingNumber').setValue(null);
    } else if (transferType.value === 'EFT') {
      this.enablePayeeCode = false;
      this.enablePayeeCdYesOrNo = true;
      this.enablePayeeDetails = true;
      this.enableMailCode = false;
      this.reload();
    }
  }// end

  /**  render the fields based on vendor code  */
  validateOnVendorCode(control: AbstractControl) {
    const vendorCode = this.disbursementForm.get('payeeCodeFlag');
    if (vendorCode.value === 'Y') {
      this.enablePayeeCode = true;
      this.enablePayeeDetails = false;
      this.disbursementForm.get('accountNumber').setValue(null);
      this.disbursementForm.get('accountType').setValue(null);
      this.disbursementForm.get('routingNumber').setValue(null);
    } else if (vendorCode.value === 'N') {
      this.enablePayeeCode = false;
      this.enablePayeeDetails = true;
      this.disbursementForm.get('payeeCode').setValue(null);
    }
  } // end

  onChangeOfSourceCode(control: AbstractControl) {
    const sourceCode = this.disbursementForm.get('sourceCode');
    const payeeCode = this.disbursementForm.get('payeeCode').value;
    if (sourceCode.value === 'X01' || sourceCode.value === 'X0109' || sourceCode.value === 'X01I') {
      this.requiredTaxId = true;
      this.enableDistribCode = true;
    } else {
      this.requiredTaxId = false;
      this.enableDistribCode = false;
      this.disbursementForm.get('distributionCode').setValue(null);
    }
  }

  setPayeeAddOfPayeeCode(payeeCode: string) {
    if (payeeCode) {
      this.sharedService.setCheckFlag(this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCode);
    } else {
      this.sharedService.setCheckFlag(null);
    }
  }

  clearForm() {
    this.disbursementForm.markAsPristine();
    this.disbursementForm.markAsUntouched();
    this.disbursementForm.updateValueAndValidity();
  }

  getLocalStorage(key) {
    const item = localStorage.getItem(key);
    if (item && item != 'null' && item != 'undefined' && key == 'localStoragePhoneNumber') {
      this.logger.info('item', item, 'key', key);
      this.data_editPhoneNumber = item;
      this.disbursementForm.get('rbPhnNum').setValue(this.data_editPhoneNumber);
    } else {

      if (this.routeClientId && key == 'localStoragePhoneNumber' && (item === 'undefined' || item == "null")) {
        this.logger.info('Local storage value', item);
        this.disbursementForm.get('rbPhnNum').setValue(null);
      } else {
        if (key == 'localStoragePhoneNumber' && (item === 'undefined' || item == "null")) {
          this.logger.info('Local storage value', item);
          this.disbursementForm.get('rbPhnNum').setValue(null);
        }
      }
    }
  }

  reload() {
    this.errorMsgDescCode = '';
    this.errorCompanyNAcc = '';
    this.statusFailMsg = '';
    this.errorRoutingNumber = '';
    this.errMsgCompanySource = '';
  }

  getReqDisbursementDataModel(): ContainerDisbursementDataModel {
    return this.disbursementService.reqtDisbursementDataModel;
  }

  setReqDisbursementDataModel(reqtDisbursementDataModel: ContainerDisbursementDataModel) {
    this.disbursementService.reqtDisbursementDataModel = reqtDisbursementDataModel;
  }

}
