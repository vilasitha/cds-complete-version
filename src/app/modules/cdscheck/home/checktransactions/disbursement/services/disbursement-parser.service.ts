import { Injectable } from "@angular/core";
import { DisbursementReqModel } from '../models/disbursement-backend';
import { RequestDisbursementModel } from "../models/reqDisbursement";

@Injectable()
export class DisbursementParserService {

    private disbursementReqModel: DisbursementReqModel;

    constructor() { }

    convertUIModelToBackEndModel(request: RequestDisbursementModel): DisbursementReqModel {
        this.disbursementReqModel = new DisbursementReqModel();
        this.disbursementReqModel.companyCode = request.companyCode;
        this.disbursementReqModel.sourceCode = request.sourceCode;
        this.disbursementReqModel.phoneNumber = request.phoneNumber;
        this.disbursementReqModel.paymentType = request.paymentType;
        this.disbursementReqModel.distributionCode = request.distributionCode;
        this.disbursementReqModel.stateCode = request.stateCode;
        this.disbursementReqModel.mailCode = request.mailCode;
        this.disbursementReqModel.taxId = request.taxId;
        this.disbursementReqModel.payeeCode = request.payeeCode;
        this.disbursementReqModel.descriptionCode = request.descriptionCode;
        this.disbursementReqModel.payeeCodeFlag = request.payeeCodeFlag;
        this.disbursementReqModel.bankAccount = request.bankAccount;
        this.disbursementReqModel.bankRouting = request.bankRouting;
        this.disbursementReqModel.accountType = request.accountType;
        this.disbursementReqModel.payeeAddressIndicator = request.payeeAddressIndicator;
        this.disbursementReqModel.payeeName = request.payeeName;
        this.disbursementReqModel.payeeAddress1 = request.payeeAddress1;
        this.disbursementReqModel.payeeAddress2 = request.payeeAddress2;
        this.disbursementReqModel.payeeAddress3 = request.payeeAddress3;
        this.disbursementReqModel.payeeAddress4 = request.payeeAddress4;
        this.disbursementReqModel.payeeCity = request.payeeCity;
        this.disbursementReqModel.payeeZip = request.payeeZip;
        this.disbursementReqModel.payeeZipPlusFour = request.payeeZipPlusFour;
        this.disbursementReqModel.payeeState = request.payeeState;
        this.disbursementReqModel.description1 = request.description1;
        this.disbursementReqModel.description2 = request.description2;
        this.disbursementReqModel.payeeCountry = request.payeeCountry;
        if (request.accountingDetails)
            this.disbursementReqModel.accountingDetails =
                request.accountingDetails.filter(item => item.accountNumber);
        this.disbursementReqModel.transactionType = request.transactionType;
        this.disbursementReqModel.status = request.status;
        this.disbursementReqModel.checkDate = request.checkDate;
        this.disbursementReqModel.lastModified = request.lastModified;
        this.disbursementReqModel.paymentAdminSystem = request.paymentAdminSystem;
        this.disbursementReqModel.originalCheckNumber = request.originalCheckNumber;
        return this.disbursementReqModel;
    }
}

