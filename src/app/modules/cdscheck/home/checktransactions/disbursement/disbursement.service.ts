import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { RequestDisbursementModel, ContainerDisbursementDataModel } from './models/reqDisbursement';
import { Constants } from '../../shared/constants/constants';

import { HttpErrorHandler, HandleError } from '../error-messages/services/http-error-handler.service';

import { HttpHeaderResponse, HttpResponse } from '@angular/common/http';
import { DisbursementParserService } from './services/disbursement-parser.service';
import { DisbursementReqModel } from './models/disbursement-backend';
import { BehaviorSubject } from 'rxjs';
import { Response } from '@angular/http';


const httpHeaderOptions = {
    headers: new HttpHeaders({
        // 'Content-Type': 'application/json',
        // 'Accept': 'application/json',
        'Cache-Control': 'no-store',
        'X-Requested-With': 'test'
    })
};

@Injectable()
export class DisbursementService {

    reqtDisbursementDataModel: ContainerDisbursementDataModel = new ContainerDisbursementDataModel();

    reset() {
        this.reqtDisbursementDataModel = new ContainerDisbursementDataModel();
    }
    constructor(private httpClient: HttpClient,
        private disbursementParserService: DisbursementParserService) {
    }


    saveDisbursement(request: RequestDisbursementModel): Observable<any> {
        const disbursementReqModel: DisbursementReqModel =
            this.disbursementParserService.convertUIModelToBackEndModel(request);
        return this.httpClient.post(Constants.REST_REQ_API_URL, disbursementReqModel, httpHeaderOptions)
            .map((resp: Response) => {
            })
            .catch(this.handleError)
    }

    updateDisbursement(clientId: string,request: RequestDisbursementModel): Observable<any> {
        const url = `${Constants.REST_REQ_API_URL}/${clientId}`;
        const disbursementReqModel: DisbursementReqModel =
            this.disbursementParserService.convertUIModelToBackEndModel(request);
        return this.httpClient.put(url, disbursementReqModel, httpHeaderOptions)
            .map((resp: Response) => {                   
            })
            .catch(this.handleError)
    }

    saveAsDraft(request: RequestDisbursementModel): Observable<any> {
        const disbursementReqModel: DisbursementReqModel =
            this.disbursementParserService.convertUIModelToBackEndModel(request);
        return this.httpClient.post(Constants.REST_REQ_API_URL, disbursementReqModel, httpHeaderOptions
        )
            .map((resp: Response) => { })
            .catch(this.handleError)
    };

    deleteDisbursement(id: string): Observable<any> {
        const url = `${Constants.REST_REQ_API_URL}/${id}`;
        return this.httpClient.delete(url, httpHeaderOptions)
            .map((response: Response) => {
            })._catch(this.handleError);
    }

    private handleError(error: any) {
        let errorObj: any = {
            errorMessage: error.message,
            errorStat: error.status,
            // errorText: error.statusText
            errorField: error.error ? error.error[0].field : error.statusText,
            errorText: error.error ? error.error[0].code : error.statusText
        };
        return Observable.throw(errorObj);
    }
}