import { AccountingDetails } from './account-details';
import { States } from '../../../shared/models/states';
import { Companies } from '../../../shared/models/companies';
import { MailCodes } from '../../../shared/models/mailcodes';
import { ReasonCodes } from '../../../shared/models/reasons';
import { AltVoidAccounts } from '../../../shared/models/voidAccounts';
import { } from './accounting-data';
import { DistributionCodes } from '../../../shared/models/distributionCodes';
export class ContainerDisbursementDataModel {

    disbursementModel: RequestDisbursementModel;
    constructor() {
        this.disbursementModel = new RequestDisbursementModel();
    }
    reset() {
        this.disbursementModel = new RequestDisbursementModel();
    }
}

export class RequestDisbursementModel {
    company: Companies;
    companyCode: string = 'AUL';

    state: States;
    stateCode: string;

    mail: MailCodes;
    mailCode: string = 'MA';

    reasons: ReasonCodes;
    reason: string = '';

    distributionCodes: DistributionCodes;
    // distributionCode: string='';
    distributionCode: string;
    transactionType: string;
    sourceCode: string;
    phoneNumber: string;
    payeeCode: string;
    payeeName: string;
    payeeAddress1: string;
    payeeAddress2: string;
    payeeAddress3: string ;
    payeeAddress4: string ;
    payeeCity: string;
    payeeState: string;
    payeeZip: string;
    payeeZipPlusFour: string;
    payeeCountry: string;
    payeeAddressIndicator: string = 'U.S';
    taxId: string;
    bankRouting: string;
    bankAccount: string;
    paymentType: string = 'Check' ;
    descriptionCode: string;
    description1: string;
    description2: string;
    status: string;
    lastModified: string;
    originalCheckNumber: string;
    // originalCheckFlag: string;
    voidToDate: string;
    ssn: string;
    checkDate: string;
    alpha:  string;
    checkAmount: string;
    voidTo: AltVoidAccounts;
    voidAccount: string;  
    payeeCodeFlag: string;
    accountType: string;
    accountingDetails: Array<AccountingDetails> =[];
    accountingDetailsBack: Array<AccountingDetails> =[];    
    totalCheckAmount: string;
    paymentAdminSystem: string;
}

