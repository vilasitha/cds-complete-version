import { AccountingDetails } from './account-details';

export class DisbursementReqModel{
    
    companyCode: string;
    stateCode: string;
    mailCode: string;
    reason: string;
    phoneNumber: string;
    payeeState: string;
    payeeAddressIndicator: string;
    paymentType: string;
    accountingDetails: Array<AccountingDetails> =[];
    transactionType: string;
    sourceCode: string;
    bankAccount: string;
    bankRouting: string;
    taxId: string;
    descriptionCode: string;
    payeeCode: string;
    payeeCountry: string;
    payeeName: string;
    payeeAddress1: string;
    payeeAddress2: string;
    payeeAddress3: string;
    payeeAddress4: string;
    payeeCity: string;
    payeeZip: string;
    payeeZipPlusFour: string;
    description1: string;
    description2: string;
    status: string;
    distributionCode: string;
    accountType: string
    checkDate: string;
    lastModified: string;
    originalCheckNumber: string;
    payeeCodeFlag: string;
    paymentAdminSystem: string;
}