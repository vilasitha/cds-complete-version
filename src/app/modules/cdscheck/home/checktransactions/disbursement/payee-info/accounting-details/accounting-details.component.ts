import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountingDetails } from '../../models/account-details';
import { FormGroup, AbstractControl, FormControl } from '@angular/forms/src/model';
import { FormBuilder, FormArray } from '@angular/forms';
import { TransactionsResponse } from '../../../../your-transactions/models/all-trasactions';
import { ConfirmationService } from '../../../confirmchecks/confirmation.service';
import { DisbursementService } from '../../disbursement.service';
import { RequestDisbursementModel, ContainerDisbursementDataModel } from '../../models/reqDisbursement';
import { ValidationService } from '../../../services/validation.service';
import { NGXLogger } from 'ngx-logger';
import { HomeService } from '../../../../services/home.service';
import { SharedService } from '../../../../shared/services/shared.service';
import { element } from 'protractor';
import { Observable } from 'rxjs/Observable';
import { AccountDetails } from '../../../../your-transactions/models/accountDetailsModel';
import { CommonRestService } from '../../../services/common-rest-service.service';
import { SourceSytems } from '../../../../maintenance/source-code/models/sourceSystems';
import { CommondropdownService } from '../../../services/commondropdown.service';


@Component({
  selector: 'app-accounting-details',
  templateUrl: './accounting-details.component.html',
  styleUrls: ['./accounting-details.component.css'],
  providers: [NGXLogger]
})
export class AccountingDetailsComponent implements OnInit {
  position = 'above';
  routePath: string;
  routeClientId: string;
  accountDetailsForm: FormGroup;
  errorMsg = '';
  errorMsgAccntFailed = '';
  sumTotalAmount: any;
  data_editAccDetails: AccountDetails[] = [];
  paymentAdminSystem: SourceSytems[] = [];
  edit_paymentAdminSys;

  public accountsArray: Array<AccountingDetails> = [];
  yourTransactionsList: any;
  constructor(private router: Router,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
    private logger: NGXLogger,
    private homeService: HomeService,
    private disbursementService: DisbursementService,
    private confirmationService: ConfirmationService,
    private sharedService: SharedService,
    private validationService: ValidationService,
    private commonRestService: CommonRestService,
    private commondropdownService: CommondropdownService) { }

  ngOnInit() {
    this.routePath = this.activeRoute.snapshot.params['list'];
    this.activeRoute.queryParams.subscribe(params => { this.routeClientId = params['clientId']; });
    this.errorMsg = '';
    this.errorMsgAccntFailed = '';
    this.getPaymentAdminSystem();
    this.confirmationService.setDisbursementMsg(true);
    this.confirmationService.setInterfaceReport(false);
    this.confirmationService.setVoidChecktMsg(false);
    this.confirmationService.setReplaceCheckMsg(false);
    if (this.routeClientId) {
      this.buildForm(this.routePath);
      this.editOrUpdateActDetails();
    } else {
      this.buildForm(this.routePath);
    }

  }
  buildForm(list: string) {
    this.accountDetailsForm = this.fb.group({
      totalCheckAmount: [this.disbursementService.reqtDisbursementDataModel.disbursementModel.totalCheckAmount, [this.validationService.validateTotalCheckAmount]],
      paymentAdminSystem: [this.edit_paymentAdminSys ||this.disbursementService.reqtDisbursementDataModel.disbursementModel.paymentAdminSystem, [this.validationService.validatePaymentAdminSys]],
      accountsArray: this.fb.array([
        this.initAccountDetails('', '', '', '', '', 'D'),
      ])
    });
    for (let i = 0; i < 9; i++) {
      this.addFieldValue('', '', '', '', '', 'D');
    }
    if (this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountingDetailsBack
      && this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountingDetailsBack.length > 0)
      this.updateAccountDetailsBack();
  }

  updateAccountDetailsBack() {

    let arrayControl = this.accountDetailsForm.get('accountsArray') as FormArray;
    arrayControl.controls.splice(0, arrayControl.controls.length);
    for (let deatils of this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountingDetailsBack) {
      this.addFieldValue(deatils.accountNumber,
        deatils.amount,
        deatils.policyNumber, deatils.suspenseNumber, deatils.alpha, deatils.debitCreditFlag);
    }


  }
  initAccountDetails(accNum, amt, pol, sus, alpha, drFlag) {
    return this.fb.group({
      accountNumber: [accNum, this.validationService.fieldNumericValidation],
      amount: [amt,  this.validationService.fieldNumericValidation],
      policyNumber: [pol],
      suspenseNumber: [sus],
      alpha: [alpha],
      debitCreditFlag: [drFlag]
    }

    );
  }

  validateAccoutDetails(formGroup: FormGroup) {
    let accControler = formGroup.get('accountNumber');
    if (accControler.value) {
      let errMsg = '';
      this.commonRestService.companyAndAccountValidation(
        this.disbursementService.reqtDisbursementDataModel.disbursementModel.companyCode,
        accControler.value).subscribe(
        (data) => {
          this.logger.info('Account Number status:', data);
        }
        , (err: any) => {
          if (err.errorStat == '404') {
            errMsg = 'invalidAccountNumber';
          } else if (err.errorStat == '400') {
            errMsg = 'errorOccuredAccountNumber';
          } else {
            errMsg = 'errorOccuredAccountNumber';
          }
          accControler.markAsTouched({ onlySelf: true });
          accControler.setErrors({
            'invalidAccountNumber': errMsg == 'invalidAccountNumber' ? true : false,
            'errorOccuredAccountNumber': errMsg != 'invalidAccountNumber' ? true : false
          });

        });
    } else {
      accControler.setErrors(null);
    }
  }

  /** Below method will converts the amount to float and validates max amount*/
  convertDecimal(obj, obj1) {
    let tmp = this.getDecimalVal(obj.value);
    obj.value = tmp;
    obj1.value = tmp;
    if (obj.value < 0 || obj.value > 99999999.99) {
      window.alert('The Amount is too big, the maximum allowable amount is 99999999.99');
    }
  }

  /** Below method will add decimal points to amount fields */
  private getDecimalVal(val): string {
    if (val && (/^[-+]?\d*\.?\d*$/.test(val)) && !val.includes('.')) {
      let tmp = val.slice(0, -2) + '.' + val.slice(-2);
      val = tmp;
    }
    return val;
  }

  /** Add new account details row */
  addFieldValue(accNum, amt, pol, sus, alpha, drFlag) {
    const control = <FormArray>this.accountDetailsForm.get('accountsArray');
    let fbGrpItem = this.initAccountDetails(accNum, amt, pol, sus, alpha, drFlag);
    control.push(fbGrpItem);
    if (accNum)
      this.validateAccoutDetails(fbGrpItem);
  }

  /** set the acoount details */
  private setAccoutingData(totalCheckAmount: string, accountList: FormArray) {
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountingDetails = [];
    accountList.controls.forEach(control => {
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountingDetails.push({
        accountNumber: control.value.accountNumber,
        amount: this.getDecimalVal(control.value.amount),
        policyNumber: control.value.policyNumber,
        suspenseNumber: control.value.suspenseNumber,
        alpha: control.value.alpha,
        debitCreditFlag: control.value.debitCreditFlag
      });
    }
    );
  }
  get accountsArrayControls() {
    return <FormArray>this.accountDetailsForm.get('accountsArray');
  }

  /** Below method will call Onclick of submit*/
  onSubmit(form: FormGroup): void {
    if (form.valid) {
      this.errorMsg = '';
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.paymentAdminSystem = this.accountDetailsForm.get('paymentAdminSystem').value;
      let arrayControl = this.accountDetailsForm.get('accountsArray') as FormArray;
      let debitSum: number = 0;
      let creditSum: number = 0;

      if (!arrayControl.controls.some(control => control.value.accountNumber
        && control.value.amount)) {

        if (!arrayControl.controls.some(control => control.value.accountNumber)) {
          this.errorMsg = 'Account number is required.';
        } else if (!arrayControl.controls.some(control => control.value.amount)) {
          this.errorMsg = 'Amount is required.';
        }
      }
      else {
        this.errorMsgAccntFailed = '';
        arrayControl.controls.filter(control => control.value.debitCreditFlag == 'D'
          && control.value.amount).forEach(control => {
            debitSum = debitSum + parseFloat(this.getDecimalVal(control.value.amount));
          });

        arrayControl.controls.filter(control => control.value.debitCreditFlag == 'C'
          && control.value.amount).forEach(control => {
            creditSum = creditSum + parseFloat(this.getDecimalVal(control.value.amount));
          });

        const total = (debitSum - creditSum).toFixed(2);
        if (total != parseFloat(this.accountDetailsForm.get('totalCheckAmount').value).toFixed(2)) {
          this.errorMsg = 'The debits minus the credits do not equal the total check amount';
        }
        else {
          this.errorMsgAccntFailed = '';
          this.setAccoutingData(parseFloat(this.accountDetailsForm.get('totalCheckAmount').value).toFixed(2), arrayControl);
          if (this.routeClientId) {
            this.disbursementService.reqtDisbursementDataModel.disbursementModel.status = 'Complete';
            this.onEditOrUpdate(this.routeClientId, this.disbursementService.reqtDisbursementDataModel.disbursementModel);

          } else {

            this.disbursementService.reqtDisbursementDataModel.disbursementModel.status = 'Complete';
            this.onSave(this.disbursementService.reqtDisbursementDataModel.disbursementModel);
          }
        }
      }
    }
    else {
      this.validationService.validateAllFormFields(form);
    }
  }

  /** below method is to save disbursement */
  onSave(disbursementModel: RequestDisbursementModel): void {
    this.setLocalStorage('localStoragePhoneNumber', disbursementModel.phoneNumber);
    this.disbursementService.saveDisbursement(disbursementModel).subscribe(data => {
      this.logger.info('Save disbursement data =>', data);
      this.disbursementService.reset();
      this.router.navigate(['home/confirmation'], { queryParams: { type: 'disbursement' } });
      // this.setLocalStorage('localStoragePhoneNumber', disbursementModel.phoneNumber);
    },
      (error: any) => {
        this.logger.error('Error Occurred while saving', error.errorStat);
        if (error.errorStat == '404') {
          this.errorMsg = '404 not found';
        } else if (error.errorStat == '403') {
          this.errorMsg = 'You’re not authorized to perform that action.';
        } else if (error.errorStat == '500') {
          this.errorMsg = 'Your request cannot be completed at this time.  Please try again';
        } else if (error.errorStat == '400') {
          this.errorMsgAccntFailed = error.errorField + error.errorText;;
        }
        else {
          this.errorMsg = 'An error occurred while processing this request.';
        }
      });
  }

  /** Edit or Update existing disbursement */
  onEditOrUpdate(clientId: string, disbursementModel: RequestDisbursementModel): void {
    this.setLocalStorage('localStoragePhoneNumber', disbursementModel.phoneNumber);
    this.disbursementService.updateDisbursement(clientId, disbursementModel).subscribe(data => {
      this.logger.info('Edit Or update disbursement data =>', data);
      this.disbursementService.reset();
      this.router.navigate(['home/confirmation'], { queryParams: { type: 'disbursement' } });
    },
      (err: any) => {
        this.logger.error('Error Occurred while updating disbursement', err.errorStat);
        if (err.errorStat == '404') {
          this.errorMsg = '404 not found';
        } else if (err.errorStat == '403') {
          this.errorMsg = 'You’re not authorized to perform that action.';
        } else if (err.errorStat == '500') {
          this.errorMsg = 'Your request cannot be completed at this time.  Please try again';
        } else if (err.errorStat == '400') {
          this.errorMsgAccntFailed = err.errorField + err.errorText;;
        } else {
          this.errorMsg = 'An error occurred while processing this request.';
        }
      });
  }

  /** edit the existing disbursement record along with the account details*/
  editOrUpdateActDetails() {
    this.sharedService.currentTransaction.subscribe(
      (response) => {
        this.yourTransactionsList = response;
        this.edit_paymentAdminSys = this.yourTransactionsList.paymentAdminSystem;
        this.accountDetailsForm.get('paymentAdminSystem').setValue(this.edit_paymentAdminSys);
        this.data_editAccDetails = this.yourTransactionsList.accountingDetails;
        if (this.data_editAccDetails != null && this.data_editAccDetails.length > 0) {
          let arrayControl = this.accountDetailsForm.get('accountsArray') as FormArray;
          arrayControl.controls.splice(0, arrayControl.controls.length);
          let creditSum: number = 0;
          let debitSum: number = 0;
          for (let deatils of this.data_editAccDetails) {
            this.addFieldValue(deatils.accountNumber,
              parseFloat(deatils.amount).toFixed(2),
              deatils.policyNumber, deatils.suspenseNumber, deatils.alpha, deatils.debitCreditFlag);

            if (deatils.debitCreditFlag == 'D') {
              debitSum = this.yourTransactionsList.accountingDetails.filter((item) => item.debitCreditFlag == 'D')
                .map((item) => +item.amount)
                .reduce((sum, current) => sum + current);
            }
            if (deatils.debitCreditFlag == 'C') {
              creditSum = this.yourTransactionsList.accountingDetails.filter((item) => item.debitCreditFlag == 'C')
                .map((item) => +item.amount)
                .reduce((sum, current) => sum + current);
            }
          }
          const total = (debitSum - creditSum).toFixed(2);
          this.sumTotalAmount = parseFloat(total).toFixed(2);
          this.accountDetailsForm.get('totalCheckAmount').setValue(this.sumTotalAmount);

        }
      },
      (err: any) => {
        this.logger.error('Error Occurred while editing disbursement', err.errorStat);
      });
  }

  setLocalStorage(key, value) {
    localStorage.setItem(key, value);
  }

  /** Onclick of this button navigate to previous page */
  onClickPrevious() {
    let arrayControl = this.accountDetailsForm.get('accountsArray') as FormArray;
    this.setAccoutingDataBack(this.accountDetailsForm.get('totalCheckAmount').value, arrayControl);
    this.router.navigate(['home/payeeAddress', 'payeeAddress'], { queryParams: { 'clientId': this.routeClientId, edit: 'p' } });
  }

  /** set the acoount details */
  private setAccoutingDataBack(totalCheckAmount: string, accountList: FormArray) {
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.totalCheckAmount = totalCheckAmount;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.paymentAdminSystem = this.accountDetailsForm.get('paymentAdminSystem').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountingDetailsBack = [];
    accountList.controls.forEach(control => {
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountingDetailsBack.push({
        accountNumber: control.value.accountNumber,
        amount: this.getDecimalVal(control.value.amount),
        policyNumber: control.value.policyNumber,
        suspenseNumber: control.value.suspenseNumber,
        alpha: control.value.alpha,
        debitCreditFlag: control.value.debitCreditFlag
      });
    }
    );
  }

  /** Onclick of Cancel navigate to home page */
  onClickCancel() {
    this.disbursementService.reset();
    this.router.navigate(['home/yourTransactions']);
  }

  /** Onclick of save as draft method */
  onSaveAsDraft(form: any) {
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.status = 'Draft';
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.paymentAdminSystem = this.accountDetailsForm.get('paymentAdminSystem').value;
    let arrayControl = this.accountDetailsForm.get('accountsArray') as FormArray;
    this.setAccoutingData(parseFloat(this.accountDetailsForm.get('totalCheckAmount').value).toFixed(2), arrayControl);
    if (this.routeClientId) {
        this.disbursementService.updateDisbursement(this.routeClientId, this.disbursementService.reqtDisbursementDataModel.disbursementModel)
          .subscribe(
          (data) => {
            this.disbursementService.reset();
            this.router.navigate(['home/yourTransactions']);
          },
          (error: any) => {
            this.logger.error('Error Occurred while saving', error.errorStat);
            if (error.errorStat == '404') {
              this.errorMsg = '404 not found';
            } else if (error.errorStat == '403') {
              this.errorMsg = 'You’re not authorized to perform that action.';
            } else if (error.errorStat == '500') {
              this.errorMsg = 'Your request cannot be completed at this time.  Please try again';
            } else if (error.errorStat == '400') {
              this.errorMsgAccntFailed = error.errorField + error.errorText;
            }
            else {
              this.errorMsg = 'An error occurred while processing this request.';
            }
          });
      
    } else {

        this.disbursementService.saveAsDraft(this.disbursementService.reqtDisbursementDataModel.disbursementModel)
          .subscribe(
          (data) => {
            this.disbursementService.reset();
            this.router.navigate(['home/yourTransactions']);
          },
          (error: any) => {
            this.logger.error('Error Occurred while saving', error.errorStat);
            if (error.errorStat == '404') {
              this.errorMsg = '404 not found';
            } else if (error.errorStat == '403') {
              this.errorMsg = 'You’re not authorized to perform that action.';
            } else if (error.errorStat == '500') {
              this.errorMsg = 'Your request cannot be completed at this time.  Please try again';
            } else if (error.errorStat == '400') {
              this.errorMsgAccntFailed = error.errorField + error.errorText;
            } else {
              this.errorMsg = 'An error occurred while processing this request.';
            }
          });
     
    }
  }

  getPaymentAdminSystem(){
    this.commondropdownService.getSourceSystems().subscribe(
      (resp) => {
        this.paymentAdminSystem = resp;
      },
      (error: any) => {
        this.logger.error('Error Occurred while loading Payment Admin Systems dropdown', error.errorStat);
      });
  }

  getReqDisbursementDataModel(): ContainerDisbursementDataModel {
    return this.disbursementService.reqtDisbursementDataModel;
  }

  setReqDisbursementDataModel(reqtDisbursementDataModel: ContainerDisbursementDataModel) {
    this.disbursementService.reqtDisbursementDataModel = reqtDisbursementDataModel;
  }
}