import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { CommondropdownService } from './../../services/commondropdown.service';
import { ValidationService } from './../../services/validation.service';
import { SharedService } from '../../../shared/services/shared.service';
import { States } from '../../../shared/models/states';
import { DisbursementService } from '../disbursement.service';
import { ContainerDisbursementDataModel } from '../models/reqDisbursement';
import { TransactionsResponse } from '../../../your-transactions/models/all-trasactions';
import { NGXLogger } from 'ngx-logger';
import { error } from 'util';
import { CheckType } from '@angular/core/src/view';


@Component({
  selector: 'app-payee-info',
  templateUrl: './payee-info.component.html',
  styleUrls: ['./payee-info.component.css'],
  providers: [NGXLogger]
})
export class PayeeInfoComponent implements OnInit {
  editCurrenTransaction: TransactionsResponse[] = [];
  payeeInfoForm: FormGroup;
  showAddressFileds: boolean = true;
  payeeState: States[] = [];
  routePath: string;
  routeClientId: string;
  responsePayeeAddress: any;
  enableCountry: boolean = false;
  enableState: boolean = true;
  enablePayeeDetails: boolean = false;
  enableP
  check_EFT_Flag: string;
  payeeCode;
  // payeeYesOrNoFlag;
  data_editPayeeName;
  data_editPayeeAdd1;
  data_editPayeeAdd2;
  data_editPayeeAdd3;
  data_editPayeeAdd4;
  data_editPayeeCity;
  data_editPayeeState;
  data_editPayeeZip;
  data_editPayeeZipPlus;
  data_editDesc1;
  data_editDesc2;
  data_status;
  data_editOtherCountry;
  data_editIntOrUSFlag;
  data_editAccountType;
  data_editRoutingNumber;
  data_editAccountNumber;
  data_editCountry;
  statusFailMsg = '';
  checkType: any;
  constructor(
    private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private commonDropdownService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService,
    private disbursementService: DisbursementService,
    private logger: NGXLogger

  ) { }

  ngOnInit() {
    this.routePath = this.activeRoute.snapshot.params['payeeAddress'];
    this.activeRoute.queryParams.subscribe(params => { this.routeClientId = params['clientId'] });
    this.sharedService.currentCheckFlag.subscribe((data) => this.payeeCode = data);
    this.sharedService.currentDescCode.subscribe((data) => { this.data_editDesc1 = data; });
    this.sharedService.currentCheckType.subscribe((data) => { this.checkType = data; });
    // this.sharedService.currentPayeeCdFlag.subscribe((data) => { this.payeeYesOrNoFlag = data; })
    this.statusFailMsg = '';
    this.getStates();
    if (this.routeClientId) {
      if (this.router.url === '/home/payeeAddress/payeeAddress?clientId=' + this.routeClientId) {
        this.editOrUpdatePayee();
      }
    }
    if (this.payeeCode) {
      this.setPayeeAdrress();
    }
    this.hidePayeeAddForEft();
    this.buildForm(this.routePath);
    this.validateStateOrCountry(null);
  }

  buildForm(payeeAddress: string): void {
    this.payeeInfoForm = this.fb.group({
      payeeName: [this.data_editPayeeName || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeName, [this.validationService.validatePayeeName]],
      payeeAddress1: [this.data_editPayeeAdd1 || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress1, [this.validationService.validatePayeeAddress]],
      payeeAddress2: [this.data_editPayeeAdd2 || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress2, [this.validationService.validateAddess]],
      payeeAddress3: [this.data_editPayeeAdd3 || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress3, [this.validationService.validateAddess]],
      payeeAddress4: [this.data_editPayeeAdd4 || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress4, [this.validationService.validateAddess]],
      payeeCity: [this.data_editPayeeCity || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCity, [this.validationService.validatePayeeCity]],
      payeeState: [this.data_editPayeeState || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeState, [this.validationService.validateStates]],
      payeeZip: [this.data_editPayeeZip || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeZip, [this.validationService.validatePayeeZip]],
      description1: [this.data_editDesc1 || this.disbursementService.reqtDisbursementDataModel.disbursementModel.description1, [this.validationService.validatePayeeDescription]],
      description2: [this.data_editDesc2 || this.disbursementService.reqtDisbursementDataModel.disbursementModel.description2],
      payeeZipPlusFour: [this.data_editPayeeZipPlus || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeZipPlusFour],
      usOrIntRadioBtn: [this.data_editIntOrUSFlag || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddressIndicator],
      country: [this.data_editCountry || this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCountry, [this.validationService.validateCountry]],
      routingNumber: [this.data_editRoutingNumber || this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankRouting],
      accountNumber: [this.data_editAccountNumber || this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankAccount],
      accountType: [this.data_editAccountType || this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountType],
    });
  }

  /** below will call on click of Next */
  onSubmit(form: FormGroup) {
    this.validationService.validateUsOrIntFields(form);
    if (this.check_EFT_Flag == 'EFT') {
      this.validationService.validatePayeeAddressforEFT(form);
    }

    if (form.valid) {
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeName =
        this.payeeInfoForm.get('payeeName').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress1 = this.payeeInfoForm.get('payeeAddress1').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress2 = this.payeeInfoForm.get('payeeAddress2').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress3 = this.payeeInfoForm.get('payeeAddress3').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress4 = this.payeeInfoForm.get('payeeAddress4').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCity = this.payeeInfoForm.get('payeeCity').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeState = this.payeeInfoForm.get('payeeState').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.state =
        this.payeeState.filter(state => state.code = this.payeeInfoForm.get('payeeState').value)[0];
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeZip = this.payeeInfoForm.get('payeeZip').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeZipPlusFour = this.payeeInfoForm.get('payeeZipPlusFour').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.description1 = this.payeeInfoForm.get('description1').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.description2 = this.payeeInfoForm.get('description2').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCountry = this.payeeInfoForm.get('country').value;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddressIndicator = this.payeeInfoForm.get('usOrIntRadioBtn').value;
      if (this.payeeCode) {
        this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankAccount = this.data_editAccountNumber;
        this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankRouting = this.data_editRoutingNumber;
        this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountType = this.data_editAccountType;
      }
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.status = 'Complete';
      this.router.navigate(['home/accoutingDetails', 'list'], { queryParams: { 'clientId': this.routeClientId } });

    } else {
      this.validationService.validateAllFormFields(form);
    }

  }

  /** Below method will call on edit Or update payee address */
  editOrUpdatePayee() {
    this.activeRoute.queryParams.subscribe(params => { this.routeClientId = params['clientId'] });
    this.sharedService.currentTransaction.subscribe(
      (result) => {
        this.responsePayeeAddress = result;
        this.data_editPayeeName = this.responsePayeeAddress.payeeName;
        this.data_editPayeeAdd1 = this.responsePayeeAddress.payeeAddress1;
        this.data_editPayeeAdd2 = this.responsePayeeAddress.payeeAddress2;
        this.data_editPayeeAdd3 = this.responsePayeeAddress.payeeAddress3;
        this.data_editPayeeAdd4 = this.responsePayeeAddress.payeeAddress4;
        this.data_editPayeeCity = this.responsePayeeAddress.payeeCity;
        this.data_editPayeeState = this.responsePayeeAddress.payeeState;
        this.data_editPayeeZip = this.responsePayeeAddress.payeeZip;
        this.data_editPayeeZipPlus = this.responsePayeeAddress.payeeZipPlusFour;
        this.data_editOtherCountry = this.responsePayeeAddress.country;
        this.data_editIntOrUSFlag = this.responsePayeeAddress.usOrIntRadioBtn;
        this.data_editDesc1 = this.responsePayeeAddress.description1;
        this.data_editDesc2 = this.responsePayeeAddress.description2;
        this.data_editAccountNumber = this.responsePayeeAddress.bankAccount;
        this.data_editRoutingNumber = this.responsePayeeAddress.bankRouting;
        this.data_editAccountType = this.responsePayeeAddress.accountType;
      },
      (error: any) => {
        this.logger.error('Error Occurred while editing', error.errorStat);
        if (error.errorStat == '404') {
          this.statusFailMsg = '404 not found';
        } else if (error.errorStat == '403') {
          this.statusFailMsg = 'You’re not authorized to perform that action.';
        } else if (error.errorStat == '500') {
          this.statusFailMsg = 'Your request cannot be completed at this time.  Please try again';
        } else if (error.errorStat == '401') {
          window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
        }
        else {
          this.statusFailMsg = 'An error occurred while processing this request.';
        }
      }
    );
  }

  /** Below method will set the payee address to the respective fields*/
  setPayeeAdrress() {
    this.sharedService.currentPayeeAddress.subscribe(
      (data) => {
        this.logger.info('In setPayeeAdrees to the Component', data);
        this.responsePayeeAddress = data[0];
        if (!this.routeClientId) {
          this.data_editPayeeName = this.responsePayeeAddress.payeeName;
          this.data_editPayeeAdd1 = this.responsePayeeAddress.payeeAddress1;
          this.data_editPayeeAdd2 = this.responsePayeeAddress.payeeAddress2;
          this.data_editPayeeAdd3 = this.responsePayeeAddress.payeeAddress3;
          this.data_editPayeeAdd4 = this.responsePayeeAddress.payeeAddress4;
          this.data_editPayeeCity = this.responsePayeeAddress.payeeCity;
          this.data_editPayeeState = this.responsePayeeAddress.payeeState;
          this.data_editPayeeZip = this.responsePayeeAddress.payeeZip;
          this.data_editPayeeZipPlus = this.responsePayeeAddress.payeeZipPlusFour;
          this.data_editOtherCountry = this.responsePayeeAddress.country;
        }
        this.data_editAccountNumber = this.responsePayeeAddress.rdfiAccountNumber;
        this.data_editRoutingNumber = this.responsePayeeAddress.rdfiRoutingNumber;
        this.data_editAccountType = this.responsePayeeAddress.accountType;
      },
      (error: any) => { this.logger.info('Erro Occurred while setting payee adress for disbursement' + error.status + 'Error message' + error.message); }
    );
  }

  /** below method will call onclick of preavious button */
  onClickPrevious() {
    this.getPayeeInfoOnPrevious();
    this.router.navigate(['home/requestDisbursement', 'payee'], { queryParams: { 'clientId': this.routeClientId, edit: 'rd' } });
  }

  /** below method will call onclick of cancel*/
  onClickCancel() {
    this.disbursementService.reset();
    this.router.navigate(['home/yourTransactions']);
  }

  /** below method will call onclick of save as draft */
  onSaveAsDraft(form: any) {

    this.disbursementService.reqtDisbursementDataModel.disbursementModel.status = 'Draft';
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeName =
      this.payeeInfoForm.get('payeeName').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress1 = this.payeeInfoForm.get('payeeAddress1').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress2 = this.payeeInfoForm.get('payeeAddress2').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress3 = this.payeeInfoForm.get('payeeAddress3').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress4 = this.payeeInfoForm.get('payeeAddress4').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCity = this.payeeInfoForm.get('payeeCity').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeState = this.payeeInfoForm.get('payeeState').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.state =
      this.payeeState.filter(state => state.code = this.payeeInfoForm.get('payeeState').value)[0];
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeZip = this.payeeInfoForm.get('payeeZip').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeZipPlusFour = this.payeeInfoForm.get('payeeZipPlusFour').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.description1 = this.payeeInfoForm.get('description1').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.description2 = this.payeeInfoForm.get('description2').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCountry = this.payeeInfoForm.get('country').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddressIndicator = this.payeeInfoForm.get('usOrIntRadioBtn').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankAccount = this.data_editAccountNumber;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankRouting = this.data_editRoutingNumber;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountType = this.data_editAccountType;

    if (this.routeClientId) {
      this.disbursementService.updateDisbursement(this.routeClientId, this.disbursementService.reqtDisbursementDataModel.disbursementModel)
        .subscribe(
        (data) => {
          this.disbursementService.reset();
          this.router.navigate(['home/yourTransactions']);
        },
        (error: any) => {
          this.logger.error('Error Occurred while saving', error.errorStat);
          if (error.errorStat == '404') {
            this.statusFailMsg = '404 not found';
          } else if (error.errorStat == '403') {
            this.statusFailMsg = 'You’re not authorized to perform that action.';
          } else if (error.errorStat == '500') {
            this.statusFailMsg = 'Your request cannot be completed at this time.  Please try again';
          } else if (error.errorStat == '401') {
            window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
          }
          else {
            this.statusFailMsg = 'An error occurred while processing this request.';
          }
        });

    } else {
      this.disbursementService.saveAsDraft(this.disbursementService.reqtDisbursementDataModel.disbursementModel)
        .subscribe(
        (data) => {
          this.disbursementService.reset();
          this.router.navigate(['home/yourTransactions']);
        },
        (error: any) => {
          this.logger.error('Error Occurred while saving' + error.errorStat);
          if (error.errorStat == '404') {
            this.statusFailMsg = '404 not found';
          } else if (error.errorStat == '403') {
            this.statusFailMsg = 'You’re not authorized to perform that action.';
          } else if (error.errorStat == '500') {
            this.statusFailMsg = 'Your request cannot be completed at this time.  Please try again';
          } else if (error.errorStat == '401') {
            window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
          }
          else {
            this.statusFailMsg = 'An error occurred while processing this request.';
          }
        });

    }
  }

  /** render the states or country fields  */
  validateStateOrCountry(control: AbstractControl) {
    let stateOrCountry = this.payeeInfoForm.get('usOrIntRadioBtn');
    if (stateOrCountry.value === 'U.S') {
      this.enableCountry = false;
      this.enableState = true;
      this.showAddressFileds = true;
      this.payeeInfoForm.get('payeeAddress4').setValue(null);
      this.payeeInfoForm.get('country').setValue(null);
    } else if (stateOrCountry.value === 'International') {
      this.enableCountry = true;
      this.enableState = false;
      this.showAddressFileds = false;
      this.payeeInfoForm.get('payeeZipPlusFour').setValue(null);
      this.payeeInfoForm.get('payeeCity').setValue(null);
      this.payeeInfoForm.get('payeeZip').setValue(null);
      this.payeeInfoForm.get('payeeState').setValue(null);
    }
  } // end

  /** get the values in states dropdown **/
  getStates() {
    this.commonDropdownService.getStatesDropdown().subscribe((response) => {
      this.payeeState = response;
    },
      (error: any) => {
        this.logger.error('Error in loading states dropdown', error);
      });
  }

  hidePayeeAddForEft() {
    this.sharedService.eftFlagValue.subscribe(
      (response) => {
        this.check_EFT_Flag = response;
        if (this.check_EFT_Flag == 'EFT') {
          this.enablePayeeDetails = false;
        } else {
          this.enablePayeeDetails = true;
        }
      }
    );
  }

  getPayeeInfoOnPrevious() {
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeName =
      this.payeeInfoForm.get('payeeName').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress1 = this.payeeInfoForm.get('payeeAddress1').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress2 = this.payeeInfoForm.get('payeeAddress2').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress3 = this.payeeInfoForm.get('payeeAddress3').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddress4 = this.payeeInfoForm.get('payeeAddress4').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCity = this.payeeInfoForm.get('payeeCity').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeState = this.payeeInfoForm.get('payeeState').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.state =
      this.payeeState.filter(state => state.code = this.payeeInfoForm.get('payeeState').value)[0];
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeZip = this.payeeInfoForm.get('payeeZip').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeZipPlusFour = this.payeeInfoForm.get('payeeZipPlusFour').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.description1 = this.payeeInfoForm.get('description1').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.description2 = this.payeeInfoForm.get('description2').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeCountry = this.payeeInfoForm.get('country').value;
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.payeeAddressIndicator = this.payeeInfoForm.get('usOrIntRadioBtn').value;
    if (this.payeeCode) {
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankAccount = this.data_editAccountNumber;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.bankRouting = this.data_editRoutingNumber;
      this.disbursementService.reqtDisbursementDataModel.disbursementModel.accountType = this.data_editAccountType;
    }
    this.disbursementService.reqtDisbursementDataModel.disbursementModel.status = 'Complete';
  }

  getReqDisbursementDataModel(): ContainerDisbursementDataModel {
    return this.disbursementService.reqtDisbursementDataModel;
  }

  setReqDisbursementDataModel(reqtDisbursementDataModel: ContainerDisbursementDataModel) {
    this.disbursementService.reqtDisbursementDataModel = reqtDisbursementDataModel;
  }
}
