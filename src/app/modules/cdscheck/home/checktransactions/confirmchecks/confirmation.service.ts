import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ConfirmationService {

    private disbursementVar = new BehaviorSubject<boolean>(false);
    private voidCheckVar = new BehaviorSubject<boolean>(false);
    private replaceCheckVar = new BehaviorSubject<boolean>(false);
    private orgCheckInterface = new BehaviorSubject<boolean>(false);

    currentDisburseMessage = this.disbursementVar.asObservable();
    currentVoidMsg = this.voidCheckVar.asObservable();
    currentReplaceCheckMsg = this.replaceCheckVar.asObservable();
    currentOrgCheckInter = this.orgCheckInterface.asObservable();

    /**On Successful submit of disbursement display the message*/
    setDisbursementMsg(message: boolean) {
        this.disbursementVar.next(message);
    }

    /**On Successful submit of void check display the message*/
    setVoidChecktMsg(message: boolean) {
        this.voidCheckVar.next(message);
    }
    /**On Successful submit of replace check display the message*/
    setReplaceCheckMsg(message: boolean) {
        this.replaceCheckVar.next(message);
    }
    /**On Successful submit of display the interface report message for void and replace*/
    setInterfaceReport(message: boolean) {
        this.orgCheckInterface.next(message);
    }


}