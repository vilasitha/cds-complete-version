import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms/src/model';
import { ConfirmationService } from './confirmation.service';

@Component({
  selector: 'app-confirmchecks',
  templateUrl: './confirmchecks.component.html',
})
export class ConfirmchecksComponent implements OnInit {

  confirmSubmitForm: FormGroup;
  constructor(private confirmationService: ConfirmationService) { }
  disbursementSuccess: boolean;
  voidSuccessMsg: boolean;
  repalceSuccessMsg: boolean;
  originalInterface: boolean;

  ngOnInit() {
    this.confirmationService.currentDisburseMessage.subscribe((resp) => this.disbursementSuccess = resp);
    this.confirmationService.currentReplaceCheckMsg.subscribe((resp) => this.repalceSuccessMsg = resp);
    this.confirmationService.currentVoidMsg.subscribe((resp) => this.voidSuccessMsg = resp);
    this.confirmationService.currentOrgCheckInter.subscribe((resp) => this.originalInterface = resp);
  }

}
