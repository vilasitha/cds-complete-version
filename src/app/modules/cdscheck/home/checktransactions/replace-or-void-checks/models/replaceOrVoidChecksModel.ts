import { Companies } from "../../../shared/models/companies";
import { States } from "../../../shared/models/states";
import { MailCodes } from "../../../shared/models/mailcodes";
import { ReasonCodes } from "../../../shared/models/reasons";
import { AltVoidAccounts } from "../../../shared/models/voidAccounts";
import { AccountingDetails } from "../../disbursement/models/account-details";
import { BankNames } from "../../../shared/models/bank-names";

export class ContainerReplaceOrVoidChecksModel{

    replaceOrVoidChecksModel: ReplaceOrVoidChecksModel;

    constructor(){
        this.replaceOrVoidChecksModel = new ReplaceOrVoidChecksModel;
    }
        accountingDetails: any;
reset(){
        this.replaceOrVoidChecksModel = new ReplaceOrVoidChecksModel;        
    }
}




export class ReplaceOrVoidChecksModel{

    bankNames: BankNames;
    companyCode: string="";
    
    state: States;
    stateCode: string = "";
    payeeState: string;
    
    mail: MailCodes;
    mailCode: string = "MA";

    reasons: ReasonCodes;
    reason: string="";

    // voidTo: AltVoidAccounts;
    voidTo: string ="Original Account";
    voidAccount: string;
    
    transactionType: string;
    sourceCode: string;
    phoneNumber: string;
    payeeName: string;
    payeeAddress1: string;
    payeeAddress2: string;
    payeeAddress3: string ;
    payeeAddress4: string ;
    payeeCity: string;
  
    payeeZip: string;
    payeeZipPlusFour: string;
    payeeCountry: string;
    paymentType: string;

    originalCheckFlag: string;
    originalCheckNumber: string;
    voidToDate: string;
    ssn: string;
    checkDate: string;
    alpha:  string;
    checkAmount: string;
    lastModified: string;
    descriptionCode: string;

    description1: string;
    description2: string;
    status: string;
    bankAccountNumber: string;
    clientId: string;
    accountingDetails: Array<AccountingDetails> =[];
    payeeAddressIndicator: string = "U.S";
    taxId: string;

    originalCheckBankRouting : string;
    originalCheckBankAccount : string;
    paymentAdminSystem: string;
}