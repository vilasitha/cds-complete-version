import { AccountingDetails } from "../../disbursement/models/account-details";

export class RequestChecksModel{

    checkDate: string;
    checkAmount: string;
    companyCode: string;
    companyNumberCode: string;
    sourceCode: string;
    paymentType: string;
    payeeCode: string;
    description: string;
    taxId: string;
    payeeName: string; 
    payeeAddress1: string;
    payeeAddress2: string;
    payeeAddress3: string;
    payeeAddress4: string;
    payeeCity: string;
    payeeState: string;
    payeeZip: string; 
    payeeZipPlusFour: string;
    payeeCountry: string;
    transactionType: string;
    stateCode: string;
    mailCode: string;
    reason: string;
    description1: string;
    description2: string;
    status: string;
    alpha:  string;
    clientId: string;
    originalCheckFlag: string;
    phoneNumber: string;    
    originalCheckNumber: string;
    accountingDetails: Array<AccountingDetails> =[];
    bankAccountNumber: string;
    originalCheckBankRouting : string;
    originalCheckBankAccount : string;
    paymentAdminSystem: string;

}