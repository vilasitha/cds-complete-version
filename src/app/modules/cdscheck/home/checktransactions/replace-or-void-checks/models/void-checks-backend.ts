
export class VoidChecksModel{

    companyCode: string;
    originalCheckNumber: string;
    phoneNumber: string;  
    sourceCode: string;
    payeeName: string; 
    payeeAddress1: string;
    payeeAddress2: string;
    payeeAddress3: string;
    payeeCity: string;
    payeeZip: string; 
    payeeZipPlusFour: string;
    payeeCountry: string;
    checkAmount: string;
    checkDate: string;
    voidTodate:string;
    description1: string;
    stateCode: string;
    reason: string;
    payeeCode: string;
    description: string;
    transactionType: string;
    status: string;
    alpha:  string;
    clientId: string;
    ssn: string;
    originalCheckFlag: string;
    voidTo: string;
    bankAccountNumber: string;
    originalCheckBankRouting : string;
    originalCheckBankAccount : string;
}