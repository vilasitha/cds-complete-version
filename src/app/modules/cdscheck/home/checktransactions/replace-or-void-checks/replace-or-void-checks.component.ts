
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Location } from '@angular/common';


import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { validateConfig } from '@angular/router/src/config';
import { Params } from '@angular/router/src/shared';
import { Companies } from '../../shared/models/companies';
import { HomeService } from '../../services/home.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';
import { HttpParams } from '@angular/common/http/src/params';
import { SharedService } from '../../shared/services/shared.service';
import { DisbursementService } from '../disbursement/disbursement.service';
import { ContainerDisbursementDataModel } from '../disbursement/models/reqDisbursement';
import { NGXLogger } from 'ngx-logger';
import { ReplaceOrVoidChecksService } from './replace-or-void-checks.service';
import { ContainerReplaceOrVoidChecksModel } from './models/replaceOrVoidChecksModel';
import { CommonRestService } from '../services/common-rest-service.service';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpResponse } from 'selenium-webdriver/http';
import { RequestChecksModel } from './models/checks-request-backend';
import { BankNames } from '../../shared/models/bank-names';
import { PayeeTableService } from '../../maintenance/payee-table/services/payee-table.service';

@Component({
  selector: 'app-replace-or-void-checks',
  templateUrl: './replace-or-void-checks.component.html',
  styleUrls: ['./replace-or-void-checks.component.css'],
  providers: [NGXLogger]
})
export class ReplaceOrVoidChecksComponent implements OnInit {
  currentTransactions: TransactionsResponse[] = [];
  replaceCheckForm: FormGroup;
  bankNames: BankNames[] = [];
  checkType: string;
  requestModelData: RequestChecksModel[] = [];
  // bankCompanyCode: any;
  local_phoneNumber = '';
  routePath: string;
  errorCCCN = '';
  errorMsgSrcCode = '';
  routeClientId: string;
  replaceOrVoidServerResp: any;
  edit_BankName;
  edit_SourceCode;
  edit_CheckNumber;
  bank_companyCode;
  constructor(
    private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private companyNamesService: CommondropdownService,
    private location: Location,
    private validationService: ValidationService,
    private homeService: HomeService,
    private sharedService: SharedService,
    private logger: NGXLogger,
    private replaceOrVoidChecksService: ReplaceOrVoidChecksService,
    private commonRestService: CommonRestService,
    private disbursementService: DisbursementService,
    private payeeMaintService: PayeeTableService
  ) {
  }

  ngOnInit() {
    if (this.router.url == '/home/replaceChecks/clientId?type=Replace' || this.router.url == '/home/voidChecks/clientId?type=Void') {
      if (this.replaceOrVoidChecksService || this.replaceOrVoidChecksService.containerChecksModel) {
        this.replaceOrVoidChecksService.reset();
      }
    }
    if (this.disbursementService || this.disbursementService.reqtDisbursementDataModel) {
      this.disbursementService.reqtDisbursementDataModel.reset();
    }
    this.payeeMaintService.reset();
    this.activeRoute.queryParams.subscribe(params => { this.routeClientId = params['clientId']; });
    this.activeRoute.queryParams.subscribe(params => { this.checkType = params['type']; });
    this.routePath = this.activeRoute.snapshot.params['clientId'];
    if (this.router.url == '/home/replaceChecks/clientId?type=Replace&edit=rc&clientId=' + this.routeClientId || this.router.url == '/home/voidChecks/clientId?type=Void&edit=vc&clientId=' + this.routeClientId) {
      this.editOrUpdate();
    }
    this.displayBankNames();
    this.errorMsgSrcCode = '';
    this.errorCCCN = '';
    this.buildForm(this.routePath);
    if (this.router.url == '/home/voidChecks/clientId?type=Void' || this.router.url == '/home/replaceChecks/clientId?type=Replace') {
      this.getLocalStorage('localStoragePhoneNumber');
    }

  }

  buildForm(clientId: string): void {
    this.replaceCheckForm = this.fb.group({
      companyCode: [this.edit_BankName || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.companyCode, [this.validationService.companyNamesValidation]],
      sourceCode: [this.edit_SourceCode || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.sourceCode, [this.validationService.validateSourceCode]],
      checkNumber: [this.edit_CheckNumber || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckNumber, [Validators.pattern('[0-9]+'), this.validationService.checkNumberValidation]],
      phoneNumber: [this.local_phoneNumber || this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.phoneNumber]
    });
  }

  /** display the values in company names dropdown **/
  displayBankNames() {
    this.companyNamesService.getBanksNames().subscribe((res) => {
      this.bankNames = res;
    }, (error: any) => {
      this.logger.error('Error in loading bank names table');
    });
  }

  //Onclick of cancel
  navigateToHome() {
    this.replaceOrVoidChecksService.reset();
    this.router.navigate(['home/yourTransactions']);
  }

  /**Onsubmit of replce or void checks */
  onNext(form: FormGroup): void {
    if (form.valid) {
      this.setReplaceOrVoidChecksData();
      this.commonRestService.getPayeeInfo(this.bank_companyCode[0],
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckNumber).subscribe(

        (data) => {
          this.logger.info('ReplaceOrVoidChecksComponent setAddressInfo API:', data);
          this.requestModelData = data;
          this.sharedService.setPayeeAddreesInfo(this.requestModelData);
          if (this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.sourceCode) {
            this.commonRestService.validateSouceCode(this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.sourceCode).subscribe(
              (response) => {
                if (this.checkType == 'Replace') {
                  this.router.navigate(['home/verifyCheckInfo', 'check'], { queryParams: { type: this.checkType } });
                } else if (this.checkType == 'Void') {
                  this.router.navigate(['home/verifyVoidCheckInfo', 'check'], { queryParams: { type: this.checkType } });
                }
              }, (error: any) => {
                this.logger.error('Source code Validation failed', error.response);
                if (error.errorStat == '404') {
                  this.errorMsgSrcCode = 'Source code is Invalid.';
                } else if (error.errorStat == '400') {
                  this.errorMsgSrcCode = error.errorText;

                } else if (error.errorStat == '401') {
                  window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
                }
                else {
                  this.errorMsgSrcCode = null;
                }
              }
            );
          }
        },
        (error: any) => {

          this.logger.error('<== Company code and Check Number Validation failed ==>', error);
          if (error.errorStat == '404') {
            this.errorCCCN = 'Please select a valid company code/check number  combination.';
          } else if (error.errorStat == '400') {
            this.errorCCCN = error.errorText;
          } else if (error.errorStat == '403') {
            this.errorCCCN = 'You’re not authorized to perform that action.';
          } else if (error.errorStat == '500') {
            this.errorCCCN = 'Your request cannot be completed at this time.  Please try again';
          } else if (error.errorStat == '401') {
            window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
          } else {
            this.errorCCCN = 'An error occurred while processing this request.';
          }
        });

    } else {
      this.validationService.validateAllFormFields(form);
    }
  } //end


  setReplaceOrVoidChecksData() {
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.companyCode = this.replaceCheckForm.get('companyCode').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.sourceCode = this.replaceCheckForm.get('sourceCode').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.originalCheckNumber = this.replaceCheckForm.get('checkNumber').value;
    this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.phoneNumber = this.replaceCheckForm.get('phoneNumber').value;
    const bankAccountName = this.replaceCheckForm.get('companyCode').value;
    this.bank_companyCode = bankAccountName.split(',');
  }

  editOrUpdate() {
    this.sharedService.currentTransaction.subscribe(
      (response) => {
        this.replaceOrVoidServerResp = response;
        // this.edit_BankName = this.replaceOrVoidServerResp.companyCode + ',' + this.replaceOrVoidServerResp.originalCheckBankRouting + ',' + this.replaceOrVoidServerResp.originalCheckBankAccount;
        this.edit_BankName = this.replaceOrVoidServerResp.companyCode + ',' +  this.replaceOrVoidServerResp.originalCheckBankAccount;
        
        this.edit_SourceCode = this.replaceOrVoidServerResp.sourceCode;
        this.replaceOrVoidChecksService.containerChecksModel.replaceOrVoidChecksModel.phoneNumber = this.replaceOrVoidServerResp.phoneNumber;
        this.edit_CheckNumber = this.replaceOrVoidServerResp.originalCheckNumber
      },
      (error: any) => {
        this.logger.error('Error occurred while editing Bank names, check number, source code', error);
      });
  }

  getReqDisbursementDataModel(): ContainerReplaceOrVoidChecksModel {
    return this.replaceOrVoidChecksService.containerChecksModel;
  }

  setReqDisbursementDataModel(containerDataModel: ContainerReplaceOrVoidChecksModel) {
    this.replaceOrVoidChecksService.containerChecksModel = containerDataModel;
  }

  getLocalStorage(key) {
    const item = localStorage.getItem(key);
    this.logger.info('item', item, 'key', key);
    if (item && item != 'null' && item != 'undefined'  && key == 'localStoragePhoneNumber') {
      this.logger.info('Local storage values =>', item, key);
      this.local_phoneNumber = item;
      this.replaceCheckForm.get('phoneNumber').setValue(this.local_phoneNumber);
    } else {
      if (key == 'localStoragePhoneNumber' && (item === 'undefined' || item == "null")) {
        this.logger.info('Local storage =>', item);
        this.replaceCheckForm.get('phoneNumber').setValue(null);
      }
    }
  }
}

