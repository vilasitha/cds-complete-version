import {  Injectable } from "@angular/core";
import { RequestChecksModel } from '../models/checks-request-backend'
import { ReplaceOrVoidChecksModel } from "../models/replaceOrVoidChecksModel";
import { VoidChecksModel } from "../models/void-checks-backend";

@Injectable()
export class ChecksParserService{

    private requestChecksModel : RequestChecksModel;
    private voidChecksModel: VoidChecksModel;

    constructor(){ }
    convertUIModelToBackEndModel(request: ReplaceOrVoidChecksModel) : RequestChecksModel{
         this.requestChecksModel = new RequestChecksModel();
         this.requestChecksModel.originalCheckNumber = request.originalCheckNumber;
         this.requestChecksModel.checkDate = request.checkDate;
         this.requestChecksModel.checkAmount = request.checkAmount;
         this.requestChecksModel.clientId = request.clientId;
         this.requestChecksModel.sourceCode = request.sourceCode;
         this.requestChecksModel.paymentType = request.paymentType; 
         this.requestChecksModel.description = request.descriptionCode;
         this.requestChecksModel.taxId = request.taxId;
         this.requestChecksModel.payeeName = request.payeeName;
         this.requestChecksModel.payeeAddress1 = request.payeeAddress1;
         this.requestChecksModel.payeeAddress2 = request.payeeAddress2;
         this.requestChecksModel.payeeAddress3 = request.payeeAddress3;
         this.requestChecksModel.payeeAddress4 = request.payeeAddress4;
         this.requestChecksModel.companyCode = request.companyCode;
         this.requestChecksModel.payeeState = request.payeeState;
         this.requestChecksModel.payeeCity = request.payeeCity;
         this.requestChecksModel.payeeZip = request.payeeZip; 
         this.requestChecksModel.payeeZipPlusFour = request.payeeZipPlusFour;       
         this.requestChecksModel.payeeCountry = request.payeeCountry;    
         this.requestChecksModel.stateCode = request.stateCode;
         this.requestChecksModel.alpha= request.alpha;
         this.requestChecksModel.description1= request.description1;
         this.requestChecksModel.description2 = request.description2;
         this.requestChecksModel.reason= request.reason;
         this.requestChecksModel.mailCode= request.mailCode;
         this.requestChecksModel.transactionType = request.transactionType;
         this.requestChecksModel.originalCheckFlag = request.originalCheckFlag;
         this.requestChecksModel.phoneNumber = request.phoneNumber;
         this.requestChecksModel.status = request.status;
         this.requestChecksModel.transactionType= request.transactionType;
         this.requestChecksModel.accountingDetails = request.accountingDetails;
         this.requestChecksModel.bankAccountNumber = request.bankAccountNumber;
         this.requestChecksModel.originalCheckBankAccount = request.originalCheckBankAccount;
         this.requestChecksModel.originalCheckBankRouting = request.originalCheckBankRouting;
         this.requestChecksModel.paymentAdminSystem = request.paymentAdminSystem;
         return this.requestChecksModel;
    }       
    
    convertUIVoidModelToBackEndModel(request: ReplaceOrVoidChecksModel): VoidChecksModel{
        this.voidChecksModel = new VoidChecksModel();
        this.voidChecksModel.originalCheckNumber = request.originalCheckNumber;
        this.voidChecksModel.checkDate = request.checkDate;
        this.voidChecksModel.checkAmount = request.checkAmount;
        this.voidChecksModel.clientId = request.clientId;
        this.voidChecksModel.sourceCode = request.sourceCode;
        this.voidChecksModel.description = request.descriptionCode;
        this.voidChecksModel.payeeName = request.payeeName;
        this.voidChecksModel.payeeAddress1 = request.payeeAddress1;
        this.voidChecksModel.payeeAddress2 = request.payeeAddress2;
        this.voidChecksModel.payeeAddress3 = request.payeeAddress3;
        this.voidChecksModel.companyCode = request.companyCode;
        this.voidChecksModel.payeeCity = request.payeeCity;
        this.voidChecksModel.payeeZip = request.payeeZip; 
        this.voidChecksModel.payeeZipPlusFour = request.payeeZipPlusFour;        
        this.voidChecksModel.payeeCountry = request.payeeCountry;    
        this.voidChecksModel.stateCode = request.stateCode;
        this.voidChecksModel.alpha= request.alpha;
        this.voidChecksModel.reason= request.reason;  
        this.voidChecksModel.transactionType = request.transactionType;
        this.voidChecksModel.originalCheckFlag = request.originalCheckFlag;
        this.voidChecksModel.phoneNumber = request.phoneNumber;
        this.voidChecksModel.status = request.status;
        this.voidChecksModel.ssn = request.ssn;
        this.voidChecksModel.voidTodate= request.voidToDate; 
        this.voidChecksModel.voidTo = request.voidAccount;
        this.voidChecksModel.transactionType= request.transactionType;
        this.voidChecksModel.bankAccountNumber = request.bankAccountNumber;
        this.voidChecksModel.originalCheckBankAccount = request.originalCheckBankAccount;
        this.voidChecksModel.originalCheckBankRouting = request.originalCheckBankRouting
        return this.voidChecksModel;
    }

}