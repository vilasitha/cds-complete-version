import { Injectable } from '@angular/core';
import { ContainerReplaceOrVoidChecksModel, ReplaceOrVoidChecksModel } from './models/replaceOrVoidChecksModel';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Constants } from '../../shared/constants/constants';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from '../error-messages/services/http-error-handler.service';
import { ChecksParserService } from './services/replace-or-void-checks-parser.service';
import { RequestChecksModel } from './models/checks-request-backend';
import { VoidChecksModel } from './models/void-checks-backend';
import { Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

const httpHeaderOptions = {
  headers: new HttpHeaders({
    // 'Content-Type': 'application/json',
    // 'Accept': 'application/json',
    'Cache-Control': 'no-store',
    'X-Requested-With': 'test'
  })
};

@Injectable()
export class ReplaceOrVoidChecksService {
  containerChecksModel: ContainerReplaceOrVoidChecksModel = new ContainerReplaceOrVoidChecksModel();

  reset() {
    this.containerChecksModel = new ContainerReplaceOrVoidChecksModel();
  }
  constructor(private httpClient: HttpClient,
    private checksParserService: ChecksParserService
  ) {
  }


  updateReplaceChecks(clientId: string, request: ReplaceOrVoidChecksModel): Observable<any> {
    const url = `${Constants.REST_REQ_API_URL}/${clientId}`;
    const requestModel: RequestChecksModel = this.checksParserService.convertUIModelToBackEndModel(request)
    return this.httpClient.put(url, requestModel, httpHeaderOptions)
      .map((resp: Response) => {
      })
      .catch(this.handleError)
  }

  updateVoidChecks(clientId: string, request: ReplaceOrVoidChecksModel): Observable<any> {
    const url = `${Constants.REST_REQ_API_URL}/${clientId}`;
    const requestModel: VoidChecksModel =
      this.checksParserService.convertUIVoidModelToBackEndModel(request);
    return this.httpClient.put(url, requestModel, httpHeaderOptions)
      .map((resp: Response) => {
      })
      .catch(this.handleError)
  }

  saveReplaceChecks(request: ReplaceOrVoidChecksModel): Observable<any> {
    const requestModel: RequestChecksModel = this.checksParserService.convertUIModelToBackEndModel(request);
    return this.httpClient.post(Constants.REST_REQ_API_URL, requestModel, httpHeaderOptions)
      .map((resp: Response) => {

      })
      .catch(this.handleError)
  }
  saveVoidChecks(request: ReplaceOrVoidChecksModel): Observable<any> {
    const requestModel: VoidChecksModel =
      this.checksParserService.convertUIVoidModelToBackEndModel(request);
    return this.httpClient.post(Constants.REST_REQ_API_URL, requestModel, httpHeaderOptions)
      .map((resp: Response) => {
      })
      .catch(this.handleError)
  }

  deleteChecks(id: string): Observable<ReplaceOrVoidChecksModel> {
    const url = `${Constants.REST_REQ_API_URL}/${id}`;
    return this.httpClient.delete(url, httpHeaderOptions)
      .map((res: Response) => {
      }).
      catch(this.handleError)
  }
  private handleError(error: any) {
    let errorObj: any = {
      errorMessage: error.messages,
      errorStat: error.status,
      // errorText: error.statusText,
      errorText: error.error ? error.error[0].code : error.statusText
    }
    return Observable.throw(errorObj);
  }



}
