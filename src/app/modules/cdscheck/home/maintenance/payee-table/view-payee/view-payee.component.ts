import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms/src/model';
import { NGXLogger } from 'ngx-logger';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, NavigationExtras, Params } from '@angular/router';
import { PayeeTableService } from '../services/payee-table.service';
import { States } from '../../../shared/models/states';
import { SharedService } from '../../../shared/services/shared.service';
import { CommondropdownService } from '../../../checktransactions/services/commondropdown.service';

@Component({
  selector: 'app-view-payee',
  templateUrl: './view-payee.component.html',
  styleUrls: ['./view-payee.component.css'],
  providers: [NGXLogger]
})
export class ViewPayeeComponent implements OnInit {
  payeeStates: States[];
  viewPayeeInfoForm: FormGroup;
  urlId: string;
  payeeServerResponse;
  errorStatusMsg = '';

  constructor(private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private payeeService: PayeeTableService,
    private logger: NGXLogger, 
    private sharedService: SharedService,
  private commonDropdownService: CommondropdownService) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => { this.urlId = params['id']; });
    this.errorStatusMsg = '';
    this.getStates();
    if (this.urlId) {
      this.getPayeeInformationById();
    }
    this.buildForm();
    this.viewPayeeInfoForm.disable();
  }

  buildForm(): void {
    this.viewPayeeInfoForm = this.fb.group({
      payeeCode: [this.payeeServerResponse.payeeCode || '--NA--'],
      companyCode: [this.payeeServerResponse.companyCode || '--NA--'],
      sourceCode: [this.payeeServerResponse.sourceCode || '--NA--'],
      legacySourceCode: [this.payeeServerResponse.legacySourceCode || '--NA--'],
      maxAmount: [this.payeeServerResponse.maxAmount || '--NA--'],
      routingNumber: [this.payeeServerResponse.rdfiRoutingNumber || '--NA--'],
      accountNumber: [this.payeeServerResponse.rdfiAccountNumber || '--NA--'],
      transactionCode: [this.payeeServerResponse.achTransactionCode || '--NA--'],
      splitPriority: [this.payeeServerResponse.splitPriority || '--NA--'],
      splitAmount: [this.payeeServerResponse.splitAmount || '--NA--'],
      splitPercent: [this.payeeServerResponse.splitPercent || '--NA--'],
      accountType: [this.payeeServerResponse.accountType || '--NA--'],
      printOptionsFlag: [this.payeeServerResponse.checkPrintOption || '--NA--'],
      payee1: [this.payeeServerResponse.payee1 || '--NA--'],
      payee2: [this.payeeServerResponse.payee2 || '--NA--'],
      payee3: [this.payeeServerResponse.payee3 || '--NA--'],
      payeeName: [this.payeeServerResponse.payeeName || '--NA--'],
      payeeAdd1: [this.payeeServerResponse.payeeAddress1 || '--NA--'],
      payeeAdd2: [this.payeeServerResponse.payeeAddress2 || '--NA--'],
      payeeCity: [this.payeeServerResponse.payeeCity || '--NA--'],
      payeeState: [this.payeeServerResponse.payeeState || '--NA--'],
      payeeZip: [this.payeeServerResponse.payeeZip || '--NA--'],
      payeeZip4: [this.payeeServerResponse.payeeZipPlusFour || '--NA--'],
      payeeCountry: [this.payeeServerResponse.payeeCountry || '--NA--']

    });
  }

  getPayeeInformationById() {
    this.payeeService.currentPayee.subscribe(
      (response) => {
        this.payeeServerResponse = response;
      }, (error: any) => {
        this.logger.error('Error Occurred while getting Payee information:', error.errorStat, error.errorStatusMsg)
      }
    );
  }

  onEdit(form: any) {
    this.activeRoute.queryParams.subscribe((params: Params) => {
      const id = params['id'];
      let navigationExtras: NavigationExtras = { queryParams: { 'id': id } };
      this.payeeService.setPayeeForEdit(this.payeeServerResponse);
      this.router.navigate(['home/addPayee', 'data'],  navigationExtras);         
    });
  }

  onCancel() {
    this.payeeService.reset();
    this.router.navigate(['home/searchPayee']);
  }

    /** get the values in states dropdown **/
    getStates() {
      this.commonDropdownService.getStatesDropdown().subscribe((response) => {
        this.payeeStates = response;
      },
        (error: any) => {
          this.logger.error('Error in loading states dropdown', error);
        });
    }

}
