import { Injectable } from '@angular/core';
import { PayeeTableModel } from '../models/payeeTabelModel';
import { PayeeTableRequestModel } from '../models/payeeTableRequest-Backend';

@Injectable()
export class PayeeTableParserService {

  constructor() { }
  private payeeTableRequestModel: PayeeTableRequestModel;
  convertUIModelToServerModel(request: PayeeTableModel): PayeeTableRequestModel {
    this.payeeTableRequestModel = new PayeeTableRequestModel();
    this.payeeTableRequestModel.payeeCode = request.payeeCode;
    this.payeeTableRequestModel.companyCode = request.companyCode;
    this.payeeTableRequestModel.sourceCode = request.sourceCode;
    this.payeeTableRequestModel.legacySourceCode = request.legacySourceCode;
    this.payeeTableRequestModel.maxAmount = request.maxAmount;
    this.payeeTableRequestModel.rdfiRoutingNumber = request.rdfiRoutingNumber;
    this.payeeTableRequestModel.rdfiAccountNumber = request.rdfiAccountNumber;
    this.payeeTableRequestModel.achTransactionCode = request.achTransactionCode;
    this.payeeTableRequestModel.splitPriority = request.splitPriority;
    this.payeeTableRequestModel.splitAmount = request.splitAmount;
    this.payeeTableRequestModel.splitPercent = request.splitPercent;
    this.payeeTableRequestModel.accountType = request.accountType;
    this.payeeTableRequestModel.checkPrintOption = request.checkPrintOption;
    this.payeeTableRequestModel.payee1 = request.payee1;
    this.payeeTableRequestModel.payee2 = request.payee2;
    this.payeeTableRequestModel.payee3 = request.payee3;
    this.payeeTableRequestModel.payeeName = request.payeeName;
    this.payeeTableRequestModel.payeeAddress1 = request.payeeAddress1;
    this.payeeTableRequestModel.payeeAddress2 = request.payeeAddress2;
    this.payeeTableRequestModel.payeeCity = request.payeeCity;
    this.payeeTableRequestModel.payeeState = request.payeeState;
    this.payeeTableRequestModel.payeeZip = request.payeeZip;
    this.payeeTableRequestModel.payeeZipPlusFour = request.payeeZipPlusFour;
    this.payeeTableRequestModel.payeeCountry = request.payeeCountry;
    
    return this.payeeTableRequestModel;
  }
}
