import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ContainerPayeeTableModel, PayeeTableModel } from '../models/payeeTabelModel';
import { Constants } from '../../../shared/constants/constants';
import { PayeeTableParserService } from './payee-table-parser.service';
import { PayeeTableRequestModel } from '../models/payeeTableRequest-Backend';
import { Response } from '@angular/http';
import { _catch } from 'rxjs/operator/catch';
import { BehaviorSubject } from 'rxjs';


const httpHeaderOptions = {
  headers: new HttpHeaders({
    // 'Content-Type': 'application/json',
    // 'Accept': 'application/json',
    'Cache-Control': 'no-store',
    'X-Requested-With': 'test'
  })
};

@Injectable()
export class PayeeTableService {

  urlPayees = 'payees';
  urlSearch = 'search';

  containerPayeeModel: ContainerPayeeTableModel = new ContainerPayeeTableModel();
  public payeeModelResponse = new BehaviorSubject<PayeeTableModel[]>([]);

  currentPayee = this.payeeModelResponse.asObservable();

  setPayeeForEdit(response: PayeeTableModel[]) {
    this.payeeModelResponse.next(response);
  }
  reset() {
    this.containerPayeeModel = new ContainerPayeeTableModel();
  }

  constructor(private httpClient: HttpClient,
    private payeeTableParser: PayeeTableParserService) { }

  getPayeeDetails(payeeId: string) {
    const uri = `${Constants.REST_MAINT_URL}/${this.urlPayees}/${payeeId}`;
    // const payeeTableModel: PayeeTableRequestModel = this.payeeTableParser.convertUIModelToServerModel(request);
    return this.httpClient.get(uri, httpHeaderOptions).catch(this.handleError);
  }

  create(request: PayeeTableModel): Observable<any> {
    const uri = `${Constants.REST_MAINT_URL}/${this.urlPayees}`;
    const payeeTableModel: PayeeTableRequestModel = this.payeeTableParser.convertUIModelToServerModel(request);
    return this.httpClient.post(uri, payeeTableModel, httpHeaderOptions)
      .map((resp: Response) => { }).catch(this.handleError);
  }

  update(payeeId: string, request: PayeeTableModel): Observable<any> {
    const uri = `${Constants.REST_MAINT_URL}/${this.urlPayees}/${payeeId}`;
    const payeeTableModel: PayeeTableRequestModel = this.payeeTableParser.convertUIModelToServerModel(request);
    return this.httpClient.put(uri, payeeTableModel, httpHeaderOptions).map((resp: Response) => { })._catch(this.handleError);
  }

  delete(payeeId: string): Observable<any> {
    const uri = `${Constants.REST_MAINT_URL}/${this.urlPayees}/${payeeId}`;
    return this.httpClient.delete(uri, httpHeaderOptions)
      .map((response: Response) => {
      })._catch(this.handleError);
  }

  searchPayee(request: PayeeTableModel): any {
    const uri = `${Constants.REST_MAINT_URL}/${this.urlSearch}/${this.urlPayees}`;
    const payeeTableModel: PayeeTableRequestModel = this.payeeTableParser.convertUIModelToServerModel(request);
    return this.httpClient.post<PayeeTableModel>(uri, payeeTableModel, httpHeaderOptions)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    const errorObj: any = {
      errorMessage: error.message,
      errorStat: error.status,
      errorText: error.statusText
    };
    return Observable.throw(errorObj);
  }
}
