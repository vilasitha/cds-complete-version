import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { FormGroup } from '@angular/forms/src/model';
import { PayeeTableService } from '../services/payee-table.service';
import { States } from '../../../shared/models/states';
import { CommondropdownService } from '../../../checktransactions/services/commondropdown.service';
import { ValidationService } from '../../../checktransactions/services/validation.service';
import { SharedService } from '../../../shared/services/shared.service';
import { CommonRestService } from '../../../checktransactions/services/common-rest-service.service';

@Component({
  selector: 'app-add-payee',
  templateUrl: './add-payee.component.html',
  styleUrls: ['./add-payee.component.css'],
  providers: [NGXLogger]
})
export class AddPayeeComponent implements OnInit {

  public addPayeeInfoForm: FormGroup;
  routeParam: string;
  urlId: string;
  statusFailMsg = '';
  payeeServerResponse: any;
  data_payeeCode;
  data_companyCode;
  data_sourceCode;
  data_legacySourceCode;
  data_maxAmount;
  data_rdfiRoutingNumber;
  data_rdfiAccountNumber;
  data_achTransactionCode;
  data_splitPriority;
  data_splitAmount;
  data_splitPercent;
  data_accountType;
  data_checkPrintOpt;
  data_payee1;
  data_payee2;
  data_payee3;
  data_payeeName;
  data_payeeAddress1;
  data_payeeAddress2;
  data_payeeCity;
  data_payeeState;
  data_payeeZip;
  data_payeeZipPlusFour;
  data_payeeCountry;
  payeeStates: States[];

  constructor(private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private logger: NGXLogger,
    private payeeTableService: PayeeTableService,
    private commonDropdownService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService,
    private commonRestService: CommonRestService,
  ) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => { this.urlId = params['id']; });
    this.statusFailMsg = '';
    this.getPayeeStates();
    if (this.urlId) {
      this.displayPayeeOnEdit();
    }
    this.buildForm(this.routeParam);
  }

  buildForm(data: string): void {
    this.addPayeeInfoForm = this.fb.group({
      payeeCode: [this.data_payeeCode || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeCode, [this.validationService.validatePayeeCode]],
      companyCode: [this.data_companyCode || this.payeeTableService.containerPayeeModel.payeeTableModel.companyCode, [this.validationService.companyCodeValidation]],
      sourceCode: [this.data_sourceCode || this.payeeTableService.containerPayeeModel.payeeTableModel.sourceCode, [this.validationService.validateSourceCode]],
      legacySourceCode: [this.data_legacySourceCode || this.payeeTableService.containerPayeeModel.payeeTableModel.legacySourceCode, [this.validationService.validatelegacyCode]],
      maxAmount: [this.data_maxAmount || this.payeeTableService.containerPayeeModel.payeeTableModel.maxAmount],
      routingNumber: [this.data_rdfiRoutingNumber || this.payeeTableService.containerPayeeModel.payeeTableModel.rdfiRoutingNumber],
      accountNumber: [this.data_rdfiAccountNumber || this.payeeTableService.containerPayeeModel.payeeTableModel.rdfiAccountNumber],
      transactionCode: [this.data_achTransactionCode || this.payeeTableService.containerPayeeModel.payeeTableModel.achTransactionCode],
      splitPriority: [this.data_splitPriority || this.payeeTableService.containerPayeeModel.payeeTableModel.splitPriority],
      splitAmount: [this.data_splitAmount || this.payeeTableService.containerPayeeModel.payeeTableModel.splitAmount],
      splitPercent: [this.data_splitPercent || this.payeeTableService.containerPayeeModel.payeeTableModel.splitPercent],
      accountType: [this.data_accountType || this.payeeTableService.containerPayeeModel.payeeTableModel.accountType],
      printOptionsFlag: [this.data_checkPrintOpt || this.payeeTableService.containerPayeeModel.payeeTableModel.checkPrintOption],
      payee1: [this.data_payee1 || this.payeeTableService.containerPayeeModel.payeeTableModel.payee1],
      payee2: [this.data_payee2 || this.payeeTableService.containerPayeeModel.payeeTableModel.payee2],
      payee3: [this.data_payee3 || this.payeeTableService.containerPayeeModel.payeeTableModel.payee3],
      payeeName: [this.data_payeeName || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeName],
      payeeAdd1: [this.data_payeeAddress1 || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeAddress1],
      payeeAdd2: [this.data_payeeAddress2 || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeAddress2],
      payeeCity: [this.data_payeeCity || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeCity],
      payeeState: [this.data_payeeState || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeState],
      payeeZip: [this.data_payeeZip || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeZip],
      payeeZip4: [this.data_payeeZipPlusFour || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeZipPlusFour],
      payeeCountry: [this.data_payeeCountry || this.payeeTableService.containerPayeeModel.payeeTableModel.payeeCountry],
    });
  }

  onCreate(form: FormGroup) {
    if( this.addPayeeInfoForm.get('routingNumber').value){
      this.validationService.validRoutingNumber(form);
    }
    if (form.valid) {
      this.payeeTableService.containerPayeeModel.payeeTableModel.legacySourceCode = this.addPayeeInfoForm.get('legacySourceCode').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeCode = this.addPayeeInfoForm.get('payeeCode').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.companyCode = this.addPayeeInfoForm.get('companyCode').value;
      let sourceCodeVal = this.addPayeeInfoForm.get('sourceCode').value;
      if(sourceCodeVal){
        this.payeeTableService.containerPayeeModel.payeeTableModel.sourceCode = sourceCodeVal.trim();
      }    
      this.payeeTableService.containerPayeeModel.payeeTableModel.maxAmount = this.addPayeeInfoForm.get('maxAmount').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.rdfiRoutingNumber = this.addPayeeInfoForm.get('routingNumber').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.rdfiAccountNumber = this.addPayeeInfoForm.get('accountNumber').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.achTransactionCode = this.addPayeeInfoForm.get('transactionCode').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.splitPriority = this.addPayeeInfoForm.get('splitPriority').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.splitAmount = this.addPayeeInfoForm.get('splitAmount').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.splitPercent = this.addPayeeInfoForm.get('splitPercent').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.accountType = this.addPayeeInfoForm.get('accountType').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.checkPrintOption = this.addPayeeInfoForm.get('printOptionsFlag').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payee1 = this.addPayeeInfoForm.get('payee1').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payee2 = this.addPayeeInfoForm.get('payee2').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payee3 = this.addPayeeInfoForm.get('payee3').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeName = this.addPayeeInfoForm.get('payeeName').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeAddress1 = this.addPayeeInfoForm.get('payeeAdd1').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeAddress2 = this.addPayeeInfoForm.get('payeeAdd2').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeCity = this.addPayeeInfoForm.get('payeeCity').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeState = this.addPayeeInfoForm.get('payeeState').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeZip = this.addPayeeInfoForm.get('payeeZip').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeZipPlusFour = this.addPayeeInfoForm.get('payeeZip4').value;
      this.payeeTableService.containerPayeeModel.payeeTableModel.payeeCountry = this.addPayeeInfoForm.get('payeeCountry').value;

      let companyCode = this.addPayeeInfoForm.get('companyCode').value
      let legacySourceCode = this.addPayeeInfoForm.get('legacySourceCode').value
      this.commonRestService.validateCompanyAndSouceCode(companyCode, legacySourceCode).subscribe(
        (result) => {
          if (this.urlId) {
            this.payeeTableService.update(this.urlId, this.payeeTableService.containerPayeeModel.payeeTableModel).subscribe(
              (response) => {
                this.logger.info('In update payee method', response);
                this.payeeTableService.reset();
                this.router.navigate(['home/searchPayee']);
              }, (error: any) => {
                this.logger.error('Error Occurred updaing maintenance Payee', error.errorStat,
                  'Error Message', error.errorMessage, 'Error Statu stext', error.statusText);
                if (error.errorStat == '404') {
                  this.statusFailMsg = '404 not found';
                } else if (error.errorStat == '403') {
                  this.statusFailMsg = 'You’re not authorized to perform that action.';
                } else if (error.errorStat == '500') {
                  this.statusFailMsg = 'Your request cannot be completed at this time.  Please try again';
                } else {
                  this.statusFailMsg = 'An error occurred while processing this request.';
                }
              }
            );

          } else {
            this.payeeTableService.create(this.payeeTableService.containerPayeeModel.payeeTableModel).subscribe(
              (response) => {
                this.logger.info('In payee create method', response);
                this.payeeTableService.reset();
                this.router.navigate(['home/searchPayee']);
              }, (error: any) => {
                this.logger.error('Error Occurred Creating new Payee', error.errorStat +
                  'Error Message', error.errorMessage + 'Error Statu stext', error.statusText);
                if (error.errorStat == '404') {
                  this.statusFailMsg = '404 not found';
                } else if (error.errorStat == '403') {
                  this.statusFailMsg = 'You’re not authorized to perform that action.';
                } else if (error.errorStat == '500') {
                  this.statusFailMsg = 'Your request cannot be completed at this time.  Please try again';
                } else {
                  this.statusFailMsg = 'An error occurred while processing this request.';
                }
              }
            );
          }

        }, (error: any) => {
          this.logger.info('Error Occurred in Payee Maintenance', error);
          if (error.errorStat == '404') {
            this.statusFailMsg = 'Company Code and Legacy Source Code combination is invalid';
          }

        }
      );
    } else {
      this.validationService.validateAllFormFields(form);
    }

  }

  displayPayeeOnEdit() {
    this.payeeTableService.currentPayee.subscribe(
      (response) => {
        this.payeeServerResponse = response;
        this.data_payeeCode = this.payeeServerResponse.payeeCode;
        this.data_companyCode = this.payeeServerResponse.companyCode;
        this.data_sourceCode = this.payeeServerResponse.sourceCode;
        this.data_legacySourceCode = this.payeeServerResponse.legacySourceCode;
        this.data_maxAmount = this.payeeServerResponse.maxAmount;
        this.data_rdfiRoutingNumber = this.payeeServerResponse.rdfiRoutingNumber;
        this.data_rdfiAccountNumber = this.payeeServerResponse.rdfiAccountNumber;
        this.data_achTransactionCode = this.payeeServerResponse.achTransactionCode;
        this.data_splitPriority = this.payeeServerResponse.splitPriority;
        this.data_splitAmount = this.payeeServerResponse.splitAmount;
        this.data_splitPercent = this.payeeServerResponse.splitPercent;
        this.data_accountType = this.payeeServerResponse.accountType;
        this.data_checkPrintOpt = this.payeeServerResponse.checkPrintOption;
        this.data_payee1 = this.payeeServerResponse.payee1;
        this.data_payee2 = this.payeeServerResponse.payee2;
        this.data_payee3 = this.payeeServerResponse.payee3;
        this.data_payeeName = this.payeeServerResponse.payeeName;
        this.data_payeeAddress1 = this.payeeServerResponse.payeeAddress1;
        this.data_payeeAddress2 = this.payeeServerResponse.payeeAddress2;
        this.data_payeeCity = this.payeeServerResponse.payeeCity;
        this.data_payeeState = this.payeeServerResponse.payeeState;
        this.data_payeeZip = this.payeeServerResponse.payeeZip;
        this.data_payeeZipPlusFour = this.payeeServerResponse.payeeZipPlusFour;
        this.data_payeeCountry = this.payeeServerResponse.payeeCountry;
      }, (error: any) => {
        this.logger.error('An error occurred while editing payee', error);
      }
    );
  }

  onCancel() {
    this.payeeTableService.reset();
    this.router.navigate(['home/searchPayee']);
  }

  /*
  validateOnChange() {
    let checkType = this.addPayeeInfoForm.get('checkOrEftRadioFlag').value
    if (checkType === 'Check') {
      this.enablePayeeAddress = true;
    } else {
      this.enablePayeeAddress = false;
    }
  } */



  /** get the values in states dropdown **/
  getPayeeStates() {
    this.commonDropdownService.getStatesDropdown().subscribe((response) => {
      this.payeeStates = response;
    },
      (error: any) => {
        this.logger.error('Error in loading Payee states dropdown', error);
      });
  }


}
