export class AddPayeeInfoModel{
    id: string;
    payeeCode: string;
    companyCode: string;
    sourceCode: string;
    legacySourceCode: string;
    maxAmount: string;
    rdfiRoutingNumber: string;
    rdfiAccountNumber: string;
    achTransactionCode: string;
    splitPriority: string;
    splitAmount: string;
    splitPercent: string;
    accountType: string='';
    checkPrintOption: string;
    payee1: string;
    payee2: string;
    payee3: string;
    payeeName: string;
    payeeAddress1: string;
    payeeAddress2: string;
    payeeCity: string;
    payeeState: string;
    payeeZip: string;
    payeeZipPlusFour: string;
    payeeCountry: string;
    
}