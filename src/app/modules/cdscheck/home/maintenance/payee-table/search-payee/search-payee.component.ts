import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PayeeTableModel } from '../models/payeeTabelModel';
import { Router, NavigationExtras } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { ValidationService } from '../../../checktransactions/services/validation.service';
import { PayeeTableService } from '../services/payee-table.service';
import { AddPayeeInfoModel } from '../add-payee/models/add-payee-info';
import { ConfirmDialogueComponent } from '../../../confirm-dialogue/confirm-dialogue.component';
import { DialogueService } from '../../../services/dialogue.service';

@Component({
  selector: 'app-payee-search',
  templateUrl: './search-payee.component.html',
  styleUrls: ['./search-payee.component.css'],
  providers: [NGXLogger]
})
export class SearchPayeeComponent implements OnInit {

  payeeSearchForm: FormGroup;
  errorStatusMsg: any;
  payeeServerResp: Array<PayeeTableModel>=[];
  viewPayeeInfo: AddPayeeInfoModel[]= [];
  enablePayeeTable: boolean = false;
  constructor(private fb: FormBuilder, private router: Router,
    private logger: NGXLogger,
    private validationService: ValidationService,
    private payeeService: PayeeTableService,
  private dialogueService:DialogueService) { }

  ngOnInit() {
    this.errorStatusMsg = '';
    this.enablePayeeTable = false;
    this.buildForm();
  }

  buildForm(): void {
    this.payeeSearchForm = this.fb.group({
      companyCode: [this.payeeService.containerPayeeModel.payeeTableModel.companyCode, [this.validationService.validateCompanyName]],
      payeeCode: [this.payeeService.containerPayeeModel.payeeTableModel.payeeCode, [this.validationService.validatePayeeCode]],
      legacySourceCode: [this.payeeService.containerPayeeModel.payeeTableModel.legacySourceCode, [this.validationService.validatelegacyCode]],
    });
  }


  onSearch(form: FormGroup) {
    if (form.valid) {
      this.payeeService.containerPayeeModel.payeeTableModel.legacySourceCode = this.payeeSearchForm.get('legacySourceCode').value;
      this.payeeService.containerPayeeModel.payeeTableModel.payeeCode = this.payeeSearchForm.get('payeeCode').value;
      this.payeeService.containerPayeeModel.payeeTableModel.companyCode = this.payeeSearchForm.get('companyCode').value;
      this.errorStatusMsg = '';
      this.payeeService.searchPayee(this.payeeService.containerPayeeModel.payeeTableModel).subscribe(
        (result) => {
          this.logger.info('On Serach of payee', result);
          this.payeeServerResp = result;
          if(this.payeeServerResp.length > 0 ){
              this.enablePayeeTable = true;
          } else {
            this.enablePayeeTable = false;  
            this.errorStatusMsg = 'No Records Found'; 
          }
       
        },
        (error: any) => {
          this.logger.error('Error Occurred while Searching for payee', error.errorStat ,
            'Error Message', error.errorMessage , 'Error Statu stext', error.statusText);
          if (error.errorStat == '404') {
            this.errorStatusMsg = '404 not found';
          } else if (error.errorStat == '403') {
            this.errorStatusMsg = 'You’re not authorized to perform that action.';
          } else if (error.errorStat == '500') {
            this.errorStatusMsg = 'Your request cannot be completed at this time.  Please try again';
          } else {
            this.errorStatusMsg = 'An error occurred while processing this request.';
          }
        });

    } else {
      this.validationService.validateAllFormFields(form);
    }
  }

  viewPayeeRecord(id: string) {
    let navigationExtras: NavigationExtras = { queryParams: { 'id': id } };

    this.payeeService.getPayeeDetails(id).subscribe(
      (response) => {
        this.viewPayeeInfo = response;
        this.payeeService.setPayeeForEdit(response);
        this.router.navigate(['home/viewPayee'], navigationExtras);
      },
      (error: any) => {
        this.logger.error('Error Occurred while viewing payee infomation', error.errorStat);
        if (error.errorStat == '404') {
          this.errorStatusMsg = '404 not found';
        } else if (error.errorStat == '403') {
          this.errorStatusMsg = 'You’re not authorized to perform that action.';
        } else if (error.errorStat == '500') {
          this.errorStatusMsg = 'Your request cannot be completed at this time.  Please try again';
        } else {
          this.errorStatusMsg = 'An error occurred while processing this request.';
        }

      }

    );
  }

  addPayee($event) {
    this.payeeService.reset();
    this.router.navigate(['home/addPayee', 'data']);
  }

  onCancel() {
    this.enablePayeeTable = false;
    this.payeeSearchForm.reset();
  }

  deleteRecord(id: string)  {
    const componentRef = this.dialogueService.openNewDialog(ConfirmDialogueComponent);
    if (componentRef && componentRef.instance) {
      componentRef.instance.setDetails({
        title: 'Confirm',
        message: 'Are you sure you want to delete this record (OK=yes Cancel=no)'
      });
      componentRef.instance.onClose.subscribe(
        isConfirmed => {
          this.dialogueService.closeComponent(componentRef);
          if (isConfirmed) {
            this.payeeService.delete(id).subscribe(
              data => {
                this.enablePayeeTable = false;
                this.payeeService.reset();
              },
              (error: any) => {
                this.logger.error('Error Occurred while deleting Payee Maintenance' , error.errorStat ,
                  'Error Message' , error.errorMessage , 'Error Status stext' , error.statusText);
                if (error.errorStat == '404') {
                  this.errorStatusMsg = '404 not found';
                } else if (error.errorStat == '403') {
                  this.errorStatusMsg = 'You’re not authorized to perform that action.';
                } else if (error.errorStat == '500') {
                  this.errorStatusMsg = 'Your request cannot be completed at this time.  Please try again';
                } else {
                  this.errorStatusMsg = 'An error occurred while processing this request.';
                }
              });
          } else {
            this.enablePayeeTable = true;
            this.router.navigate(['home/searchPayee']);
          }
        });
    }
  }

}
