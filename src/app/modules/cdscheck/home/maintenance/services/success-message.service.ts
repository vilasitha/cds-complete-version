import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SuccessMessageService {

  constructor() { }

  private saveSourceCodes = new BehaviorSubject<boolean>(false);
  private updateSourceCodes = new BehaviorSubject<boolean>(false);

  currentSourceSaveMsg = this.saveSourceCodes.asObservable();
  currentSourceUpdateMsg = this.updateSourceCodes.asObservable();

  setSaveForSourceCodes(message: boolean){
    this.saveSourceCodes.next(message);
  }

  setUpdateForSourceCodes(message: boolean){
    this.updateSourceCodes.next(message);
  }

}
