import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { ValidationService } from '../../../checktransactions/services/validation.service';
import { MaintSourceCodesService } from '../services/maint-source-codes.service';
import { MaintSourceCodesModel } from '../models/maintSourceCodesModel';

@Component({
  selector: 'app-search-sourcecode',
  templateUrl: './search-sourcecode.component.html',
  styleUrls: ['./search-sourcecode.component.css']
})
export class SearchSourcecodeComponent implements OnInit {

  searchSourceCodesForm: FormGroup;
  sourceServerResp: Array<MaintSourceCodesModel>[];
  viewSources: MaintSourceCodesModel[]=[];
  enableSources: boolean = false;
  errorStatusMsg: any;

  constructor(private fb: FormBuilder, private router: Router,
    private logger: NGXLogger, private validationService: ValidationService,
    private sourceCodeService: MaintSourceCodesService) { }

  ngOnInit() {
    this.enableSources;
    this.errorStatusMsg = '';
    this.buildForm();
  }

  buildForm(): void {
    this.searchSourceCodesForm = this.fb.group({
      sourceCode: [this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.sourceCode, [this.validationService.validateSourceCode]]
    });
  }

  onSearch(form: FormGroup) {
    this.errorStatusMsg = '';
    if (form.valid) {
      this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.sourceCode = this.searchSourceCodesForm.get('sourceCode').value;
      this.sourceCodeService.searchSources(this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel).subscribe(
        (result) => {
          this.sourceServerResp = result;
          if (this.sourceServerResp.length > 0) {
            this.enableSources = true;
          } else {
            this.enableSources = false;
            this.errorStatusMsg = 'EAS Source code not found';
          }
        },
        (error: any) => {
          this.throwErrorMsg(error);
        }
      );
    } else {
      this.validationService.validateAllFormFields(form);
    }
  }

  viewSourceRecord(id) {
    let navigationExtras: NavigationExtras = { queryParams: { 'id': id } };
    this.sourceCodeService.getSourceById(id).subscribe(
      (result) => {
        this.viewSources = result;
        this.sourceCodeService.setSources(this.viewSources);
        this.router.navigate(['home/maintenance/viewSources'], navigationExtras);
      },   
      (error: any) => {
        this.throwErrorMsg(error);
      }
    );
  }

  throwErrorMsg(error: any){
    this.logger.error('Error Occurred while viewing sources', error.errorStat);
    if (error.errorStat == '404') {
      this.errorStatusMsg = '404 not found';
    } else if (error.errorStat == '403') {
      this.errorStatusMsg = 'You’re not authorized to perform that action.';
    } else if (error.errorStat == '500') {
      this.errorStatusMsg = 'Your request cannot be completed at this time.  Please try again';
    } else {
      this.errorStatusMsg = 'An error occurred while processing this request.';
    }
  }

}
