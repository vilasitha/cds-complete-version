import { Injectable } from "@angular/core";
import { MaintSourceCodesModel } from "../models/maintSourceCodesModel";
import { SourceCodesRequestModel } from "../models/sourceCodesRequest";


@Injectable()
export class SourcesParserService {

    private sourcesRequestModel: SourceCodesRequestModel;
    convertUIModelToServer(request: MaintSourceCodesModel): SourceCodesRequestModel {
        this.sourcesRequestModel = new SourceCodesRequestModel();
        this.sourcesRequestModel.sourceCode= request.sourceCode;
        this.sourcesRequestModel.companyCode = request.companyCode;
        this.sourcesRequestModel.sourceSystemName = request.sourceSystemName;
        this.sourcesRequestModel.legacySourceCode = request.legacySourceCode;
        this.sourcesRequestModel.sourceDescription = request.sourceDescription;
        this.sourcesRequestModel.rmsPool = request.rmsPool;
        this.sourcesRequestModel.securityGroup = request.securityGroup
        return this.sourcesRequestModel;
    }

}