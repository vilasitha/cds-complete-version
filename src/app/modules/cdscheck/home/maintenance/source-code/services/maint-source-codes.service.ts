import { Injectable } from '@angular/core';
import { ContainerSourceCodesModel, MaintSourceCodesModel } from '../models/maintSourceCodesModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ErrorHandler } from '@angular/core/src/error_handler';
import { Constants } from '../../../shared/constants/constants';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SourceSytems } from '../models/sourceSystems';
import { SourcesParserService } from './maint-source-codes-parser.service';
import { SourceCodesRequestModel } from '../models/sourceCodesRequest';
const httpHeaderOptions = {
  headers: new HttpHeaders({
    'Cache-Control': 'no-store',
    'X-Requested-With': 'test'
  })
};

@Injectable()
export class MaintSourceCodesService{

  urlSources = 'sources'
  
  constructor(private httpClient: HttpClient, private sourcesParser: SourcesParserService) {
 }
  containerSourceCodesModel :ContainerSourceCodesModel = new ContainerSourceCodesModel();
  public sourcesResponse  = new BehaviorSubject<MaintSourceCodesModel[]>([]);
  
  currentSource = this.sourcesResponse.asObservable();

  setSources(response: MaintSourceCodesModel[]){
    this.sourcesResponse.next(response);
  }
  
  reset(){
    this.containerSourceCodesModel = new ContainerSourceCodesModel();
  }

  searchSources(request:MaintSourceCodesModel){
    const uri = `${Constants.REST_MAINT_URL}/${Constants.SEARCH}/${this.urlSources}`;
    return this.httpClient.post<MaintSourceCodesModel>(uri, request, httpHeaderOptions)
   .catch(this.handleError);
  }

  getSourceById(id){
    const uri = `${Constants.REST_MAINT_URL}/${this.urlSources}/${id}`;
    return this.httpClient.get(uri, httpHeaderOptions).catch(this.handleError)
  }

  create(request:MaintSourceCodesModel): Observable<any> {
    const uri = `${Constants.REST_MAINT_URL}/${this.urlSources}`;
    const sourceCodesModel: SourceCodesRequestModel = this.sourcesParser.convertUIModelToServer(request);
    return this.httpClient.post(uri, sourceCodesModel, httpHeaderOptions).map((resp:Response) => {}).catch(this.handleError);
  }

  update(id: string,request:MaintSourceCodesModel): Observable<any> {
    const uri = `${Constants.REST_MAINT_URL}/${this.urlSources}/${id}`;
    const sourceCodesModel: SourceCodesRequestModel = this.sourcesParser.convertUIModelToServer(request);    
    return this.httpClient.put(uri, sourceCodesModel, httpHeaderOptions).catch(this.handleError);  
  }
  
  private handleError(error: any) {
    const errorObj: any = {
      errorMessage: error.message,
      errorStat: error.status,
      errorText: error.statusText
    };
    return Observable.throw(errorObj);
  }
}
