import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MaintSourceCodesService } from '../services/maint-source-codes.service';
import { SourceSytems } from '../models/sourceSystems';
import { NGXLogger } from 'ngx-logger';
import { ValidationService } from '../../../checktransactions/services/validation.service';
import { CommonRestService } from '../../../checktransactions/services/common-rest-service.service';
import { SuccessMessageService } from '../../services/success-message.service';
import { CommondropdownService } from '../../../checktransactions/services/commondropdown.service';

@Component({
  selector: 'app-view-maint-sources',
  templateUrl: './view-maint-sources.component.html',
  styleUrls: ['./view-maint-sources.component.css'],
  providers: [NGXLogger]
})
export class ViewMaintSourcesComponent implements OnInit {

  viewMaintSourcesForm: FormGroup;
  sourceSysNames: SourceSytems[] = [];
  sourceServerResp: any;
  routeId: any;
  statusFailMsg = '';
  rmsPool:string;

  constructor(private fb: FormBuilder,
    private router: Router, private sourceCodeService: MaintSourceCodesService,
    private activeRoute: ActivatedRoute,
    private validationService: ValidationService,
    private commonRestService: CommonRestService,
    private sucessMsgService:SuccessMessageService,
    private commondropdownService: CommondropdownService,
    private logger: NGXLogger) { }

  ngOnInit() {
    this.statusFailMsg = '';
    this.rmsPool;
    this.getSourcesystemDD();
    this.activeRoute.queryParams.subscribe(params => { this.routeId = params['id']; });
    this.getSourcesInfoById();
    this.buildForm();
  }

  buildForm(): void {
    this.viewMaintSourcesForm = this.fb.group({
      companyCode: [this.sourceServerResp.companyCode || this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.companyCode],
      sourceSysNames: [this.sourceServerResp.sourceSystemName || this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.sourceSystemName,
      [this.validationService.validateAdminSys]],
      legacySourceCode: [this.sourceServerResp.legacySourceCode || this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.legacySourceCode, 
                        this.validationService.validatelegacyCode],
      sourceCode: [this.sourceServerResp.sourceCode || this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.sourceCode],
      sourceDescription: [this.sourceServerResp.sourceDescription || this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.sourceDescription],
      rmsPool: [this.rmsPool || this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.rmsPool],
      securityGroup: [this.sourceServerResp.securityGroup || this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.securityGroup,
      [this.validationService.validateSecurityGrp]],
      copyOrUpdate: ['Update'],
    });
  }

  onSubmit(form: FormGroup) {
    this.statusFailMsg = '';
    if (form.valid) {
      this.formValues();
      const copyOrUpdateVal = this.viewMaintSourcesForm.get('copyOrUpdate').value;
      if (copyOrUpdateVal == 'Update') {
        this.sucessMsgService.setUpdateForSourceCodes(true);
        this.sucessMsgService.setSaveForSourceCodes(false);
        this.updateSources();
       
      } else {
        this.sucessMsgService.setUpdateForSourceCodes(false);
        this.sucessMsgService.setSaveForSourceCodes(true);
        this.createSources();
        
      }
    } else {
      this.validationService.validateAllFormFields(form);
    }
  }

  onCancel() {
    this.sourceCodeService.reset();
    this.router.navigate(['home/maintenance/sourceCodes']);
  }

  formValues() {
    this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.companyCode = this.viewMaintSourcesForm.get('companyCode').value;
    this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.sourceCode = this.viewMaintSourcesForm.get('sourceCode').value;
    const legacySource = this.viewMaintSourcesForm.get('legacySourceCode').value;
    if(legacySource && /\s/.test(legacySource)){
      this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.legacySourceCode = legacySource.trim();   
    }    
    this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.sourceSystemName = this.viewMaintSourcesForm.get('sourceSysNames').value;
    this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.securityGroup = this.viewMaintSourcesForm.get('securityGroup').value;   
    this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.rmsPool = this.viewMaintSourcesForm.get('rmsPool').value;
    this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.sourceDescription = this.viewMaintSourcesForm.get('sourceDescription').value;
 
  }

  updateSources() {
    this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.id = this.routeId;
    this.sourceCodeService.update(this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel.id, this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel).subscribe(
      () => {
        this.logger.info('Update source code information successfully!');
        this.sourceCodeService.reset();
        this.router.navigate(['home/maintenance/success']);
      }, (error: any) => {
        this.throwErrorMgs(error);
      }
    );
  }

  createSources() {
    this.sourceCodeService.create(this.sourceCodeService.containerSourceCodesModel.maintSourceCodesModel).subscribe(
      () => {
        this.logger.info('Inserted source code information successfully!')
        this.sourceCodeService.reset();
        this.router.navigate(['home/maintenance/success']);
      }, (error: any) => {
        this.throwErrorMgs(error);
      }
    );
  }

  /** below method is to validation security group */
  validateSecurityGrp(secGrpVal) {
    let securityGrp = this.viewMaintSourcesForm.get('securityGroup');
    if (secGrpVal) {
      let errMsg = '';
      this.commonRestService.validateSecurityGrp(secGrpVal).subscribe(() => {
        this.logger.info('Security Group is Valid');
      }, (err: any) => {
        if (err.errorStat == '404') {
          errMsg = 'invalidSecurityGroup';
        } else{
          this.logger.info('Security Group Validation Failed', err.errorStat);
        }
        securityGrp.markAsTouched({ onlySelf: true });
        securityGrp.setErrors({
          'invalidSecurityGroup': errMsg == 'invalidSecurityGroup' ? true : false         
        })
      });
    }
  }

  getSourcesInfoById() {
    this.sourceCodeService.currentSource.subscribe((response) => {
      this.sourceServerResp = response;
      if(this.sourceServerResp.rmsPool !== 'Y'){
        this.rmsPool = ' ';
      }else{
        this.rmsPool = 'Y';
      }
    },
      (error: any) => {
        this.logger.error('Error Occurred while viewing Source Codes', error.errorStat);
      });
  }

  getSourcesystemDD() {
    this.commondropdownService.getSourceSystems().subscribe(
      (resp) => {
        this.sourceSysNames = resp;
      },
      (error: any) => {
        this.logger.error('Error Occurred while loading Admin Systems dropdown', error.errorStat);
      });
  }

  throwErrorMgs(error: any) {
    this.logger.error('Error Occurred in Source codes', error.errorStat,
      'Error Message', error.errorMessage, 'Error Statu stext', error.statusText);
    if (error.errorStat == '404') {
      this.statusFailMsg = '404 not found';
    } else if (error.errorStat == '403') {
      this.statusFailMsg = 'You’re not authorized to perform that action.';
    } else if (error.errorStat == '500') {
      this.statusFailMsg = 'Your request cannot be completed at this time.  Please try again';
    } else {
      this.statusFailMsg = 'An error occurred while processing this request.';
    }
  }

}
