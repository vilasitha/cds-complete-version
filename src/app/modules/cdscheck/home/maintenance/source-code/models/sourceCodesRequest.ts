import { SourceSytems } from "./sourceSystems";

export class SourceCodesRequestModel {

    sourceCode: string;
    sourceSytems: SourceSytems;
    id: string;
    companyCode: string;
    sourceSystemName: string;
    legacySourceCode: string;
    sourceDescription: string;
    rmsPool: string;
    securityGroup: string;
}