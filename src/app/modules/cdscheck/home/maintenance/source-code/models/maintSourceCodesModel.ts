import { SourceSytems } from "./sourceSystems";

export class ContainerSourceCodesModel{

    maintSourceCodesModel :MaintSourceCodesModel;

    constructor() {
        this.maintSourceCodesModel = new MaintSourceCodesModel();
    }

    reset() {
        this.maintSourceCodesModel = new MaintSourceCodesModel();
    }

}


export class MaintSourceCodesModel
{
    sourceSysNames: SourceSytems;
    sourceCode: string; 
    sourceSystemName: string; 
    id: string;
    companyCode: string;
    legacySourceCode: string;
    sourceDescription: string;
    rmsPool: string;
    securityGroup: string;
}