import { Component, OnInit } from '@angular/core';
import { SuccessMessageService } from '../services/success-message.service';
import { FormGroup } from '@angular/forms/src/model';

@Component({
  selector: 'app-success-message',
  templateUrl: './success-message.component.html',
})
export class SuccessMessageComponent implements OnInit {

  successSubmitForm: FormGroup;

  constructor(private successMsgService:SuccessMessageService) { }
  saveSuccessMsg: boolean;
  updateSUccessMsg: boolean;

  ngOnInit() {
  
  this.successMsgService.currentSourceSaveMsg.subscribe((response) => { this.saveSuccessMsg = response});
  this.successMsgService.currentSourceUpdateMsg.subscribe((response)  => { this.updateSUccessMsg = response});
  }


}
