import { Directive, HostListener } from "@angular/core";


@Directive({
    selector: '[appUppercase]'
})
export class AppUpperCaseDirective{
    

    constructor() {}
    
      @HostListener('input', ['$event']) onKeyUp(event) {
        event.target['value'] = event.target['value'].toUpperCase();
      }
}