import { Directive, Renderer, ElementRef, forwardRef, Renderer2 } from '@angular/core';
import { NG_VALUE_ACCESSOR, DefaultValueAccessor } from '@angular/forms';

const UPCASE_INPUT_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputFieldUpperCaseDirective),
  multi: true,
};

@Directive({
  selector: 'input[appInputUpperCase]',
  host: {
    '(input)': 'onInput($event.target.value)',
    '(blur)': 'onTouched()',
  },
  providers: [
    UPCASE_INPUT_CONTROL_VALUE_ACCESSOR,
  ],
})

export class InputFieldUpperCaseDirective extends DefaultValueAccessor{
    constructor(renderer: Renderer2, elementRef: ElementRef) {
        super(renderer, elementRef, false);
      }
    
      writeValue(value: any): void {
        const transformed = this.transformValue(value);   
        super.writeValue(transformed);
      }
    
      onInput(value: any): void {
        const transformed = this.transformValue(value); 
        super.writeValue(transformed);
        this.onChange(transformed);
      }
    
      private transformValue(value: any): any {
        const result = value && typeof value === 'string'
          ? value.toLocaleUpperCase()
          : value;
    
        return result;
      }

}