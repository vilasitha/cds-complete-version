import { Component, OnInit, Input, HostListener, EventEmitter, OnDestroy } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs/Rx';
import { HomeService } from '../services/home.service';

import { AppComponent } from '../../../../app.component';
import { TransactionsResponse } from './models/all-trasactions';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { SharedService } from '../shared/services/shared.service';

import { Sort } from '@angular/material/sort';
import { Output } from '@angular/core';
import { ReplaceOrVoidChecksService } from '../checktransactions/replace-or-void-checks/replace-or-void-checks.service';
import { DisbursementService } from '../checktransactions/disbursement/disbursement.service';
import { PayeeTableService } from '../maintenance/payee-table/services/payee-table.service';

@Component({
  selector: 'app-your-transactions',
  templateUrl: './your-transactions.component.html',
  styleUrls: ['./your-transactions.component.css'],
  providers: [NGXLogger]
})
export class YourTransactionsComponent implements OnInit {

  transactionsData: TransactionsResponse[] = [];
  sortedData;
  maintTab: boolean;
  constructor(private logger: NGXLogger,
    private homeService: HomeService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private sharedService: SharedService,
    private replaceOrVoidChecksService: ReplaceOrVoidChecksService,
    private disbursementService: DisbursementService,
    private payeeMaintService: PayeeTableService
  ) {
    this.sortedData = this.transactionsData.slice();
  }
  
  ngOnInit() {
    if (this.router.url == '/home/replaceChecks/clientId?type=Replace' || this.router.url == '/home/voidChecks/clientId?type=Void') {
      if (this.replaceOrVoidChecksService || this.replaceOrVoidChecksService.containerChecksModel) {
        this.replaceOrVoidChecksService.reset();
      }
    }
    if (this.router.url === '/home/requestDisbursement/payee') {
      if (this.disbursementService || this.disbursementService.reqtDisbursementDataModel) {
        this.disbursementService.reqtDisbursementDataModel.reset();
      }
    }
    this.payeeMaintService.reset();
    this.homeService.maintenanceVarMsg.subscribe((result) => { this.maintTab = result });
    this.loadYourTransaction();
  }


  /**
   * Method to get all transaction types
   */
  loadYourTransaction() {
    this.homeService.getCheckTransactions().subscribe(
      (result) => {
        this.transactionsData = result;
        this.sortedData = this.transactionsData.slice();
      }, (error: any) => {
        this.logger.error("Error in load your transactions method", error);
      }
    );
  }

  /** Method to view transaction by client Id */
  viewTransactions(clientId: string) {
    let navigationExtras: NavigationExtras = {
      queryParams: { "clientId": clientId }
    };
    for (let checks of this.transactionsData) {
      if (checks.transactionType != null || checks.transactionType != '') {
        if (clientId === checks.clientId && checks.transactionType === 'Replacement') {
          this.homeService.getTransactionById(clientId).subscribe(
            (response) => {
              this.transactionsData = response;
              this.sharedService.setTransactionsToDisplay(this.transactionsData);
              this.router.navigate(['home/viewChecks'], navigationExtras);
            },
            (error: any) => {
              this.logger.error('Error Occurred while calling view replacement checks', error.errorStat);
            }
          );

        }
        else if (clientId === checks.clientId && checks.transactionType === 'Void') {
          this.homeService.getTransactionById(clientId).subscribe(
            (response) => {
              this.transactionsData = response;
              this.sharedService.setTransactionsToDisplay(this.transactionsData);
              this.router.navigate(['home/viewChecks'], navigationExtras);
            },
            (error: any) => {
              this.logger.error('Error Occurred while calling view void checks', error.errorStat);
            }
          );

        }
        else if (clientId === checks.clientId && checks.transactionType === 'Disbursement') {
          this.homeService.getTransactionById(clientId).subscribe(
            (response) => {
              this.transactionsData = response;
              this.sharedService.setTransactionsToDisplay(this.transactionsData);
              this.router.navigate(['home/viewDisbursement'], navigationExtras);
            },
            (error: any) => {
              this.logger.error('Error Occurred while calling viewDisbursement', error.errorStat);
            }
          );
        }

      }

    }
  }  //end edit method

  /** sort data table */
  sortData(sort: Sort) {
    let isAsc = sort.direction == 'asc';
    const data = this.transactionsData.slice();
    if (!sort.active || sort.direction == '') {
      this.sortedData = data;
      return;
    }
    this.sortArrOfObjectsByParam(data, sort.active, isAsc);
    this.sortedData = data;
  }


  sortArrOfObjectsByParam(arrToSort, strObjParamToSortBy, sortAscending) {

    if (sortAscending == undefined) sortAscending = true;  // default to true
    arrToSort.sort(function (a, b) {
      if (a[strObjParamToSortBy] === null) {
        return 1;
      }
      else if (b[strObjParamToSortBy] === null) {
        return -1;
      }
      else if (a[strObjParamToSortBy] === b[strObjParamToSortBy]) {
        return 0;
      }
      else if (sortAscending) {
        return a[strObjParamToSortBy] < b[strObjParamToSortBy] ? -1 : 1;
      }
      else if (!sortAscending) {
        return a[strObjParamToSortBy] < b[strObjParamToSortBy] ? 1 : -1;
      }

    });
  }
}


function compare(item1, item2, isAsc) {
  return (item1 < item2 ? -1 : 1) * (isAsc ? 1 : -1);
}

