import { AccountDetails } from './accountDetailsModel';

export class TransactionsResponse {  
    status: string;
    payeeName: string;
    lastModified: string;
    stateCode: string;
    checkDate: string;
    transactionType: string;
    companyCode: string;
    sourceCode: string;
    phoneNumber: string;
    vendorCode: string;
    payeeAddress1: string;
    payeeAddress2: string;
    payeeAddress3: string;
    payeeAddress4: string;
    payeeCity: string;
    payeeState: string;
    payeeZip: string;
    payeeZipPlusFour: string;
    payeeCountry: string;
    payeeAddressIndicator: string;
    taxId: string;
    bankRouting: string;
    bankAccount: string;
    paymentType: string;
    mailCode: string;
    descriptionCode: string;
    description1: string;
    description2: string;
    originalCheckNumber: string;
    originalCheckFlag: string;
    reason: string;
    checkAmount: string;
    voidAccount: string;
    accountingDetails: Array<AccountDetails> = [];
    clientId: string;
    ssn: string;
    alpha:  string;
    accountTypes: string;


}