
import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatNativeDateModule, MatTooltipModule } from '@angular/material';
import { MatSortModule } from '@angular/material/sort';
import { RouterModule } from '@angular/router';
import { BreadcrumbModule } from 'angular2-crumbs';
import { homeRoutes } from './home.router';
import { Http, HttpModule } from '@angular/http';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorHandler } from '@angular/common/http/src/interceptor';
import { CurrencyMaskModule } from 'ngx-currency-mask';
// import { CookieService } from 'ngx-cookie-service';
import { Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
// Services
import { HomeService } from './services/home.service';
import { ValidationService } from '../home/checktransactions/services/validation.service';
import { CommondropdownService } from '../home/checktransactions/services/commondropdown.service';
import { UserRolesService } from '../home/shared/services/user-roles.service';
import { DevdataService } from '../../../components/devdata/devdata.service';
// Components
import { YourTransactionsComponent } from '../home/your-transactions/your-transactions.component';
import { ControlMessagesComponent } from '../home/checktransactions/error-messages/error-messages-component';
import { DisbursementComponent } from '../home/checktransactions/disbursement/disbursement.component';
import { ReplaceOrVoidChecksComponent } from '../home/checktransactions/replace-or-void-checks/replace-or-void-checks.component';
import { ViewReplaceOrVoidChecksComponent } from './checktransactions/view-replace-or-void-checks/view-replace-or-void-checks.component';
import { VerifyCheckInfoComponent } from '../home/checktransactions/verify-check-info/verify-check-info.component';
import { ConfirmchecksComponent } from '../home/checktransactions/confirmchecks/confirmchecks.component';
import { PayeeInfoComponent } from '../home/checktransactions/disbursement/payee-info/payee-info.component';
import { AccountingDetailsComponent } from '../home/checktransactions/disbursement/payee-info/accounting-details/accounting-details.component';

import { ConfirmationService } from './checktransactions/confirmchecks/confirmation.service';
import { DisbursementService } from './checktransactions/disbursement/disbursement.service';
import { CommonRestService } from '../home/checktransactions/services/common-rest-service.service';
import { ViewDisbursementComponent } from './checktransactions/view-disbursement/view-disbursement.component';
import { ReplaceOrVoidChecksService } from './checktransactions/replace-or-void-checks/replace-or-void-checks.service';
import { MessageService } from './checktransactions/error-messages/services/messages.service';
import { HttpErrorHandler } from './checktransactions/error-messages/services/http-error-handler.service';
import { DisbursementParserService } from './checktransactions/disbursement/services/disbursement-parser.service';
import { PayeeTableService } from '../home/maintenance/payee-table/services/payee-table.service';

import { ChecksParserService } from './checktransactions/replace-or-void-checks/services/replace-or-void-checks-parser.service';
import { VerifyVoidCheckComponent } from './checktransactions/verify-void-check/verify-void-check.component';
import { PayeeTableParserService } from './maintenance/payee-table/services/payee-table-parser.service';
import { SearchPayeeComponent } from './maintenance/payee-table/search-payee/search-payee.component';
import { AddPayeeComponent } from './maintenance/payee-table/add-payee/add-payee.component';
import { ViewPayeeComponent } from './maintenance/payee-table/view-payee/view-payee.component';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ngx-currency-mask/src/currency-mask.config';
import { AppUpperCaseDirective } from './directives/uppercase.directive';
import { InputFieldUpperCaseDirective } from './directives/fielduppercase.directive';
import { SearchSourcecodeComponent } from './maintenance/source-code/search-sourcecode/search-sourcecode.component';
import { MaintSourceCodesService } from './maintenance/source-code/services/maint-source-codes.service';
import { ViewMaintSourcesComponent } from './maintenance/source-code/view-maint-sources/view-maint-sources.component';
import { SourcesParserService } from './maintenance/source-code/services/maint-source-codes-parser.service';
import { SuccessMessageComponent } from './maintenance/success-message/success-message.component';
import { SuccessMessageService } from './maintenance/services/success-message.service';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'left',
  allowNegative: true,
  allowZero: true,
  decimal: '.',
  precision: 2,
  prefix: '$',
  suffix: '',
  thousands: ','
};

@NgModule({
  declarations: [
    YourTransactionsComponent,
    ReplaceOrVoidChecksComponent,
    DisbursementComponent,
    ViewReplaceOrVoidChecksComponent,
    ControlMessagesComponent,
    VerifyCheckInfoComponent,
    ConfirmchecksComponent,
    PayeeInfoComponent,
    AccountingDetailsComponent,
    ViewDisbursementComponent,
    VerifyVoidCheckComponent,
    SearchPayeeComponent,
    AddPayeeComponent,
    ViewPayeeComponent,
    OnlyNumberDirective,
    AppUpperCaseDirective,
    InputFieldUpperCaseDirective,
    SearchSourcecodeComponent,
    ViewMaintSourcesComponent,
    SuccessMessageComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSortModule,
    MatTooltipModule,    
    CdkTableModule,
    MatNativeDateModule,
    CurrencyMaskModule,
    RouterModule.forChild(homeRoutes)],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [HomeService,
    CommondropdownService,
    ValidationService,
    DisbursementService,
    UserRolesService, DevdataService, ConfirmationService,
    CommonRestService, ReplaceOrVoidChecksService,
    MessageService, HttpErrorHandler,
    DisbursementParserService,
    ChecksParserService,
    PayeeTableService,
    PayeeTableParserService,
    MaintSourceCodesService,
    SourcesParserService,
    SuccessMessageService,
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
  ],
  exports: [ RouterModule, CommonModule],
})
export class HomeModule {}
