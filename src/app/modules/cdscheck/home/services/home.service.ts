import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NGXLogger } from 'ngx-logger';
import { environment } from '../../../../../environments/environment';
import { TransactionsResponse } from '../your-transactions/models/all-trasactions';
import { Constants } from '../shared/constants/constants';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { NgxLoggerLevel } from 'ngx-logger';
import { LoginUser } from '../shared/models/login-user';

const httpHeaderOptions = {
  headers: new HttpHeaders({
    'Cache-Control': 'no-store',
    'X-Requested-With': 'application'
  })
};

@Injectable()
export class HomeService {
  private serverError: NgxLoggerLevel;
  private requestVar = new BehaviorSubject<boolean>(false);
  private voidVar = new BehaviorSubject<boolean>(false);
  private replaceVar = new BehaviorSubject<boolean>(false);
  private maintenanceVar = new BehaviorSubject<boolean>(false);
  private yourTransVar = new BehaviorSubject<boolean>(false);
  private loginUserId = new BehaviorSubject<string>('');
  private sourcesVar = new BehaviorSubject<boolean>(false);

  currentUser = this.loginUserId.asObservable();
  replaceVarMsg = this.replaceVar.asObservable();
  voidVarMsg = this.voidVar.asObservable();
  requestVarMsg = this.requestVar.asObservable();
  maintenanceVarMsg = this.maintenanceVar.asObservable();
  yourTransVarMsg = this.yourTransVar.asObservable();
  sourcesVarMsg = this.sourcesVar.asObservable();

  constructor(private httpClient: HttpClient) { }


  /**
   * display your transactions table
   */
  getCheckTransactions(): any {
      return this.httpClient.get<TransactionsResponse>(Constants.REST_REQ_API_URL)
      ._catch(this.handleError);    
  }

  /** View Transaction record by Id */
  getTransactionById(clientId: string): any {
    const url = `${Constants.REST_REQ_API_URL}/${clientId}`;
      return this.httpClient.get<TransactionsResponse>(url
        , httpHeaderOptions)._catch(this.handleError); 
  }

  changeRequestValues(request: boolean) {
    this.requestVar.next(request);
  }

  displayVoidValues(voidCheck: boolean) {
    this.voidVar.next(voidCheck);
  }

  displayReplaceValues(replace: boolean) {
    this.replaceVar.next(replace);
  }
  setUser(userId: string) {
    this.loginUserId.next(userId);
  }

  displayMaintanceTab(maintTab: boolean) {
    this.maintenanceVar.next(maintTab);
  }
  displayYourTranstab(yourTransTab: boolean) {
    this.yourTransVar.next(yourTransTab);
  }
  displaySourceCodes(sourcesTab : boolean){
    this.sourcesVar.next(sourcesTab);
  }

  private handleError(error: any) {
    let errorObj: any = {
      errorMessage: error.message,
      errorStat: error.status,
      errorText: error.statusText
    };
    return Observable.throw(errorObj);
  }

}
