import { ComponentFactoryResolver, ViewContainerRef, Component, Injectable, ComponentRef, ApplicationRef, Injector } from '@angular/core';
/**
 * used as a service to open dialog on app Root level or compoent level
 */
@Injectable()
export class DialogueService {

  public vcf: any;
  private loadedComponentRef: ComponentRef<any>;
  private appRef: ApplicationRef;
  constructor(private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector) {

  }
  /**
   * component is loaded in app root level if its empty
   * confirmation dialogs are loaded here
   * @param component component to be loaded
   */
  openNewDialog(component: any) {
    if (this.vcf && this.vcf.length === 0) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
      this.vcf.clear();
      this.loadedComponentRef = this.vcf.createComponent(componentFactory);
      return this.loadedComponentRef;
    }
  }
  /**
   * loads the component on the provided viewContainerRef if its empty
   * Functional components are loaded here
   * @param vcf viewContainerRef 
   * @param component component to be loaded
   */
  loadComponent(vcf, component: any) {
    if (vcf && vcf.length === 0) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
      vcf.clear();
      this.loadedComponentRef = vcf.createComponent(componentFactory);
      return this.loadedComponentRef;
    }
  }
  /**
   * closes the component loaded from component level
   * and manually tiggers change detection
   * @param componentRef componetRef to be destroyed
   */
  closeComponent(componentRef: ComponentRef<any>) {
    componentRef.destroy();
    if (this.vcf) {
      this.vcf.clear();
    }
    const appref: ApplicationRef = this.injector.get(ApplicationRef);
    setTimeout(() => { appref.tick(); }, 200);
  }
  /**
   * closes the component loaded in app root level
   * and manually tiggers change detection
   */
  closeExistingComponentRefs() {
    if (this.loadedComponentRef) {
      this.loadedComponentRef.destroy();
      this.loadedComponentRef = undefined;
      const appref: ApplicationRef = this.injector.get(ApplicationRef);
      setTimeout(() => { appref.tick(); }, 200);
    }
  }
}
