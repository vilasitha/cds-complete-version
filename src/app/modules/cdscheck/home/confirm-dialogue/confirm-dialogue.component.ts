import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-confirm-dialogue',
  templateUrl: './confirm-dialogue.component.html',
  styleUrls: ['./confirm-dialogue.component.css']
})
export class ConfirmDialogueComponent implements OnInit {
  title: string;
  message: string;
  onClose: EventEmitter<any>;
  constructor() {
    this.onClose= new EventEmitter();
   }

  ngOnInit() {
  }
  /**
   * Method to confirm the dialog
   */
  confirm() {
    this.onClose.next(true);
  }

  /**
   * Method to set Confirm component
   * @param obj confirm component object with title and message
   */
  setDetails(obj: any) {
    this.title = obj.title;
    this.message = obj.message;
  }

  /**
   * Method to close confirm component
   */
  close() {
    this.onClose.next(false);
  }
}
