import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { URLSearchParams, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CookieService } from 'ngx-cookie-service';
import { HomeService } from './modules/cdscheck/home/services/home.service';
import { NGXLogger } from 'ngx-logger';
import { Constants } from './modules/cdscheck/home/shared/constants/constants';
import { error } from 'selenium-webdriver';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { DialogueService } from './modules/cdscheck/home/services/dialogue.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NGXLogger]
})
export class AppComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    this.dialogueService.vcf = this.target;
  }
  @ViewChild('target', {
    read: ViewContainerRef
  }) target;
  userCapabilities;
  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService,
    private router: Router,
    private homeService: HomeService,
    private logger: NGXLogger,
    private dialogueService: DialogueService
  ) { }

  ngOnInit() {
    this.httpClient.get(Constants.REST_API_URL + '/user').subscribe(results => {
      this.userCapabilities = results;
      this.homeService.setUser(this.userCapabilities.displayName);
      this.logger.info('<<==AppComponent List of User==>', this.userCapabilities);
      if (this.userCapabilities.capabilities == 'CSC_CHECKREQ') {
        this.logger.info('<<==AppComponent CSC Checkreq User==>', this.userCapabilities.capabilities);
        this.homeService.changeRequestValues(true);
        this.homeService.displayReplaceValues(false);
        this.homeService.displayVoidValues(false);
        this.homeService.displayMaintanceTab(false);
        this.homeService.displayYourTranstab(true);
        this.homeService.displaySourceCodes(false);
        this.router.navigate(['/home/yourTransactions']);
      } else if (this.userCapabilities.capabilities == 'CSC_CHECKMAINT') {
        this.logger.info('<<==AppComponent CSC Maint User==>', this.userCapabilities.capabilities);
        this.homeService.changeRequestValues(false);
        this.homeService.displayReplaceValues(true);
        this.homeService.displayVoidValues(true);
        this.homeService.displayMaintanceTab(false);
        this.homeService.displayYourTranstab(true);
        this.homeService.displaySourceCodes(false);
        this.router.navigate(['/home/yourTransactions']);
      } else if (this.userCapabilities.capabilities == 'CSC_MAINT') {
        this.logger.info('<<==AppComponent CSC Maint User==>', this.userCapabilities.capabilities);
        this.homeService.changeRequestValues(false);
        this.homeService.displayReplaceValues(false);
        this.homeService.displayVoidValues(false);
        this.homeService.displayYourTranstab(false);
        this.homeService.displayMaintanceTab(true);
        this.homeService.displaySourceCodes(false);
        this.router.navigate(['/home/searchPayee']);
      } else if (this.userCapabilities.capabilities == 'CSC_SOURCEMAINT') {
        this.logger.info('<<==AppComponent CSC Source Codes User==>', this.userCapabilities.capabilities);
        this.homeService.changeRequestValues(false);
        this.homeService.displayReplaceValues(false);
        this.homeService.displayVoidValues(false);
        this.homeService.displayYourTranstab(false);
        this.homeService.displayMaintanceTab(false);
        this.homeService.displaySourceCodes(true);
        this.router.navigate(['/home/maintenance/sourceCodes']);
      }  
      for (let i of this.userCapabilities.capabilities) {
        if (this.userCapabilities.capabilities.length > 1) {
          this.homeService.changeRequestValues(true);
          this.homeService.displayReplaceValues(true);
          this.homeService.displayVoidValues(true);
          this.homeService.displayMaintanceTab(true);
          this.homeService.displayYourTranstab(true);
          this.router.navigate(['/home/yourTransactions']);
        }
      }
      

    }, (error: any) => {
      this.logger.error('Error Occurred while loading users (AppComponent) =>', error);
    });
  }

  public handleInactivityCallback() {
    window.location.href = '/servlet/AppSwitchServlet?OAE_APP_NAME=Logout';
  }
}

