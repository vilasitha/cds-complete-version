
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { RouterModule, Routes } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { appRoutes } from './app.router';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { NgxInactivity } from 'ngx-inactivity';
import { DataTableModule } from 'angular2-datatable';

// project related components
import { AppComponent } from './app.component';
import { HomeModule } from './modules/cdscheck/home/home.module';
import { DevdataComponent } from './components/devdata/devdata.component';
import { HomeComponent } from './modules/cdscheck/home/home.component';
import { HeaderComponent } from '../app/components/header/header.component';
import { FooterComponent } from '../app/components/footer/footer.component';
import { SidemenuComponent } from '../app/components/sidemenu/sidemenu.component';
import { PageNotFoundComponent } from '../app/modules/cdscheck/home/page-not-found/page-not-found/page-not-found.component';
import { ConfirmDialogueComponent } from './modules/cdscheck/home/confirm-dialogue/confirm-dialogue.component';

// project related services
import { AppService } from './app.service';
import { HomeService } from './modules/cdscheck/home/services/home.service';
import { ValidationService } from './modules/cdscheck/home/checktransactions/services/validation.service';
import { CommondropdownService } from './modules/cdscheck/home/checktransactions/services/commondropdown.service';
import { UserRolesService } from './modules/cdscheck/home/shared/services/user-roles.service';
import { DevdataService } from './components/devdata/devdata.service';
import { SharedService } from './modules/cdscheck/home/shared/services/shared.service';
import { DialogueService } from './modules/cdscheck/home/services/dialogue.service';
import { UserIdleModule } from 'angular-user-idle';

@NgModule({
  declarations: [
    AppComponent,
    DevdataComponent,
    PageNotFoundComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    SidemenuComponent,
    ConfirmDialogueComponent,  
  ],
  imports: [
    BrowserModule,
    NgxInactivity,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes, {  useHash: true }),
    HttpClientModule,
    LoggerModule.forRoot({
      level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR
    }),
    UserIdleModule.forRoot({ idle: 600, timeout: 300, ping: 120 })],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    HomeService,
    CommondropdownService,
    ValidationService,
    AppService,
    UserRolesService,
    CookieService,
    DevdataService,
    SharedService,
    DialogueService],
  exports: [RouterModule],
  entryComponents: [ConfirmDialogueComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
